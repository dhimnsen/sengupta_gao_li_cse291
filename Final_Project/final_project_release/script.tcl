
open_project hls
set_top l1_l2_hw
add_files l1_l2.cpp
add_files l1_l2.h
add_files -tb l1_l2_test.cpp
add_files -tb car.orig
add_files -tb conv.orig
open_solution "solution1"
set_part  {xc7z020clg484-1}
create_clock -period 5 -name default

source "./directives.tcl"

