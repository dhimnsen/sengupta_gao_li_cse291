/**
 *  CSE 291 Assignment 1
 */


#include "matmul.h"
using namespace std;

void matmul_sw(DTYPE matrix1[SIZE][SIZE], DTYPE matrix2[SIZE][SIZE], DTYPE out[SIZE][SIZE]) {
	DTYPE sum=0;
	for(int i=0;i<SIZE;i++) {
		for(int j=0;j<SIZE;j++) {
			sum = 0;
			for(int k=0;k<SIZE;k++) {
				sum = sum + matrix1[i][k]*matrix2[k][j];
			}
			out[i][j] = sum;
		}
	}
}


int main() {
	int fail=0;

	DTYPE matrix1[SIZE][SIZE];
	DTYPE matrix2[SIZE][SIZE];
	DTYPE matrix_swout[SIZE][SIZE];
	DTYPE matrix_hwout[SIZE][SIZE];


	for(int i=0;i<SIZE;i++){
		for(int j=0;j<SIZE;j++){
			matrix1[i][j]=i;
			matrix2[i][j]=j;
			matrix_swout[i][j]=0;
			matrix_hwout[i][j]=0;
		}
	}

    /*
     * Call Hardware Matrix Mul
     * */
	matmul_hw(matrix1,matrix2, matrix_hwout);

    /*
     * Call Software Matrix Mul
     * */
	matmul_sw(matrix1,matrix2, matrix_swout);

    /*
     * Check output
     * */
	for(int i=0;i<SIZE;i++){
		for(int j=0;j<SIZE;j++){
			if(matrix_swout[i][j]!=matrix_hwout[i][j]) {
				fail=1;
				cout<<"Software output = "<<matrix_swout[i][j]<<" Hardware output = "<<matrix_hwout[i][j]<<endl;
		}
	}
	}

	if(fail==1) {
		cout<<"FAILED"<<endl;
	}
	else {
		cout<<"PASSED"<<endl;
	}

	return 0;
}
