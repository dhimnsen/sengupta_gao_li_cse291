#ifndef _L1_L2_H_
#define _L1_L2_H_
#include <stdio.h>
#include <stdlib.h>
#include "hls_stream.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include "ap_int.h"
using namespace std;

typedef ap_int<8> PTYPE;
typedef ap_uint<8> DTYPE;
typedef ap_int<9> FTYPE;
typedef int MIDTYPE;
typedef hls::stream<MIDTYPE> MIDSTREAM;

#pragma SDS data copy(in[0:in_size], out[0:out_size], kernel[0:param_size])
#pragma SDS data access_pattern(in:SEQUENTIAL, out:SEQUENTIAL, kernel:SEQUENTIAL)
#pragma SDS data mem_attribute(in:NON_CACHEABLE|PHYSICAL_CONTIGUOUS, out:NON_CACHEABLE|PHYSICAL_CONTIGUOUS, kernel:NON_CACHEABLE|PHYSICAL_CONTIGUOUS)

void l1_l2_hw(MIDTYPE in[150528], MIDTYPE out[301056], MIDTYPE kernel[50176],
		MIDTYPE info[12], int in_size, int out_size, int param_size);

#endif
