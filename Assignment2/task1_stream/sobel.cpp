#include "sobel.h"

void sobel_hw (MYSTREAM *in, MYSTREAM *out) {

	DTYPE lineBuffer[3][SIZE_w];
#pragma HLS ARRAY_PARTITION variable=lineBuffer complete dim=1

	ap_int<12> windowBuffer[3][3];

    for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {
		#pragma HLS PIPELINE

			lineBuffer[0][i]=lineBuffer[1][i];
			lineBuffer[1][i]=lineBuffer[2][i];
			in->read(lineBuffer[2][i]);

			for(int k = 0; k < 3; k++) {
				windowBuffer[k][2] = windowBuffer[k][1];
				windowBuffer[k][1] = windowBuffer[k][0];
				windowBuffer[k][0] = (ap_int<12>)lineBuffer[k][i];
			}

			ap_int<12> result;
			ap_uint<8> result_final;

			ap_int<12> gx = 0;
			ap_int<12> gy = 0;
			ap_int<12> gx_abs = 0;
			ap_int<12> gy_abs = 0;

			gx = windowBuffer[0][0]+2*windowBuffer[1][0]+windowBuffer[2][0]
				- (windowBuffer[0][2]+2*windowBuffer[1][2]+windowBuffer[2][2]);
			gy = windowBuffer[0][0]+2*windowBuffer[0][1]+windowBuffer[0][2]
				- (windowBuffer[2][0]+2*windowBuffer[2][1]+windowBuffer[2][2]);

			gx_abs = (gx > 0)? gx : (ap_int<12>)-gx;
			gy_abs = (gy > 0)? gy : (ap_int<12>)-gy;

			result = gx_abs + gy_abs;
			result_final = (result>255)? (ap_uint<8>)255: (ap_uint<8>)result;
			out->write(result_final);

		}
	}

}

