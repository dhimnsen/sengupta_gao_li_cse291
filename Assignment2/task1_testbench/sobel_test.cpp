/**
 *  CSE 291 Assignment 2
 */


#include "sobel.h"
using namespace std;

void sobel_sw(DTYPE in[SIZE_w][SIZE_h], DTYPE out[SIZE_w][SIZE_h]) {
	
    for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {

	    	if (j == 0 || j == SIZE_h-1 || i == 0 || i == SIZE_w-1) {
	    		out[i][j] = 0;
	    		continue;
	    	}

	    	int gx = (int)(in[i+1][j-1]) + 2*(int)(in[i+1][j]) + (int)(in[i+1][j+1])
					- ((int)(in[i-1][j-1]) + 2*(int)(in[i-1][j]) + (int)(in[i-1][j+1]));
	    	int gy = (int)(in[i-1][j-1]) + 2*(int)(in[i][j-1]) + (int)(in[i+1][j-1])
					- ((int)(in[i-1][j+1]) + 2*(int)(in[i][j+1]) + (int)(in[i+1][j+1]));
	    	if (gx < 0) gx = -gx;
	    	if (gy < 0) gy = -gy;
	    	int result = gx + gy;
	    	if (result > 255) result = 255;
	    	out[i][j] = (DTYPE) result;

		}
	}

}


int main() {

	FILE * fp = fopen("lenna_gray.orig","r");
	int value = 0;
	DTYPE in_pic[SIZE_w][SIZE_h];

	for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {
			fscanf(fp, "%d", &value);
			in_pic[i][j] = (DTYPE)value;
		}
	}
	
	fclose(fp);

	DTYPE out_pic[SIZE_w][SIZE_h];
	DTYPE sw_out_pic[SIZE_w][SIZE_h];

	sobel_sw(in_pic, sw_out_pic);
	sobel_hw(in_pic, out_pic);

	FILE * fpw = fopen("lenna_gray.out","w");

	bool correct = 1;
	for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {
			fprintf(fpw,"%d\n",(int)(out_pic[i][j]));
			if (out_pic[i][j] != sw_out_pic[i][j]) {
				printf("Error: True %d After %d\n", (int)(sw_out_pic[i][j]), (int)(out_pic[i][j]));
				correct = 0;
			}
		}
	}	

	fclose(fpw);

	if (correct == 1) {
		printf("PASS: The output matches the sw output!\n");
		return 0;
	} else {
		printf("FAIL: Output DOES NOT match the sw output\n");
		return 1;
	}

}
