
open_project hls
set_top matmul_hw
add_files matmul.cpp
add_files matmul.h
add_files -tb matmul_test.cpp
open_solution "solution1"
set_part  {xc7z020clg484-1}
create_clock -period 5 -name default

source "./directives.tcl"

