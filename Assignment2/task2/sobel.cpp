#include "sobel.h"



void sobel_filter(DTYPE matrix1[ROW][COL], DTYPE out[ROW-2][COL-2]) {

	DTYPE mat[COL][9];
#pragma HLS ARRAY_PARTITION variable=mat cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=matrix1 cyclic factor=6 dim=1
#pragma HLS ARRAY_PARTITION variable=matrix1 cyclic factor=6 dim=2

	#pragma HLS ARRAY_PARTITION variable=out cyclic factor=58 dim=2


	for(int j =0; j<ROW-2; j++) {

	#pragma HLS DATAFLOW
		//initWindow(row0, row1, row2, window);
		for(int i = 0; i < COL-2; i++) {
		#pragma HLS PIPELINE II=1
		#pragma HLS UNROLL factor=2
			for(int k = 0; k < 3; k ++){
//			#pragma HLS PIPELINE II=1

				mat[i][0+k] = matrix1[j][i+k];
				mat[i][3+k] = matrix1[j+1][i+k];
				mat[i][6+k] = matrix1[j+2][i+k];
			}

		}
		for(int i = 0; i < COL-2; i++) {
		#pragma HLS unroll factor=58

			ap_int<13> Gx = - mat[i][0] +   mat[i][2] -
					   2*mat[i][3] + 2*mat[i][5] -
						 mat[i][6] +   mat[i][8];


			ap_int<13> Gy = mat[i][0] + 2*mat[i][1] + mat[i][2] -
					   mat[i][6] - 2*mat[i][7] - mat[i][8];

			if(Gx < 0) Gx = -Gx;
			if(Gy < 0) Gy = -Gy;

			out[j][i] = (DTYPE)(Gx + Gy);


		}
	}
}

