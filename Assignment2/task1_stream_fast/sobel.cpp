#include "sobel.h"

void sobel_hw (MYSTREAM *in, MYSTREAM *out) {

	DTYPE lineBuffer[3][SIZE_w];
#pragma HLS ARRAY_PARTITION variable=lineBuffer complete dim=1
#pragma HLS ARRAY_PARTITION variable=lineBuffer cyclic factor=8 dim=2

	ap_int<12> windowBuffer[8][3][3];

    for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i += 8) {
#pragma HLS PIPELINE

			for (int f = 0; f < 8; f++) {
				lineBuffer[0][i+f]=lineBuffer[1][i+f];
				lineBuffer[1][i+f]=lineBuffer[2][i+f];
			}

			ap_uint<64> temp = 0;
			in->read(temp);
			lineBuffer[2][i+0] = (ap_uint<8>)((temp & 0xFF00000000000000)>>56);
			lineBuffer[2][i+1] = (ap_uint<8>)((temp & 0x00FF000000000000)>>48);
			lineBuffer[2][i+2] = (ap_uint<8>)((temp & 0x0000FF0000000000)>>40);
			lineBuffer[2][i+3] = (ap_uint<8>)((temp & 0x000000FF00000000)>>32);
			lineBuffer[2][i+4] = (ap_uint<8>)((temp & 0x00000000FF000000)>>24);
			lineBuffer[2][i+5] = (ap_uint<8>)((temp & 0x0000000000FF0000)>>16);
			lineBuffer[2][i+6] = (ap_uint<8>)((temp & 0x000000000000FF00)>>8);
			lineBuffer[2][i+7] = (ap_uint<8>)(temp & 0x00000000000000FF);

			for(int k = 0; k < 3; k++) {
				windowBuffer[0][k][2] = windowBuffer[7][k][1];
				windowBuffer[0][k][1] = windowBuffer[7][k][0];
				windowBuffer[0][k][0] = (ap_int<12>)lineBuffer[k][i];
				windowBuffer[1][k][2] = windowBuffer[7][k][0];
				windowBuffer[1][k][1] = (ap_int<12>)lineBuffer[k][i];
				windowBuffer[1][k][0] = (ap_int<12>)lineBuffer[k][i+1];
				windowBuffer[2][k][2] = (ap_int<12>)lineBuffer[k][i];
				windowBuffer[2][k][1] = (ap_int<12>)lineBuffer[k][i+1];
				windowBuffer[2][k][0] = (ap_int<12>)lineBuffer[k][i+2];
				windowBuffer[3][k][2] = (ap_int<12>)lineBuffer[k][i+1];
				windowBuffer[3][k][1] = (ap_int<12>)lineBuffer[k][i+2];
				windowBuffer[3][k][0] = (ap_int<12>)lineBuffer[k][i+3];
				windowBuffer[4][k][2] = (ap_int<12>)lineBuffer[k][i+2];
				windowBuffer[4][k][1] = (ap_int<12>)lineBuffer[k][i+3];
				windowBuffer[4][k][0] = (ap_int<12>)lineBuffer[k][i+4];
				windowBuffer[5][k][2] = (ap_int<12>)lineBuffer[k][i+3];
				windowBuffer[5][k][1] = (ap_int<12>)lineBuffer[k][i+4];
				windowBuffer[5][k][0] = (ap_int<12>)lineBuffer[k][i+5];
				windowBuffer[6][k][2] = (ap_int<12>)lineBuffer[k][i+4];
				windowBuffer[6][k][1] = (ap_int<12>)lineBuffer[k][i+5];
				windowBuffer[6][k][0] = (ap_int<12>)lineBuffer[k][i+6];
				windowBuffer[7][k][2] = (ap_int<12>)lineBuffer[k][i+5];
				windowBuffer[7][k][1] = (ap_int<12>)lineBuffer[k][i+6];
				windowBuffer[7][k][0] = (ap_int<12>)lineBuffer[k][i+7];
			}

			ap_int<12> result[8];
			ap_uint<8> result_final[8];

			for (int f = 0; f < 8; f++) {
				ap_int<12> gx = 0;
				ap_int<12> gy = 0;
				ap_int<12> gx_abs = 0;
				ap_int<12> gy_abs = 0;

				gx = windowBuffer[f][0][0]+2*windowBuffer[f][1][0]+windowBuffer[f][2][0]
					- (windowBuffer[f][0][2]+2*windowBuffer[f][1][2]+windowBuffer[f][2][2]);
				gy = windowBuffer[f][0][0]+2*windowBuffer[f][0][1]+windowBuffer[f][0][2]
					- (windowBuffer[f][2][0]+2*windowBuffer[f][2][1]+windowBuffer[f][2][2]);

				gx_abs = (gx > 0)? gx : (ap_int<12>)-gx;
				gy_abs = (gy > 0)? gy : (ap_int<12>)-gy;

				result[f] = gx_abs + gy_abs;
				result_final[f] = (result[f]>255)? (ap_uint<8>)255: (ap_uint<8>)result[f];
			}

			temp = 0;
			temp = temp | (((ap_uint<64>)result_final[0])<<56);
			temp = temp | (((ap_uint<64>)result_final[1])<<48);
			temp = temp | (((ap_uint<64>)result_final[2])<<40);
			temp = temp | (((ap_uint<64>)result_final[3])<<32);
			temp = temp | (((ap_uint<64>)result_final[4])<<24);
			temp = temp | (((ap_uint<64>)result_final[5])<<16);
			temp = temp | (((ap_uint<64>)result_final[6])<<8);
			temp = temp | ((ap_uint<64>)result_final[7]);
			out->write(temp);

		}
	}

}

