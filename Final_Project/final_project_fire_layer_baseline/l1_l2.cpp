#include "l1_l2.h"

void doNothing(){}

void fire_layer (MYSTREAM *in, MYSTREAM *out, MYSTREAM *smallOut, const ap_uint<9> data_size,
		const ap_uint<9> out_ch, const ap_uint<7> in_ch,
		const bool is_first, const ap_int<9> start_size,
		const ap_int<9> end_size, const ap_uint<3> loop_divider,
		const ap_uint<10> first_ch, const ap_uint<5> first_divider,
		const PTYPE kernelBuffer1[7][7][3][96], const PTYPE kernelBuffer2[3][3][64][256],
		const PTYPE kernelOffset[256], const PTYPE smallKernel[64][256],
		const PTYPE smallKernelOffset[256], const PTYPE firstKernel[512][64],
		const PTYPE firstKernelOffset[64]) {

#pragma HLS DATAFLOW

	DTYPE lineBuffer1[7][230][3];
#pragma HLS ARRAY_PARTITION variable=lineBuffer1 complete dim=1

	DTYPE lineBuffer2[3][58][64];
#pragma HLS ARRAY_PARTITION variable=lineBuffer2 complete dim=1

	DTYPE windowBuffer1[7][7][3];
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=2
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=3

	DTYPE windowBuffer2[3][3][64];
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=2
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=3

#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=1
#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=2
#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=3

#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 complete dim=1
#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 complete dim=2
#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 cyclic factor=16 dim=3

#pragma HLS ARRAY_PARTITION variable=smallKernel cyclic factor=16 dim=1

#pragma HLS ARRAY_PARTITION variable=firstKernel cyclic factor=32 dim=1

	MYSTREAM middle_fifo;
#pragma HLS STREAM variable=middle_fifo depth=4000 dim=1

	First_Height_Loop: for(ap_uint<8> i = 0; i < data_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=14 max=224
		First_Width_Loop: for (ap_uint<8> j = 0; j < data_size; j++) {
#pragma HLS LOOP_TRIPCOUNT min=14 max=224

			if (is_first) {
				for (ap_uint<7> k = 0; k < in_ch; k++) {
#pragma HLS LOOP_TRIPCOUNT min=3 max=3
#pragma HLS PIPELINE
					DTYPE tempBuf = in->read();
					middle_fifo.write(tempBuf);
				}
				continue;
			}

			DTYPE firstBuffer[512];
#pragma HLS ARRAY_PARTITION variable=firstBuffer cyclic factor=32 dim=1

			for (ap_uint<10> k = 0; k < first_ch; k++) {
#pragma HLS LOOP_TRIPCOUNT min=96 max=512
#pragma HLS PIPELINE
				DTYPE tempBuf = in->read();
				firstBuffer[k] = tempBuf;
			}

			ap_int<24> firstIntermediate[64];

			for (ap_uint<5> z = 0; z < first_divider; z++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=16
				for (ap_uint<7> k = 0; k < in_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
#pragma HLS PIPELINE
					ap_int<24> firstTempBuf = (z==0)? (ap_int<24>)0 : firstIntermediate[k];
					for (int l = 0; l < 32; l++){
						firstTempBuf += firstBuffer[l+32*z]*firstKernel[l+32*z][k];
					}

					firstTempBuf = (z==(first_divider-1))? (ap_int<24>)(firstTempBuf+firstKernelOffset[k]) : firstTempBuf;

					firstTempBuf = ((firstTempBuf>255)&&(z==(first_divider-1)))? (ap_int<24>)255 : firstTempBuf;
					firstTempBuf = ((firstTempBuf<0)&&(z==(first_divider-1)))? (ap_int<24>)0 : firstTempBuf;
					firstIntermediate[k] = firstTempBuf;

					(z==(first_divider-1))? middle_fifo.write((DTYPE)firstTempBuf) : doNothing();
				}
			}

		}
	}



	Height_Loop: for(ap_int<9> i = start_size; i < end_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=16 max=230
		Width_Loop: for (ap_int<9> j = start_size; j < end_size; j++) {
#pragma HLS LOOP_TRIPCOUNT min=16 max=230

			ap_int<24> intermediate[256];
			ap_int<24> smallIntermediate[256];
			DTYPE smallBuffer[64];
#pragma HLS ARRAY_PARTITION variable=smallBuffer complete dim=1

			if (is_first) {

				First_Loop_1: for (ap_uint<7> k = 0; k < in_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=3
#pragma HLS DEPENDENCE variable=lineBuffer1 false
#pragma HLS DEPENDENCE variable=windowBuffer1 false
#pragma HLS PIPELINE

					DTYPE tempBuf = !(i<0||j<0||i>=data_size||j>=data_size) ? middle_fifo.read() : (DTYPE)0;
					DTYPE innerBuffer1[7];

					for (int l = 0; l < 6; l++){
						innerBuffer1[l] = lineBuffer1[l+1][j+3][k];
						lineBuffer1[l][j+3][k] = innerBuffer1[l];
					}

					lineBuffer1[6][j+3][k] = tempBuf;
					innerBuffer1[6] = tempBuf;

					for (int l = 0; l < 7; l++){
						for (int m = 0; m < 6; m++){
							windowBuffer1[l][m][k] = windowBuffer1[l][m+1][k];
						}
						windowBuffer1[l][6][k] = innerBuffer1[l];
					}

				}

			} else {

				First_Loop_2: for (ap_uint<7> k = 0; k < in_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
#pragma HLS DEPENDENCE variable=lineBuffer2 false
#pragma HLS DEPENDENCE variable=windowBuffer2 false
#pragma HLS DEPENDENCE variable=smallBuffer false
#pragma HLS PIPELINE II=2
					DTYPE tempBuf = !(i<0||j<0||i>=data_size||j>=data_size) ? middle_fifo.read() : (DTYPE)0;
					DTYPE innerBuffer2[3];

					for (int l = 0; l < 2; l++){
						innerBuffer2[l] = lineBuffer2[l+1][j+1][k];
						lineBuffer2[l][j+1][k] = innerBuffer2[l];
					}

					lineBuffer2[2][j+1][k] = tempBuf;
					innerBuffer2[2] = tempBuf;

					windowBuffer2[0][0][k] = windowBuffer2[0][1][k];
					windowBuffer2[0][1][k] = windowBuffer2[0][2][k];
					windowBuffer2[0][2][k] = innerBuffer2[0];


					windowBuffer2[1][0][k] = windowBuffer2[1][1][k];

					DTYPE sharedValue = windowBuffer2[1][2][k];
					smallBuffer[k] = sharedValue;
					windowBuffer2[1][1][k] = sharedValue;
					windowBuffer2[1][2][k] = innerBuffer2[1];

					windowBuffer2[2][0][k] = windowBuffer2[2][1][k];
					windowBuffer2[2][1][k] = windowBuffer2[2][2][k];
					windowBuffer2[2][2][k] = innerBuffer2[2];

				}

			}

			if ((((i%2==0 || j%2==0 || i<3 || j<3)&&is_first)) || ((i<1 || j<1)&&(!is_first))) continue;

			for (ap_uint<3> z = 0; z < loop_divider; z++) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=4

				Second_Loop: for (ap_uint<9> k = 0; k < out_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
#pragma HLS DEPENDENCE variable=windowBuffer1 false
#pragma HLS DEPENDENCE variable=windowBuffer2 false
#pragma HLS PIPELINE
					ap_int<24> tempBuf = (z==0)? (ap_int<24>)0 : intermediate[k];
					ap_int<24> smallTempBuf = (z==0)? (ap_int<24>)0 : smallIntermediate[k];
					DTYPE candidate_data[147];
#pragma HLS ARRAY_PARTITION variable=candidate_data complete dim=1
					PTYPE candidate_para[147];
#pragma HLS ARRAY_PARTITION variable=candidate_para complete dim=1

					ap_uint<8> index = 0;
					Second_inner_1: for (int s = 0; s < 3; s++){
						ap_int<9> true_i = i-2+s;
						for (int t = 0; t < 3; t++){
							ap_int<9> true_j = j-2+t;
							for (int l = 0; l < 16; l++){
								candidate_data[index] = ((!is_first)&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										windowBuffer2[s][t][l+16*z] : (DTYPE)0;
								candidate_para[index] = ((!is_first)&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										kernelBuffer2[s][t][l+16*z][k] : (PTYPE)0;
								index++;
							}
						}
					}
					candidate_data[144] = (DTYPE)0;
					candidate_para[144] = (PTYPE)0;
					candidate_data[145] = (DTYPE)0;
					candidate_para[145] = (PTYPE)0;
					candidate_data[146] = (DTYPE)0;
					candidate_para[146] = (PTYPE)0;

					index = 0;
					Second_inner_2: for (int s = 0; s < 7; s++){
						ap_int<9> true_i = i-6+s;
						for (int t = 0; t < 7; t++){
							ap_int<9> true_j = j-6+t;
							for (int l = 0; l < 3; l++){
								candidate_data[index] = (is_first&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										windowBuffer1[s][t][l] : candidate_data[index];
								candidate_para[index] = (is_first&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										kernelBuffer1[s][t][l][k] : candidate_para[index];
								index++;
							}
						}
					}

					for (int l = 0; l < 147; l++){
						tempBuf += candidate_data[l]*candidate_para[l];
					}

					for (int l = 0; l < 16; l++){
						smallTempBuf += smallBuffer[l+16*z]*smallKernel[l+16*z][k];
					}

					tempBuf = (z==(loop_divider-1))? (ap_int<24>)(tempBuf+kernelOffset[k]) : tempBuf;

					tempBuf = ((tempBuf>255)&&(z==(loop_divider-1)))? (ap_int<24>)255 : tempBuf;
					tempBuf = ((tempBuf<0)&&(z==(loop_divider-1)))? (ap_int<24>)0 : tempBuf;
					intermediate[k] = tempBuf;

					(z==(loop_divider-1))? out->write((DTYPE)tempBuf) : doNothing();

					smallTempBuf = (z==(loop_divider-1))? (ap_int<24>)(smallTempBuf+smallKernelOffset[k]) : smallTempBuf;

					smallTempBuf = ((smallTempBuf>255)&&(z==(loop_divider-1)))? (ap_int<24>)255 : smallTempBuf;
					smallTempBuf = ((smallTempBuf<0)&&(z==(loop_divider-1)))? (ap_int<24>)0 : smallTempBuf;
					smallIntermediate[k] = smallTempBuf;

					(z==(loop_divider-1) && (!is_first))? smallOut->write((DTYPE)smallTempBuf) : doNothing();

				}

			}
		}

	}


}

void l1_l2_hw (MYSTREAM *in, MYSTREAM *out, MYSTREAM *smallOut, PASTREAM *kernel, Settings *info) {

	PTYPE kernelBuffer1[7][7][3][96];
	PTYPE kernelBuffer2[3][3][64][256];
	PTYPE kernelOffset[256];

	PTYPE smallKernel[64][256];
	PTYPE smallKernelOffset[256];

	PTYPE firstKernel[512][64];
	PTYPE firstKernelOffset[64];

const ap_uint<9> data_size = info->dataSize;
const ap_uint<9> out_ch = info->outputChannel;
const ap_uint<7> in_ch = info->inputChannel;
const ap_uint<3> win_size = info->windowSize;
const bool is_first = info->isFirst;
const ap_int<9> start_size = info->startSize;
const ap_int<9> end_size = info->endSize;
const ap_uint<3> loop_divider = info->loopDivider;
const ap_uint<10> first_ch = info->firstChannel;
const ap_uint<5> first_divider = info->firstDivider;

	Kernel_Loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
		for (ap_uint<7> j = 0; j < in_ch; j++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=64
			for (ap_uint<3> s = 0; s < win_size; s++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=7
				for (ap_uint<3> t = 0; t < win_size; t++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=7
					 (is_first)? kernel->read(kernelBuffer1[s][t][j][i])
							 : kernel->read(kernelBuffer2[s][t][j][i]);
				}
			}
		}
	}

	Kernel_offset_loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
		kernel->read(kernelOffset[i]);
	}

	smallKernel_Loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
		for (ap_uint<7> j = 0; j < in_ch; j++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
			smallKernel[j][i] = (!is_first)? kernel->read() : (PTYPE)0;
		}
	}

	smallKernel_offset_loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
		smallKernelOffset[i] = (!is_first)? kernel->read() : (PTYPE)0;
	}

	firstKernel_Loop: for (ap_uint<7> i = 0; i < in_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
		for (ap_uint<10> j = 0; j < first_ch; j++){
#pragma HLS LOOP_TRIPCOUNT min=96 max=512
			firstKernel[j][i] = (!is_first)? kernel->read() : (PTYPE)0;
		}
	}

	firstKernel_offset_loop: for (ap_uint<7> i = 0; i < in_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
		firstKernelOffset[i] = (!is_first)? kernel->read() : (PTYPE)0;
	}

	fire_layer(in, out, smallOut, data_size, out_ch, in_ch, is_first, start_size,
		end_size, loop_divider, first_ch, first_divider, kernelBuffer1, kernelBuffer2,
		kernelOffset, smallKernel, smallKernelOffset, firstKernel, firstKernelOffset);

}

