#include "matmul.h"

void matmul_hw(DTYPE matrix1[SIZE][SIZE], DTYPE matrix2[SIZE][SIZE], DTYPE out[SIZE][SIZE]) {
	DTYPE sum=128;
	ap_int<2> temp[SIZE] = {0};
	ap_int<2> temp1[SIZE] = {0};

#pragma HLS ARRAY_PARTITION variable=matrix1 complete dim = 2
#pragma HLS ARRAY_PARTITION variable=matrix2 complete dim = 1
#pragma HLS ARRAY_PARTITION variable=matrix2 cyclic factor=64 dim = 2
#pragma HLS ARRAY_PARTITION variable=out cyclic factor=64 dim = 2

	for(int i=0;i<SIZE;i++) {
		for(int k=0;k<SIZE;k++) {
		#pragma HLS UNROLL
			temp[k] = matrix1[i][k];
		}
		for(int j=0;j<SIZE;j++) {
		#pragma HLS PIPELINE II=1
		#pragma HLS UNROLL factor=64
			sum = 128;
			for(int k=0;k<SIZE;k++) {
				temp1[k] = matrix2[k][j];
			}
			for(int k=0;k<SIZE;k++) {
				sum += (temp[k]^temp1[k]);
			}
			out[i][j] = sum;
		}
	}
}
