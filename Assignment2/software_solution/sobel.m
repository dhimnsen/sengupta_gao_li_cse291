function [ img_out ] = sobel( img_in )
% Sobel filter estimation
%   Detailed explanation goes here
    [row, col, dim] = size(img_in);
    
    h = single([-1 0 1; -2 0 2; -1 0 1]);
    v = single([1 2 1; 0 0 0; -1 -2 -1]);
    
    G = zeros(row, col, dim);
    
    for r = 1:(row-3)
        for c = 1:(col-3)
            A = single(img_in(r:r+2, c:c+2, 1));
            Gx = sum(sum(h.*A));
            Gy = sum(sum(v.*A));
            
           % rgb = sqrt(Gx*Gx + Gy*Gy);
            rgb = abs(Gx) + abs(Gy);
            G(r,c,1) = rgb;
            G(r,c,2) = rgb;
            G(r,c,3) = rgb;
        end 
    end

    G = cast(G, 'uint8');
    img_out = G(2:row-1, 2:col-1, :);
end

