Assignment 1
====================

[TOC]

![a2_result](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/a2_result.bmp)

**Figure 1:** Input and output streams of the sobel filter

# Task 1: 

## Hardware Architecture Diagram 

![a2_task1](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/a2_task1.jpg)

**Figure 2:** Architecture datapath for line buffer sobel filter

## Performance and Area Utilization Results 

~~~~

================================================================
== Vivado HLS Report for 'sobel_hw'
================================================================
* Date:           Mon Apr 24 22:29:58 2017

* Version:        2016.4 (Build 1756540 on Mon Jan 23 19:31:01 MST 2017)
* Project:        hls
* Solution:       solution1
* Product family: zynq
* Target device:  xc7z020clg484-1


================================================================
== Performance Estimates
================================================================
+ Timing (ns): 
    * Summary: 
    +--------+-------+----------+------------+
    |  Clock | Target| Estimated| Uncertainty|
    +--------+-------+----------+------------+
    |ap_clk  |   5.00|      5.24|        0.62|
    +--------+-------+----------+------------+

+ Latency (clock cycles): 
    * Summary: 
    +--------+--------+--------+--------+---------+
    |     Latency     |     Interval    | Pipeline|
    |   min  |   max  |   min  |   max  |   Type  |
    +--------+--------+--------+--------+---------+
    |  307209|  307209|  307210|  307210|   none  |
    +--------+--------+--------+--------+---------+

    + Detail: 
        * Instance: 
        N/A

        * Loop: 
        +----------+--------+--------+----------+-----------+-----------+--------+----------+
        |          |     Latency     | Iteration|  Initiation Interval  |  Trip  |          |
        | Loop Name|   min  |   max  |  Latency |  achieved |   target  |  Count | Pipelined|
        +----------+--------+--------+----------+-----------+-----------+--------+----------+
        |- Loop 1  |  307207|  307207|         9|          1|          1|  307200|    yes   |
        +----------+--------+--------+----------+-----------+-----------+--------+----------+



================================================================
== Utilization Estimates
================================================================
* Summary: 
+-----------------+---------+-------+--------+-------+
|       Name      | BRAM_18K| DSP48E|   FF   |  LUT  |
+-----------------+---------+-------+--------+-------+
|DSP              |        -|      -|       -|      -|
|Expression       |        -|      -|       0|    205|
|FIFO             |        -|      -|       -|      -|
|Instance         |        -|      -|       -|      -|
|Memory           |        2|      -|       0|      0|
|Multiplexer      |        -|      -|       -|     34|
|Register         |        -|      -|     343|     33|
+-----------------+---------+-------+--------+-------+
|Total            |        2|      0|     343|    272|
+-----------------+---------+-------+--------+-------+
|Available        |      280|    220|  106400|  53200|
+-----------------+---------+-------+--------+-------+
|Utilization (%)  |    ~0   |      0|   ~0   |   ~0  |
+-----------------+---------+-------+--------+-------+

~~~~ 

## Specific Optimizations 

- For easier computation, we simply added up the absolute value of Gx and of Gy together instead of taking the square root of the squares. The loss in accuracy is out weighted by the performance gained by computing the value the first way.

- Actually, the processing speed of sobel filter depends on the data width of input FIFO (how many image pixels that can be input to the filter for each clk cycle). If we have a faster interface to outside, we may accelerate the whole process. In another version of task 1 (named task1_stream_fast), we achieve 8 times faster design compared to the original one. The way is that we are using 8 window buffers at the same time, and computing them in parallel in each clk cycle. Currently, the resulting interval is 38409 intervals. If the interface is fast enough, we may achieve as low as ~650 cycles for filtering the whole image. 


# Task 2: 


![in](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/in.jpg)

**Figure 3:** The picture that was processed by the sobel filter before

![out](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/out.bmp)

**Figure 4:** The picture after being processed by the sobel filter

## Hardware Architecture Diagram 

![hw2_task2_architecture](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/hw2_task2_architecture.png)

**Figure 5:** The architecture of the dataflow method of the sobel filter 

## Performance and Area Utilization Results 

```
================================================================
== Performance Estimates
================================================================
+ Timing (ns): 
    * Summary: 
    +--------+-------+----------+------------+
    |  Clock | Target| Estimated| Uncertainty|
    +--------+-------+----------+------------+
    |ap_clk  |   5.00|      4.82|        0.62|
    +--------+-------+----------+------------+

+ Latency (clock cycles): 
    * Summary: 
    +--------+--------+--------+--------+---------+
    |     Latency     |     Interval    | Pipeline|
    |   min  |   max  |   min  |   max  |   Type  |
    +--------+--------+--------+--------+---------+
    |  167059|  167059|  167060|  167060|   none  |
    +--------+--------+--------+--------+---------+

    + Detail: 
        * Instance: 
        +-----------------------------+------------------+-----+-----+-----+-----+----------+
        |                             |                  |  Latency  |  Interval | Pipeline |
        |           Instance          |      Module      | min | max | min | max |   Type   |
        +-----------------------------+------------------+-----+-----+-----+-----+----------+
        |grp_dataflow_in_loop_fu_224  |dataflow_in_loop  |  584|  584|  349|  349| dataflow |
        +-----------------------------+------------------+-----+-----+-----+-----+----------+

        * Loop: 
        +----------+--------+--------+----------+-----------+-----------+------+----------+
        |          |     Latency     | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name|   min  |   max  |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+--------+--------+----------+-----------+-----------+------+----------+
        |- Loop 1  |  167058|  167058|       586|          -|          -|   478|    no    |
        +----------+--------+--------+----------+-----------+-----------+------+----------+



================================================================
== Utilization Estimates
================================================================
* Summary: 
+-----------------+---------+-------+--------+-------+
|       Name      | BRAM_18K| DSP48E|   FF   |  LUT  |
+-----------------+---------+-------+--------+-------+
|DSP              |        -|      -|       -|      -|
|Expression       |        -|      -|       0|     18|
|FIFO             |        -|      -|       -|      -|
|Instance         |       32|     14|    8309|  12276|
|Memory           |        -|      -|       -|      -|
|Multiplexer      |        -|      -|       -|     18|
|Register         |        -|      -|      20|      -|
+-----------------+---------+-------+--------+-------+
|Total            |       32|     14|    8329|  12312|
+-----------------+---------+-------+--------+-------+
|Available        |      280|    220|  106400|  53200|
+-----------------+---------+-------+--------+-------+
|Utilization (%)  |       11|      6|       7|     23|
+-----------------+---------+-------+--------+-------+
```

## Specific Optimizations 

- The architecture of this sobel filter is a modified version of that found in Chellapilla et al.'s paper "High performance convolutional neural networks for document processing."

- The two 3x3 kernels were converted into a 9 X 2 kernel

- Instead of converting the entire 480 X 640 array into a 304964 X 9 array we decided to segmented it into 478 different 638 X 9 array. This way instead of calculating the entire output matrix in one [304964,9] X [9,2] matrix multiplication, which would have used up too much resources, we are calculating the output matrix in 478 different [678, 9] X [9, 2] matrix multiplies.

- We restructured our input array in a way that will allow us to multiply our two kernel in parallel. By restructuring our array as shown in figure 5 do multiple multiplication in one cycle.