
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "l1_l2.h"
#include "runconv.h"
#include "testbench.h"
#include "utility.h"

#include <sys/types.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include <pthread.h>
#include <opencv2/opencv.hpp>
#include <errno.h>

#include "draw.h"
#include "drm.h"

using namespace cv;

#define PIXEL_STRIDE 5
#define RUN_SECONDS     30

struct webcam_thread_data {
	int thread_id;
	volatile char *camera;
	uint32_t webcam_x_res;
	uint32_t webcam_y_res;
	volatile bool ready;
	volatile bool stop;
};

int num_modeset_dev = 0;
drm_modeset_dev *modeset_list = NULL;

/* This thread is responsible for capturing data from the webcam, scaling (if necessary), and color space
 * conversion.
 */
void *webcam_thread(void *thread_data)
{
   webcam_thread_data *data = (webcam_thread_data *)thread_data;

   cv::VideoCapture webcam(0);

   if(!webcam.isOpened()) {
      std::cerr << "Cannot open the webcam" << std::endl;
      exit(1);
   }

   webcam.set(3, 320);
   webcam.set(4, 240);

   data->webcam_x_res = (int)(webcam.get(3) + 0.5);
   data->webcam_y_res = (int)(webcam.get(4) + 0.5);
   std::cout << "Webcam opened at " << data->webcam_x_res << "x" << data->webcam_y_res << std::endl;

   cv::Mat webcam_frame;
   webcam >> webcam_frame;

   /* The use of sds_alloc here is equivalent to malloc(), but registers the buffers with the SDSoC
    * back end and ensures that we're allocating contiguous memory for subsequent DMA operations.
    */

   char *buffer_one_pointer = (char *)sds_alloc(webcam_frame.total() * 4);
   char *buffer_two_pointer = (char *)sds_alloc(webcam_frame.total() * 4);

  if ((!buffer_one_pointer) || (!buffer_two_pointer)) {
    std::cerr << "ERROR: Unable to allocate memory for incoming webcam frames" << std::endl;
    exit(1);
  }

   cv::Mat rgba_mat1(webcam_frame.size(), CV_8UC4, buffer_one_pointer);
   cv::Mat rgba_mat2(webcam_frame.size(), CV_8UC4, buffer_two_pointer);

   cv::cvtColor(webcam_frame, rgba_mat1, cv::COLOR_BGR2RGBA, 4);
   data->camera = buffer_one_pointer;
   data->ready = 1;

   while(1) {

      // Check for our exit condition
      if (data->stop) {
         pthread_exit(NULL);
      }

      webcam >> webcam_frame;
      if (data->camera == buffer_one_pointer) {
         cv::cvtColor(webcam_frame, rgba_mat2, cv::COLOR_BGR2BGRA, 4);
         data->camera = buffer_two_pointer;
      }
      else {
         cv::cvtColor(webcam_frame, rgba_mat1, cv::COLOR_BGR2BGRA, 4);
         data->camera = buffer_one_pointer;
      }
   }
}

/* This function handles actually calling the hardware accelerator. In addition, it manages the pointers
 * for the current frame buffer and registers a page flip request with the DRM subsystem when we're done
 * drawing the next frame.
 */
static void draw_hardware(int fd, struct webcam_thread_data *thread_data)
{
   static unsigned int x, y = 0;
   static bool x_dir, y_dir = false;
   drm_modeset_buf *buf;
   char *fb;
   int status;

   unsigned long long count_start;

   /* Ensure that we're using the current (non-displayed) frame buffer, then launch the hardware
    * accelerator to fill it. We'll bracket this with timebase functions to accurately calculate the
    * CPU time spent on hardware acceleration.
    */
   buf = &modeset_list->bufs[modeset_list->front_buf ^ 1];
   fb = (char *)buf->map;
   count_start = sds_clock_counter();
   draw_something((unsigned int *)fb, (unsigned int *)thread_data->camera, buf->width,
         buf->height, thread_data->webcam_x_res, thread_data->webcam_y_res, x, y);
   modeset_list->total_run_time += sds_clock_counter() - count_start;

   /* Swap the frame buffers */
   status = drmModePageFlip(fd, modeset_list->crtc, buf->fb, DRM_MODE_PAGE_FLIP_EVENT, thread_data);
   if (status) {
      std::cerr << "ERROR: Cannot schedule page flip for connector " << modeset_list->conn
            << ": " << status << std::endl;
      exit(1);
   }
   else {
      modeset_list->front_buf ^= 1;
      modeset_list->pflip_pending = true;
   }

   // Everything after this is just repositioning the overlay for the next frame
   if (x_dir) {
      x -= PIXEL_STRIDE;
      if (x <= 0) {
         x = 0;
         x_dir = false;
      }
   }
   else {
      x += PIXEL_STRIDE;
      if (x >= (buf->width - thread_data->webcam_x_res)) {
         x = buf->width - thread_data->webcam_x_res;
         x_dir = true;
      }
   }

   if (y_dir) {
      y -= PIXEL_STRIDE;
      if (y <= 0) {
         y = 0;
         y_dir = false;
      }
   }
   else {
      y += PIXEL_STRIDE;
      if (y >= (buf->height - thread_data->webcam_y_res)) {
         y = buf->height - thread_data->webcam_y_res;
         y_dir = true;
      }
   }
   modeset_list->num_frames++;
}

/* This is the software equivalent version of the hardware accelerator, so that we can effectively
 * compare the CPU execution time between hardware and software
 */
int draw_something_sw(unsigned int *fb,
                  unsigned int *img,
                 unsigned int x_res,
                 unsigned int y_res,
                 unsigned int img_x_res,
                 unsigned int img_y_res,
                 unsigned int img_x,
                 unsigned int img_y,
                 unsigned int *stream_o) {

   FILE *fp;
   fp = fopen("frame.dat", "w");
   for(unsigned int y = 0; y < y_res; y++) {
	   for(unsigned int x = 0; x < x_res; x++) {
         if((x >= img_x) && (x < (img_x + img_x_res)) &&
            (y >= img_y) && (y < (img_y + img_y_res))) {
            //fprintf(fp, "00|%3u|%3u|%3u ", *(img)&0x000000FF, (*(img)&0x0000FF00)>>8, (*(img)&0x00FF0000)>>16);
            //stream_o[y*x_res + x_res] = *img;
            *fb++ = *img++;
         }
         else {
            //fprintf(fp, "00|00|00|00 ");
            //stream_o[y*x_res + x_res] = 0;
            *fb++ = 0;
         }
      }
      fprintf(fp, "\n");
   }

   fclose(fp);
   return 0;
}

/* Every time we flip a page, i.e. every time the display is in the vlank state and we have a new
 * frame buffer drawn, this event will be called. As long as we're not pending a cleanup (system shutdown)
 * we'll immediately launch into drawing the next hardware frame buffer.
 */
static void drm_page_flip_event(int fd, unsigned int frame,
                        unsigned int sec, unsigned int usec,
                        void *data)
{
   modeset_list->pflip_pending = false;
   if(!modeset_list->cleanup) {
      draw_hardware(fd, (struct webcam_thread_data *)data);
   }
}

void write_input_buffer(unsigned int *img,
                        unsigned int x_res,
                        unsigned int y_res,
                        unsigned int img_x_res,
                        unsigned int img_y_res,
                        unsigned int img_x,
                        unsigned int img_y,
                        unsigned int *stream) {

   for(unsigned int x = 0; x < x_res; x++) {
      for(unsigned int y = 0; y < y_res; y++) {
         if((x >= img_x) && (x < (img_x + img_x_res)) &&
            (y >= img_y) && (y < (img_y + img_y_res))) {
            //fprintf(fp, "00|%3u|%3u|%3u ", *(img)&0x000000FF, (*(img)&0x0000FF00)>>8, (*(img)&0x00FF0000)>>16);
            stream[x*y_res + y] = *img++;
         }
         else {
            stream[x*y_res + y] = 0;
         }
      }
   }
}

void draw_output_buffer(unsigned int *fb,
                        unsigned int *stream,
                        unsigned int x_res,
                        unsigned int y_res,
                        unsigned int img_x_res,
                        unsigned int img_y_res,
                        unsigned int img_x,
                        unsigned int img_y) {

   for(unsigned int x = 0; x < x_res; x++) {
      for(unsigned int y = 0; y < y_res; y++) {
         if((x >= img_x) && (x < (img_x + img_x_res)) &&
            (y >= img_y) && (y < (img_y + img_y_res))) {
            //fprintf(fp, "00|%3u|%3u|%3u ", *(img)&0x000000FF, (*(img)&0x0000FF00)>>8, (*(img)&0x00FF0000)>>16);
            *fb++ = stream[x*y_res + y];
         }
         else {
            *fb++ = 0;
         }
      }
   }
}

int main (int argc, char* argv[]) {

     MIDTYPE *in, *img_in, *out, *outcacheable
     	 	, *kernel1, *kernel2, *kernel3, *kernel4, *kernel5
	 		, *kernel6, *kernel7, *kernel8, *kernel9, *kernel10
			, *in1, *in2, *in3, *in4, *in5
			, *in6, *in7, *in8, *in9, *in10
			, *out_sw1, *out_sw2, *out_sw3, *out_sw4, *out_sw5
			, *out_sw6, *out_sw7, *out_sw8, *out_sw9, *out_sw10
			, *inmp1, *inmp4, *inmp8, *outmp_sw1, *outmp_sw4, *outmp_sw8;

     in = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     img_in = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     out = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     outcacheable = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));

     kernel1 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel2 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel3 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel4 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel5 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel6 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel7 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel8 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel9 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     kernel10 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
     
     in1 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in2 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in3 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in4 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in5 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in6 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in7 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in8 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in9 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     in10 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
     inmp1 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     inmp4 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     inmp8 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));

     out_sw1 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw2 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw3 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw4 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw5 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw6 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw7 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw8 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw9 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     out_sw10 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     outmp_sw1 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     outmp_sw4 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));
     outmp_sw8 = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));

     if (!in || !img_in || !out || !outcacheable
      || !kernel1 || !kernel2 || !kernel3 || !kernel4 || !kernel5
	  || !kernel6 || !kernel7 || !kernel8 || !kernel9 || !kernel10
	  || !in1 || !in2 || !in3 || !in4 || !in5
	  || !in6 || !in7 || !in8 || !in9 || !in10
	  || !out_sw1 || !out_sw2 || !out_sw3 || !out_sw4 || !out_sw5
	  || !out_sw6 || !out_sw7 || !out_sw8 || !out_sw9 || !out_sw10
	  || !inmp1 || !inmp4 || !inmp8 || !outmp_sw1 || !outmp_sw4 || !outmp_sw8) {

    	  if (in) sds_free(in);
    	  if (img_in) sds_free(img_in);
          if (out) sds_free(out);
          if (outcacheable) sds_free(outcacheable);

          if (kernel1) sds_free(kernel1);
		  if (kernel2) sds_free(kernel2);
		  if (kernel3) sds_free(kernel3);
		  if (kernel4) sds_free(kernel4);
		  if (kernel5) sds_free(kernel5);
		  if (kernel6) sds_free(kernel6);
		  if (kernel7) sds_free(kernel7);
		  if (kernel8) sds_free(kernel8);
		  if (kernel9) sds_free(kernel9);
		  if (kernel10) sds_free(kernel10);

          if (in1) sds_free(in1);
		  if (in2) sds_free(in2);
		  if (in3) sds_free(in3);
		  if (in4) sds_free(in4);
		  if (in5) sds_free(in5);
		  if (in6) sds_free(in6);
		  if (in7) sds_free(in7);
		  if (in8) sds_free(in8);
		  if (in9) sds_free(in9);
		  if (in10) sds_free(in10);
		  if (inmp1) sds_free(inmp1);
		  if (inmp4) sds_free(inmp4);
		  if (inmp8) sds_free(inmp8);

          if (out_sw1) sds_free(out_sw1);
		  if (out_sw2) sds_free(out_sw2);
		  if (out_sw3) sds_free(out_sw3);
		  if (out_sw4) sds_free(out_sw4);
		  if (out_sw5) sds_free(out_sw5);
		  if (out_sw6) sds_free(out_sw6);
		  if (out_sw7) sds_free(out_sw7);
		  if (out_sw8) sds_free(out_sw8);
		  if (out_sw9) sds_free(out_sw9);
		  if (out_sw10) sds_free(out_sw10);
		  if (outmp_sw1) sds_free(outmp_sw1);
		  if (outmp_sw4) sds_free(outmp_sw4);
		  if (outmp_sw8) sds_free(outmp_sw8);

          return 2;
     }
/*
     init_arrays(kernel1, kernel2, kernel3, kernel4, kernel5,
	 			kernel6, kernel7, kernel8, kernel9, kernel10
				,in1, in2, in3, in4, in5,
	 			in6, in7, in8, in9, in10, img_in
				,out_sw1, out_sw2, out_sw3, out_sw4, out_sw5,
	 			out_sw6, out_sw7, out_sw8, out_sw9, out_sw10
				, inmp1, inmp4, inmp8, outmp_sw1, outmp_sw4, outmp_sw8);

     int test_passed = l1_l2_test(in, out, outcacheable
    		 	 	, kernel1, kernel2, kernel3, kernel4, kernel5,
	 				kernel6, kernel7, kernel8, kernel9, kernel10
					, in1, in2, in3, in4, in5,
					in6, in7, in8, in9, in10
					, out_sw1, out_sw2, out_sw3, out_sw4, out_sw5,
					out_sw6, out_sw7, out_sw8, out_sw9, out_sw10
					, inmp1, inmp4, inmp8, outmp_sw1, outmp_sw4, outmp_sw8);

     std::cout << "TEST RESULT: " << (test_passed ? "FAILED" : "PASSED") << std::endl;
*/

     init_lite (kernel1, kernel2, kernel3, kernel4, kernel5, kernel6, kernel7,
     				kernel8, kernel9, kernel10, img_in);

     run_segmentation (in, img_in, out, outcacheable, kernel1, kernel2, kernel3,
     				kernel4, kernel5, kernel6, kernel7, kernel8, kernel9, kernel10);


     webcam_thread_data thread_data;
     pthread_t thread_id;
     char *fb = NULL;
     drm_modeset_buf *buf;
     int fd;

     thread_data.ready = false;
     thread_data.stop = false;

     //==========================================================================
     // Device Set Up
     //==========================================================================
     // Open the DRM device
     if (drm_open(&fd, "/dev/dri/card0")) {
        std::exit(1);
     }

     // Prepare all connectors and CRTCs
     if (drm_prepare(fd, &num_modeset_dev)) {
        close(fd);
        exit(1);
     }

     // Perform mode setting on each found connector + CRTC
     for (drm_modeset_dev *iter = modeset_list; iter; iter = iter->next) {
        iter->saved_crtc = drmModeGetCrtc(fd, iter->crtc);
        buf = &iter->bufs[iter->front_buf];
        if (drmModeSetCrtc(fd, iter->crtc, buf->fb, 0, 0, &iter->conn, 1, &iter->mode)) {
           std::cerr << "Cannot set CRTC for connector " << iter->conn << std::endl;
        }
     }

     //==========================================================================
     // Launch Webcam
     //==========================================================================
     pthread_create(&thread_id, NULL, webcam_thread, (void *)&thread_data);
     while(1) {
        if (thread_data.ready) {
           break;
        }
     }

     unsigned int *stream_i = (unsigned int *)malloc(thread_data.webcam_x_res *
                                                     thread_data.webcam_y_res *
                                                     sizeof(unsigned int));

     unsigned int *stream_o = (unsigned int *)malloc(thread_data.webcam_x_res *
                                                     thread_data.webcam_y_res *
                                                     sizeof(unsigned int));

     uint32_t x_res = thread_data.webcam_x_res;
     uint32_t y_res = thread_data.webcam_y_res;
     uint32_t x_img = 400;
     uint32_t y_img = 400;


     while(1) {
	   fb = (char *)buf->map;
	   draw_something_sw((unsigned int *)fb, (unsigned int *)thread_data.camera, buf->width,
                     buf->height, thread_data.webcam_x_res, thread_data.webcam_y_res, 400, 400, stream_i);
/*
        fb = (char *)buf->map;
  	  write_input_buffer((unsigned int *)thread_data.camera,
  						 buf->width,
  						 buf->height,
  						 x_res,
  						 y_res,
  						 x_img,
  						 y_img,
  						 stream_i);

  	  fb = (char *)buf->map;
  	  draw_output_buffer((unsigned int *)fb,
  						 stream_i,
  						 buf->width,
  						 buf->height,
  						 x_res,
  						 y_res,
  						 x_img,
  						 y_img);
*/
  //	  if(cv::waitKey(30) == 'q') break;
     }



     // Wait for any pending page flip events and unregister the framebuffers
     drm_cleanup(fd, drm_page_flip_event, num_modeset_dev);

     free(stream_i);
     close(fd);



     sds_free(in);
     sds_free(img_in);
     sds_free(out);
     sds_free(outcacheable);

     sds_free(kernel1);
	 sds_free(kernel2);
	 sds_free(kernel3);
	 sds_free(kernel4);
	 sds_free(kernel5);
	 sds_free(kernel6);
	 sds_free(kernel7);
	 sds_free(kernel8);
	 sds_free(kernel9);
	 sds_free(kernel10);

     sds_free(in1);
	 sds_free(in2);
	 sds_free(in3);
	 sds_free(in4);
	 sds_free(in5);
	 sds_free(in6);
	 sds_free(in7);
	 sds_free(in8);
	 sds_free(in9);
	 sds_free(in10);
	 sds_free(inmp1);
	 sds_free(inmp4);
	 sds_free(inmp8);

     sds_free(out_sw1);
	 sds_free(out_sw2);
	 sds_free(out_sw3);
	 sds_free(out_sw4);
	 sds_free(out_sw5);
	 sds_free(out_sw6);
	 sds_free(out_sw7);
	 sds_free(out_sw8);
	 sds_free(out_sw9);
	 sds_free(out_sw10);
	 sds_free(outmp_sw1);
	 sds_free(outmp_sw4);
	 sds_free(outmp_sw8);

     return 0;

}
