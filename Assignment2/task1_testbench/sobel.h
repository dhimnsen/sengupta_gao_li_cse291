#ifndef _SOBEL_H_
#define _SOBEL_H_
#include <stdio.h>
#include <stdlib.h>
#include "hls_stream.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include "ap_int.h"
using namespace std;

const int debug=1;
typedef ap_uint<8> DTYPE;

const int SIZE_w = 480;
const int SIZE_h = 640;

void sobel_hw(DTYPE in[SIZE_w][SIZE_h], DTYPE out[SIZE_w][SIZE_h]);

#endif
