
#ifndef _RUNCONV_H_
#define _RUNCONV_H_

#include "l1_l2.h"
#include "utility.h"

void init_arrays (MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10,
				MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10, MIDTYPE *img_in,
				MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10,
				MIDTYPE inmp1[301056], MIDTYPE inmp4[301056], MIDTYPE inmp8[301056],
				MIDTYPE outmp_sw1[301056], MIDTYPE outmp_sw4[301056], MIDTYPE outmp_sw8[301056]);

void init_lite (MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10, MIDTYPE *img_in);

void l1_l2_mp1_run (MIDTYPE *in, MIDTYPE *out);

void l1_l2_mp4_run (MIDTYPE *in, MIDTYPE *out);

void l1_l2_mp8_run (MIDTYPE *in, MIDTYPE *out);

void l1_l2_33_hw_run (MIDTYPE *in, MIDTYPE *out, MIDTYPE *kernel,
						Settings *info, int in_size, int out_size, int param_size);

void upSamplingNew (MIDTYPE *in, volatile MIDTYPE *out);

void upSampling (MIDTYPE *in, MIDTYPE *out);

void run_segmentation (MIDTYPE *in, MIDTYPE *img_in, MIDTYPE *out, MIDTYPE *outcacheable
				, MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10);

#endif
