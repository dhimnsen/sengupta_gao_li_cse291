#ifndef _MATMUL_H_
#define _MATMUL_H_
#include "hls_stream.h"
#include "ap_int.h"
#include<iostream>
#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

const int debug=1;

typedef ap_int<9> DTYPE;
const int SIZE=128;

void matmul_hw(DTYPE matrix1[SIZE][SIZE], DTYPE matrix2[SIZE][SIZE], DTYPE out[SIZE][SIZE]);

#endif
