#ifndef _MATMUL_H_
#define _MATMUL_H_
#include "hls_stream.h"
#include<iostream>
#include <iostream>
#include <iomanip>
#include <vector>
#include "ap_int.h"
using namespace std;

const int debug=1;

typedef int DTYPE;
const int SIZE=128;

void matmul_hw(DTYPE matrix1[SIZE][SIZE], DTYPE matrix2[SIZE][SIZE], DTYPE out[SIZE][SIZE]);

#endif
