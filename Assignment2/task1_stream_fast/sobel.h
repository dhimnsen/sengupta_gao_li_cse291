#ifndef _SOBEL_H_
#define _SOBEL_H_
#include <stdio.h>
#include <stdlib.h>
#include "hls_stream.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include "ap_int.h"
using namespace std;

typedef ap_uint<8> DTYPE;
typedef ap_uint<64> NTYPE;
typedef hls::stream<NTYPE> MYSTREAM;

const int SIZE_w = 480;
const int SIZE_h = 640;

void sobel_hw(MYSTREAM *in, MYSTREAM *out);

#endif
