/**
 *  CSE 291 Final Project
 */


#include "l1_l2.h"
using namespace std;

void l1_l2_sw(DTYPE in[SIZE1][SIZE1][3], DTYPE out[SIZE2][SIZE2][96],
		const PTYPE conv[7][7][3][96], const PTYPE offset[96]) {

    for(int i=0; i<SIZE1; i+=2) {
		for (int j = 0; j < SIZE1; j+=2) {
			for (int k = 0; k < 96; k++){
				int result = 0;
				for (int s = 0; s < 7; s++){
					for (int t = 0; t < 7; t++){
						if (i-3+s<0 || i-3+s>=SIZE1 || j-3+t<0 || j-3+t>=SIZE1){
							continue;
						}
						result += ((int)conv[s][t][0][k]*(int)in[i-3+s][j-3+t][0]
								+ (int)conv[s][t][1][k]*(int)in[i-3+s][j-3+t][1]
								+ (int)conv[s][t][2][k]*(int)in[i-3+s][j-3+t][2]);
					}
				}

				result += offset[k];

				if (result > 255) result = 255;
				if (result < 0) result = 0;
				out[i/2][j/2][k] = (DTYPE)result;
			}

		}
	}

}


bool l1_l2_sw_33(const DTYPE in[56][56][512], const PTYPE conv[3][3][64][256],
		const PTYPE offset[256], const PTYPE small_conv[64][256],
		const PTYPE small_offset[256], const PTYPE first_conv[512][64],
		const PTYPE first_offset[64], Settings *info,
		RAMTYPE *in_33, RAMTYPE *out_33, RAMTYPE *param_33) {


	DTYPE middle[56][56][64];
	DTYPE sw_out_pic_33[56][56][256];
	DTYPE small_sw_out_pic_33[56][56][256];

	const int data_size = info->dataSize;
	const int out_ch = info->outputChannel;
	const int in_ch = info->inputChannel;
	const int first_ch = info->firstChannel;

	for (int i = 0; i < data_size; i++){
		for (int j = 0; j < data_size; j++){
			for (int k = 0; k < in_ch; k++){
				int result = 0;
				for (int l = 0; l < first_ch; l++){
					result += ((int)first_conv[l][k]*(int)in[i][j][l]);
				}
				result += first_offset[k];
				if (result > 255) result = 255;
				if (result < 0) result = 0;
				middle[i][j][k] = (DTYPE)result;
			}
		}
	}

    for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < out_ch; k++){
				int result = 0;
				for (int s = 0; s < 3; s++){
					for (int t = 0; t < 3; t++){
						if (i-1+s<0 || i-1+s>=data_size || j-1+t<0 || j-1+t>=data_size){
							continue;
						}
						for (int l = 0; l < in_ch; l++){
							result += ((int)conv[s][t][l][k]*(int)middle[i-1+s][j-1+t][l]);
						}
					}
				}

				int small_result = 0;
				for (int l = 0; l < in_ch; l++){
					small_result += ((int)small_conv[l][k]*(int)middle[i][j][l]);
				}

				result += offset[k];
				small_result += small_offset[k];

				if (result > 255) result = 255;
				if (result < 0) result = 0;
				sw_out_pic_33[i][j][k] = (DTYPE)result;

				if (small_result > 255) small_result = 255;
				if (small_result < 0) small_result = 0;
				small_sw_out_pic_33[i][j][k] = (DTYPE)small_result;
			}

		}
	}


	DTYPE out_pic_33[56][56][256];
	DTYPE small_out_pic_33[56][56][256];

	int inCounter = 0;

	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < first_ch; k+=2){
				RAMTYPE temp = ((((RAMTYPE)in[i][j][k])<<8)&0xFF00) | (((RAMTYPE)in[i][j][k+1])&0x00FF);
				in_33[inCounter] = temp;
				inCounter++;
			}
		}
	}

	int paCounter = 0;

	for (int s = 0; s < 3; s++){
		for (int t = 0; t < 3; t++){
			for (int i = 0; i < out_ch; i++){
				for (int j = 0; j < in_ch; j+=2){
					RAMTYPE temp = ((((RAMTYPE)conv[s][t][j][i])<<8)&0x3F00) | (((RAMTYPE)conv[s][t][j+1][i])&0x003F);
					param_33[paCounter] = temp;
					paCounter++;
				}
			}
		}
	}

	for (int i = 0; i < out_ch; i++){
		param_33[paCounter] = offset[i];
		paCounter++;
	}

	for (int i = 0; i < out_ch; i++){
		for (int j = 0; j < in_ch; j+=2){
			RAMTYPE temp = ((((RAMTYPE)small_conv[j][i])<<8)&0x3F00) | (((RAMTYPE)small_conv[j+1][i])&0x003F);
			param_33[paCounter] = temp;
			paCounter++;
		}
	}

	for (int i = 0; i < out_ch; i++){
		param_33[paCounter] = small_offset[i];
		paCounter++;
	}

	for (int i = 0; i < in_ch; i++){
		for (int j = 0; j < first_ch; j+=2){
			RAMTYPE temp = ((((RAMTYPE)first_conv[j][i])<<8)&0x3F00) | (((RAMTYPE)first_conv[j+1][i])&0x003F);
			param_33[paCounter] = temp;
			paCounter++;
		}
	}

	for (int i = 0; i < in_ch; i++){
		param_33[paCounter] = first_offset[i];
		paCounter++;
	}

	RAMTYPE info_array_33[10];
	info_array_33[0] = (RAMTYPE)(info->dataSize);
	info_array_33[1] = (RAMTYPE)(info->inputChannel);
	info_array_33[2] = (RAMTYPE)(info->outputChannel);
	info_array_33[3] = (RAMTYPE)(info->isFirst);
	info_array_33[4] = (RAMTYPE)(info->isLast);
	info_array_33[5] = (RAMTYPE)(info->startSize);
	info_array_33[6] = (RAMTYPE)(info->endSize);
	info_array_33[7] = (RAMTYPE)(info->loopDivider);
	info_array_33[8] = (RAMTYPE)(info->firstChannel);
	info_array_33[9] = (RAMTYPE)(info->firstDivider);

	const int in_33_size = data_size*data_size*first_ch/2;
	const int out_33_size = data_size*data_size*out_ch;
	const int param_33_size = 3*3*out_ch*in_ch/2 + out_ch
			+ out_ch*in_ch/2 + out_ch + in_ch*first_ch/2 + in_ch;



	l1_l2_hw(in_33, out_33, param_33, info_array_33, in_33_size, out_33_size, param_33_size);

	int outCounter = 0;
	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < out_ch; k++){
				RAMTYPE temp = out_33[outCounter];
				outCounter++;
				out_pic_33[i][j][k] = (DTYPE)(temp&0x00FF);
				small_out_pic_33[i][j][k] = (DTYPE)((temp>>8)&0x00FF);
			}
		}
	}

	bool correct_33 = 1;
	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < out_ch; k++){
				if (out_pic_33[i][j][k] != sw_out_pic_33[i][j][k]) {
					printf("hehe");
					correct_33 = 0;
				}
				if (small_out_pic_33[i][j][k] != small_sw_out_pic_33[i][j][k]) {
					printf("sad");
					correct_33 = 0;
				}
			}
		}
	}


	return correct_33;

}

bool l1_l2_sw_last(const DTYPE in[56][56][512], const PTYPE first_conv[512][64],
		const PTYPE first_offset[64], Settings *info,
		RAMTYPE *in_33, RAMTYPE *out_33, RAMTYPE *param_33) {


	DTYPE middle[56][56][64];

	const int data_size = info->dataSize;
	const int in_ch = info->inputChannel;
	const int first_ch = info->firstChannel;

	for (int i = 0; i < data_size; i++){
		for (int j = 0; j < data_size; j++){
			for (int k = 0; k < in_ch; k++){
				int result = 0;
				for (int l = 0; l < first_ch; l++){
					result += ((int)first_conv[l][k]*(int)in[i][j][l]);
				}
				result += first_offset[k];
				if (result > 255) result = 255;
				if (result < 0) result = 0;
				middle[i][j][k] = (DTYPE)result;
			}
		}
	}

	DTYPE out_pic_33[56][56][256];

	int inCounter = 0;

	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < first_ch; k+=2){
				RAMTYPE temp = ((((RAMTYPE)in[i][j][k])<<8)&0xFF00) | (((RAMTYPE)in[i][j][k+1])&0x00FF);
				in_33[inCounter] = temp;
				inCounter++;
			}
		}
	}

	int paCounter = 0;

	for (int i = 0; i < in_ch; i++){
		for (int j = 0; j < first_ch; j+=2){
			RAMTYPE temp = ((((RAMTYPE)first_conv[j][i])<<8)&0x3F00) | (((RAMTYPE)first_conv[j+1][i])&0x003F);
			param_33[paCounter] = temp;
			paCounter++;
		}
	}

	for (int i = 0; i < in_ch; i++){
		param_33[paCounter] = first_offset[i];
		paCounter++;
	}

	RAMTYPE info_array_33[10];
	info_array_33[0] = (RAMTYPE)(info->dataSize);
	info_array_33[1] = (RAMTYPE)(info->inputChannel);
	info_array_33[2] = (RAMTYPE)(info->outputChannel);
	info_array_33[3] = (RAMTYPE)(info->isFirst);
	info_array_33[4] = (RAMTYPE)(info->isLast);
	info_array_33[5] = (RAMTYPE)(info->startSize);
	info_array_33[6] = (RAMTYPE)(info->endSize);
	info_array_33[7] = (RAMTYPE)(info->loopDivider);
	info_array_33[8] = (RAMTYPE)(info->firstChannel);
	info_array_33[9] = (RAMTYPE)(info->firstDivider);

	const int in_33_size = data_size*data_size*first_ch/2;
	const int out_33_size = data_size*data_size*in_ch;
	const int param_33_size = in_ch*first_ch/2 + in_ch;

	l1_l2_hw(in_33, out_33, param_33, info_array_33, in_33_size, out_33_size, param_33_size);

	int outCounter = 0;
	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < in_ch; k++){
				out_pic_33[i][j][k] = (DTYPE)((out_33[outCounter])&0x00FF);
				outCounter++;
			}
		}
	}

	bool correct_33 = 1;
	for(int i=0; i<data_size; i++) {
		for (int j = 0; j < data_size; j++) {
			for (int k = 0; k < in_ch; k++){
				if (out_pic_33[i][j][k] != middle[i][j][k]) {
					printf("sadddd");
					correct_33 = 0;
				}
			}
		}
	}

	return correct_33;

}


int main() {

	int value = 0;
	bool correct = 1;


	FILE * fp = fopen("car.orig","r");
	DTYPE in_pic[SIZE1][SIZE1][3];

	for(int i=0; i<SIZE1; i++) {
		for (int j = 0; j < SIZE1; j++) {
			fscanf(fp, "%d", &value);
			in_pic[i][j][0] = (DTYPE)value;
			fscanf(fp, "%d", &value);
			in_pic[i][j][1] = (DTYPE)value;
			fscanf(fp, "%d", &value);
			in_pic[i][j][2] = (DTYPE)value;
		}
	}
	fclose(fp);

	FILE * fpc = fopen("conv.orig","r");
	value = 0;
	PTYPE in_conv[7][7][3][96];
	for(int i=0; i<7; i++) {
		for (int j = 0; j < 7; j++) {
			for(int k=0; k<3; k++) {
				for(int l=0; l<96; l++) {
					fscanf(fpc, "%d", &value);
					in_conv[i][j][k][l] = (PTYPE)value;
				}
			}

		}
	}

	PTYPE in_conv_offset[96];
	for (int i = 0; i < 96; i++){
		fscanf(fpc, "%d", &value);
		in_conv_offset[i] = (PTYPE)value;
	}

	fclose(fpc);

	DTYPE out_pic[SIZE2][SIZE2][96];
	DTYPE sw_out_pic[SIZE2][SIZE2][96];

	l1_l2_sw(in_pic, sw_out_pic, in_conv, in_conv_offset);

	RAMTYPE *in = new RAMTYPE[1204224];
	RAMTYPE *out = new RAMTYPE[1204224];
	int inCounter = 0;

	for(int i=0; i<SIZE1; i++) {
		for (int j = 0; j < SIZE1; j++) {
			in[inCounter] = in_pic[i][j][0];
			inCounter++;
			in[inCounter] = in_pic[i][j][1];
			inCounter++;
			in[inCounter] = in_pic[i][j][2];
			inCounter++;
		}
	}

	Settings info(224, 3, 96, true, false, -3, 227, 1, 3, 1);

	RAMTYPE *param = new RAMTYPE[98880];
	int paCounter = 0;

	for (int s = 0; s < 7; s++){
		for (int t = 0; t < 7; t++){
			for (int i = 0; i < info.outputChannel; i++){
				for (int j = 0; j < info.inputChannel; j++){
					param[paCounter] = in_conv[s][t][j][i];
					paCounter++;
				}
			}
		}
	}

	for (int i = 0; i < info.outputChannel; i++){
		param[paCounter] = in_conv_offset[i];
		paCounter++;
	}

	RAMTYPE info_array[10];
	info_array[0] = (RAMTYPE)(info.dataSize);
	info_array[1] = (RAMTYPE)(info.inputChannel);
	info_array[2] = (RAMTYPE)(info.outputChannel);
	info_array[3] = (RAMTYPE)(info.isFirst);
	info_array[4] = (RAMTYPE)(info.isLast);
	info_array[5] = (RAMTYPE)(info.startSize);
	info_array[6] = (RAMTYPE)(info.endSize);
	info_array[7] = (RAMTYPE)(info.loopDivider);
	info_array[8] = (RAMTYPE)(info.firstChannel);
	info_array[9] = (RAMTYPE)(info.firstDivider);

	const int in_size = SIZE1*SIZE1*3;
	const int out_size = SIZE2*SIZE2*96;
	const int param_size = 7*7*96*3 + 96;

	l1_l2_hw(in, out, param, info_array, in_size, out_size, param_size);

	int outCounter = 0;
	for(int i=0; i<SIZE2; i++) {
		for (int j = 0; j < SIZE2; j++) {
			for (int k = 0; k < 96; k++){
				out_pic[i][j][k] = (DTYPE)((out[outCounter])&0x00FF);
				outCounter++;
			}
		}
	}

	FILE * fpw = fopen("car.out","w");

	for(int i=0; i<SIZE2; i++) {
		for (int j = 0; j < SIZE2; j++) {
			for (int k = 0; k < 96; k++){
				fprintf(fpw,"%d\n",(int)(out_pic[i][j][k]));
				if (out_pic[i][j][k] != sw_out_pic[i][j][k]) {
					//printf("Error: True %d After %d\n", (int)(sw_out_pic[i][j][k]), (int)(out_pic[i][j][k]));
					correct = 0;
				}
			}
		}
	}	

	fclose(fpw);


	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 1\n");
		return 1;
	}

/////////////////////////////////////////////////////////////////////////////////////////////////

	FILE * fp_33 = fopen("car.orig","r");
	value = 0;
	DTYPE in_pic_33[56][56][512];

	for(int i=0; i<56; i++) {
		for (int j = 0; j < 56; j++) {
			for (int k = 0; k < 512; k++){
				fscanf(fp_33, "%d", &value);
				in_pic_33[i][j][k] = (DTYPE)value;
			}
		}
	}
	fclose(fp_33);

	FILE * fpc_33 = fopen("conv.orig","r");
	value = 0;
	PTYPE in_conv_33[3][3][64][256];
	for(int i=0; i<3; i++) {
		for (int j = 0; j < 3; j++) {
			for(int k=0; k<64; k++) {
				for(int l=0; l<256; l++) {
					fscanf(fpc_33, "%d", &value);
					in_conv_33[i][j][k][l] = (PTYPE)value;
				}
			}

		}
	}

	PTYPE in_conv_offset_33[256];
	for (int i = 0; i < 256; i++){
		fscanf(fpc_33, "%d", &value);
		in_conv_offset_33[i] = (PTYPE)value;
	}

	PTYPE small_in_conv_33[64][256];
	for(int k=0; k<64; k++) {
		for(int l=0; l<256; l++) {
			fscanf(fpc_33, "%d", &value);
			small_in_conv_33[k][l] = (PTYPE)value;
		}
	}


	PTYPE small_in_conv_offset_33[256];
	for (int i = 0; i < 256; i++){
		fscanf(fpc_33, "%d", &value);
		small_in_conv_offset_33[i] = (PTYPE)value;
	}

	PTYPE first_in_conv_33[512][64];
	for(int k=0; k<512; k++) {
		for(int l=0; l<64; l++) {
			fscanf(fpc_33, "%d", &value);
			first_in_conv_33[k][l] = (PTYPE)value;
		}
	}


	PTYPE first_in_conv_offset_33[64];
	for (int i = 0; i < 64; i++){
		fscanf(fpc_33, "%d", &value);
		first_in_conv_offset_33[i] = (PTYPE)value;
	}

	fclose(fpc_33);


	RAMTYPE *in_33 = new RAMTYPE[1204224];
	RAMTYPE *out_33 = new RAMTYPE[1204224];
	RAMTYPE *param_33 = new RAMTYPE[98880];


	Settings info_2(56, 16, 64, false, false, -1, 57, 1, 96, 3);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_2,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 2\n");
		return 1;
	}

	Settings info_3(56, 16, 64, false, false, -1, 57, 1, 128, 4);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_3,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 3\n");
		return 1;
	}

	Settings info_4(56, 32, 128, false, false, -1, 57, 2, 128, 4);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_4,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 4\n");
		return 1;
	}

	Settings info_5(28, 32, 128, false, false, -1, 29, 2, 256, 8);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_5,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 5\n");
		return 1;
	}

	Settings info_6(28, 48, 192, false, false, -1, 29, 3, 256, 8);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_6,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 6\n");
		return 1;
	}

	Settings info_7(28, 48, 192, false, false, -1, 29, 3, 384, 12);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_7,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 7\n");
		return 1;
	}

	Settings info_8(28, 64, 256, false, false, -1, 29, 4, 384, 12);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_8,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 8\n");
		return 1;
	}

	Settings info_9(14, 64, 256, false, false, -1, 15, 4, 512, 16);
	correct = l1_l2_sw_33(in_pic_33, in_conv_33, in_conv_offset_33, small_in_conv_33,
			small_in_conv_offset_33, first_in_conv_33, first_in_conv_offset_33, &info_9,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 9\n");
		return 1;
	}

	Settings info_10(14, 21, 21, false, true, 0, 14, 0, 512, 16);
	correct = l1_l2_sw_last(in_pic_33, first_in_conv_33, first_in_conv_offset_33, &info_10,
			in_33, out_33, param_33);
	if (correct == 0) {
		printf("FAIL: Output DOES NOT match the sw output on layer 10\n");
		return 1;
	}

	printf("PASS: The output matches the sw output!\n");
			return 0;

}
