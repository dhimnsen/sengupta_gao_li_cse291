
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "l1_l2.h"
#include "sds_lib.h"

#ifndef NUM_TESTS
#define NUM_TESTS 1
#endif

class perf_counter
{
public:
     uint64_t tot, cnt, calls;
     perf_counter() : tot(0), cnt(0), calls(0) {};
     inline void reset() { tot = cnt = calls = 0; }
     inline void start() { cnt = sds_clock_counter(); calls++; };
     inline void stop() { tot += (sds_clock_counter() - cnt); };
     inline uint64_t avg_cpu_cycles() { return ((tot+(calls>>1)) / calls); };
};

static void init_arrays (MIDTYPE *in, MIDTYPE *kernel) {

	cout << "start init arrays" << endl;

	FILE * fp = fopen("car.orig","r");
	MIDTYPE value1, value2, value3, value4;
	for(int i = 0; i < 602112; i++) {
		fscanf(fp, "%d", &value1);
		fscanf(fp, "%d", &value2);
		fscanf(fp, "%d", &value3);
		fscanf(fp, "%d", &value4);
		in[i] = ((value1<<24)&0xFF000000) | ((value2<<16)&0x00FF0000)
				| ((value3<<8)&0x0000FF00) | (value4&0x000000FF);
	}
	fclose(fp);

	FILE * fpc = fopen("conv.orig","r");
	for(int i = 0; i < 50176; i++) {
		fscanf(fpc, "%d", &value1);
		fscanf(fpc, "%d", &value2);
		fscanf(fpc, "%d", &value3);
		fscanf(fpc, "%d", &value4);
		kernel[i] = ((value1<<24)&0x3F000000) | ((value2<<16)&0x003F0000)
				| ((value3<<8)&0x00003F00) | (value4&0x0000003F);
	}
	fclose(fpc);

	cout << "end init arrays" << endl;

}

uint64_t l1_l2_33_hw_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *kernel, Settings *info) {

	MIDTYPE info_array[10];
	info_array[0] = (MIDTYPE)(info->dataSize);
	info_array[1] = (MIDTYPE)(info->inputChannel);
	info_array[2] = (MIDTYPE)(info->outputChannel);
	info_array[3] = (MIDTYPE)(info->isFirst);
	info_array[4] = (MIDTYPE)(info->isLast);
	info_array[5] = (MIDTYPE)(info->startSize);
	info_array[6] = (MIDTYPE)(info->endSize);
	info_array[7] = (MIDTYPE)(info->loopDivider);
	info_array[8] = (MIDTYPE)(info->firstChannel);
	info_array[9] = (MIDTYPE)(info->firstDivider);

	const int data_size = info->dataSize;
	const int out_ch = info->outputChannel;
	const int in_ch = info->inputChannel;
	const int first_ch = info->firstChannel;
	const bool is_first = info->isFirst;
	const bool is_last = info->isLast;

	int in_size;
	int out_size;
	int param_size;

	if (is_first) {
		in_size = data_size*data_size;
		out_size = data_size/2*data_size/2*96/2;
		param_size = 7*7*out_ch + out_ch;
	} else if (is_last) {
		in_size = data_size*data_size*first_ch/4;
		out_size = data_size*data_size*in_ch;
		param_size = in_ch*first_ch/4 + in_ch;
	} else {
		in_size = data_size*data_size*first_ch/4;
		out_size = data_size*data_size*out_ch/2;
		param_size = 3*3*out_ch*in_ch/4 + out_ch
				+ out_ch*in_ch/4 + out_ch + in_ch*first_ch/4 + in_ch;
	}

	perf_counter hw_ctr;
	hw_ctr.start();
	l1_l2_hw(in, out, kernel, info_array, in_size, out_size, param_size);
	hw_ctr.stop();

	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();
    std::cout << "Average number of CPU cycles running in hardware: "
              << hw_cycles << std::endl;

    return hw_cycles;

}

int l1_l2_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *kernel) {

	std::cout << "Testing " << NUM_TESTS << " iterations of l1_l2" << endl;

	bool result = 0;
	uint64_t total_time = 0;
	for (int i = 0; !result && i < NUM_TESTS; i++) {

		Settings info1(224, 3, 96, true, false, -3, 227, 1, 3, 1);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info1);

		Settings info2(56, 16, 64, false, false, -1, 57, 1, 96, 3);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info2);

		Settings info3(56, 16, 64, false, false, -1, 57, 1, 128, 4);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info3);

		Settings info4(56, 32, 128, false, false, -1, 57, 2, 128, 4);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info4);

		Settings info5(28, 32, 128, false, false, -1, 29, 2, 256, 8);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info5);

		Settings info6(28, 48, 192, false, false, -1, 29, 3, 256, 8);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info6);

		Settings info7(28, 48, 192, false, false, -1, 29, 3, 384, 12);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info7);

		Settings info8(28, 64, 256, false, false, -1, 29, 4, 384, 12);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info8);

		Settings info9(14, 64, 256, false, false, -1, 15, 4, 512, 16);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info9);

		Settings info10(14, 21, 21, false, true, 0, 14, 0, 512, 16);
		total_time += l1_l2_33_hw_test (in, out, kernel, &info10);

	}

    std::cout << "Total number of CPU cycles running in hardware: "
              << total_time << std::endl;

	return result;
}

int main (int argc, char* argv[]) {

     MIDTYPE *in, *out, *kernel;

     in = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     kernel = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     
     if (!in || !out || !kernel) {
          if (in) sds_free(in);
          if (out) sds_free(out);
          if (kernel) sds_free(kernel);
          return 2;
     }
     
     init_arrays(in, kernel);

     int test_passed = l1_l2_test(in, out, kernel);
     std::cout << "TEST " << (test_passed ? "FAILED" : "PASSED") << std::endl;
     
     sds_free(in);
     sds_free(out);
     sds_free(kernel);
     
     return (test_passed ? -1 : 0);

}

