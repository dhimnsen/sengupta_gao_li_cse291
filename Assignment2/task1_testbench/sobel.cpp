#include "sobel.h"

void sobel_hw(DTYPE in[SIZE_w][SIZE_h], DTYPE out[SIZE_w][SIZE_h]) {

    for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {

	    	if (j == 0 || j == SIZE_h-1 || i == 0 || i == SIZE_w-1) {
	    		out[i][j] = 0;
	    		continue;
	    	}

	    	int gx = (int)(in[i+1][j-1]) + 2*(int)(in[i+1][j]) + (int)(in[i+1][j+1])
					- ((int)(in[i-1][j-1]) + 2*(int)(in[i-1][j]) + (int)(in[i-1][j+1]));
	    	int gy = (int)(in[i-1][j-1]) + 2*(int)(in[i][j-1]) + (int)(in[i+1][j-1])
					- ((int)(in[i-1][j+1]) + 2*(int)(in[i][j+1]) + (int)(in[i+1][j+1]));
	    	if (gx < 0) gx = -gx;
	    	if (gy < 0) gy = -gy;
	    	int result = gx + gy;
	    	if (result > 255) result = 255;
	    	out[i][j] = (DTYPE) result;

		}
	}

}

