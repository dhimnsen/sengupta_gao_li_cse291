
#ifndef _TESTBENCH_H_
#define _TESTBENCH_H_

#ifndef NUM_TESTS
#define NUM_TESTS 16
#endif

#include "l1_l2.h"
#include "utility.h"

uint64_t l1_l2_mp1_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result);

uint64_t l1_l2_mp4_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result);

uint64_t l1_l2_mp8_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result);

uint64_t l1_l2_33_hw_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *out_sw, MIDTYPE *kernel,
						Settings *info, bool *result, int in_size, int out_size, int param_size);

int l1_l2_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *outcacheable
				, MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10
				, MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10
				, MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10,
				MIDTYPE inmp1[301056], MIDTYPE inmp4[301056], MIDTYPE inmp8[301056],
				MIDTYPE outmp_sw1[301056], MIDTYPE outmp_sw4[301056], MIDTYPE outmp_sw8[301056]);

#endif
