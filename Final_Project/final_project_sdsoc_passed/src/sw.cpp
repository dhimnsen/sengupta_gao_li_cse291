
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "l1_l2.h"
#include "sds_lib.h"

#ifndef NUM_TESTS
#define NUM_TESTS 512
#endif

class perf_counter
{
public:
     uint64_t tot, cnt, calls;
     perf_counter() : tot(0), cnt(0), calls(0) {};
     inline void reset() { tot = cnt = calls = 0; }
     inline void start() { cnt = sds_clock_counter(); calls++; };
     inline void stop() { tot += (sds_clock_counter() - cnt); };
     inline uint64_t avg_cpu_cycles() { return ((tot+(calls>>1)) / calls); };
};

static void init_arrays (MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10,
				MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10,
				MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10) {

	MIDTYPE value;

	cout << "start init kernel arrays" << endl;

	FILE * fpc1 = fopen("/mnt/data/car_kernel_1.out","r");
	FILE * fpc2 = fopen("/mnt/data/car_kernel_2.out","r");
	FILE * fpc3 = fopen("/mnt/data/car_kernel_3.out","r");
	FILE * fpc4 = fopen("/mnt/data/car_kernel_4.out","r");
	FILE * fpc5 = fopen("/mnt/data/car_kernel_5.out","r");
	FILE * fpc6 = fopen("/mnt/data/car_kernel_6.out","r");
	FILE * fpc7 = fopen("/mnt/data/car_kernel_7.out","r");
	FILE * fpc8 = fopen("/mnt/data/car_kernel_8.out","r");
	FILE * fpc9 = fopen("/mnt/data/car_kernel_9.out","r");
	FILE * fpc10 = fopen("/mnt/data/car_kernel_10.out","r");

	for(int i = 0; i < 50176; i++) {
		fscanf(fpc1, "%d", &value);
		kernel1[i] = value;
		fscanf(fpc2, "%d", &value);
		kernel2[i] = value;
		fscanf(fpc3, "%d", &value);
		kernel3[i] = value;
		fscanf(fpc4, "%d", &value);
		kernel4[i] = value;
		fscanf(fpc5, "%d", &value);
		kernel5[i] = value;
		fscanf(fpc6, "%d", &value);
		kernel6[i] = value;
		fscanf(fpc7, "%d", &value);
		kernel7[i] = value;
		fscanf(fpc8, "%d", &value);
		kernel8[i] = value;
		fscanf(fpc9, "%d", &value);
		kernel9[i] = value;
		fscanf(fpc10, "%d", &value);
		kernel10[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);

	cout << "end init kernel arrays" << endl;

	cout << "start init in arrays (To be removed in real application)" << endl;

	fpc1 = fopen("/mnt/data/car_in_1.out","r");
	fpc2 = fopen("/mnt/data/car_in_2.out","r");
	fpc3 = fopen("/mnt/data/car_in_3.out","r");
	fpc4 = fopen("/mnt/data/car_in_4.out","r");
	fpc5 = fopen("/mnt/data/car_in_5.out","r");
	fpc6 = fopen("/mnt/data/car_in_6.out","r");
	fpc7 = fopen("/mnt/data/car_in_7.out","r");
	fpc8 = fopen("/mnt/data/car_in_8.out","r");
	fpc9 = fopen("/mnt/data/car_in_9.out","r");
	fpc10 = fopen("/mnt/data/car_in_10.out","r");

	for(int i = 0; i < 602112; i++) {
		fscanf(fpc1, "%d", &value);
		in1[i] = value;
		fscanf(fpc2, "%d", &value);
		in2[i] = value;
		fscanf(fpc3, "%d", &value);
		in3[i] = value;
		fscanf(fpc4, "%d", &value);
		in4[i] = value;
		fscanf(fpc5, "%d", &value);
		in5[i] = value;
		fscanf(fpc6, "%d", &value);
		in6[i] = value;
		fscanf(fpc7, "%d", &value);
		in7[i] = value;
		fscanf(fpc8, "%d", &value);
		in8[i] = value;
		fscanf(fpc9, "%d", &value);
		in9[i] = value;
		fscanf(fpc10, "%d", &value);
		in10[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);

	cout << "end init in arrays" << endl;

	cout << "start init out arrays (To be removed in real application)" << endl;

	fpc1 = fopen("/mnt/data/car_out_1.out","r");
	fpc2 = fopen("/mnt/data/car_out_2.out","r");
	fpc3 = fopen("/mnt/data/car_out_3.out","r");
	fpc4 = fopen("/mnt/data/car_out_4.out","r");
	fpc5 = fopen("/mnt/data/car_out_5.out","r");
	fpc6 = fopen("/mnt/data/car_out_6.out","r");
	fpc7 = fopen("/mnt/data/car_out_7.out","r");
	fpc8 = fopen("/mnt/data/car_out_8.out","r");
	fpc9 = fopen("/mnt/data/car_out_9.out","r");
	fpc10 = fopen("/mnt/data/car_out_10.out","r");

	for(int i = 0; i < 602112; i++) {
		fscanf(fpc1, "%d", &value);
		out_sw1[i] = value;
		fscanf(fpc2, "%d", &value);
		out_sw2[i] = value;
		fscanf(fpc3, "%d", &value);
		out_sw3[i] = value;
		fscanf(fpc4, "%d", &value);
		out_sw4[i] = value;
		fscanf(fpc5, "%d", &value);
		out_sw5[i] = value;
		fscanf(fpc6, "%d", &value);
		out_sw6[i] = value;
		fscanf(fpc7, "%d", &value);
		out_sw7[i] = value;
		fscanf(fpc8, "%d", &value);
		out_sw8[i] = value;
		fscanf(fpc9, "%d", &value);
		out_sw9[i] = value;
		fscanf(fpc10, "%d", &value);
		out_sw10[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);

	cout << "end init out arrays" << endl;

}

uint64_t l1_l2_33_hw_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *out_sw, MIDTYPE *kernel,
						Settings *info, bool *result, int in_size, int out_size, int param_size) {

	MIDTYPE info_array[10];
	info_array[0] = (MIDTYPE)(info->dataSize);
	info_array[1] = (MIDTYPE)(info->inputChannel);
	info_array[2] = (MIDTYPE)(info->outputChannel);
	info_array[3] = (MIDTYPE)(info->isFirst);
	info_array[4] = (MIDTYPE)(info->isLast);
	info_array[5] = (MIDTYPE)(info->startSize);
	info_array[6] = (MIDTYPE)(info->endSize);
	info_array[7] = (MIDTYPE)(info->loopDivider);
	info_array[8] = (MIDTYPE)(info->firstChannel);
	info_array[9] = (MIDTYPE)(info->firstDivider);

	const bool is_first = info->isFirst;

	perf_counter hw_ctr;
	hw_ctr.start();
	l1_l2_hw(in, out, kernel, info_array, in_size, out_size, param_size);
	hw_ctr.stop();

	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();
    //std::cout << "Average number of CPU cycles running in hardware: "
    //          << hw_cycles << std::endl;

	bool correct = true;
	for (int i = 0; i < out_size; i++){
		if (out[i] != out_sw[i]){
			if (is_first) {
				if ((out[i]&0x00FF00FF) == (out_sw[i]&0x00FF00FF)) continue;
			}
			cout << "Correct: " << out_sw[i] << "Wrong: " << out[i] << endl;
			correct = false;
		}
	}

	if (correct == false) {
		cout << "The layer above has error!" << endl;
		*result = 1;
	}

    return hw_cycles;

}

int l1_l2_test (MIDTYPE *out, MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10
				, MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10
				, MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10) {

	std::cout << "Testing " << NUM_TESTS << " iterations of l1_l2" << endl;

	bool result = 0;
	uint64_t total_time = 0;

	Settings* info[10];

	(info[0]) = new Settings(224, 3, 96, true, false, -3, 227, 1, 3, 1);
	(info[1]) = new Settings(56, 16, 64, false, false, -1, 57, 1, 96, 3);
	(info[2]) = new Settings(56, 16, 64, false, false, -1, 57, 1, 128, 4);
	(info[3]) = new Settings(56, 32, 128, false, false, -1, 57, 2, 128, 4);
	(info[4]) = new Settings(28, 32, 128, false, false, -1, 29, 2, 256, 8);
	(info[5]) = new Settings(28, 48, 192, false, false, -1, 29, 3, 256, 8);
	(info[6]) = new Settings(28, 48, 192, false, false, -1, 29, 3, 384, 12);
	(info[7]) = new Settings(28, 64, 256, false, false, -1, 29, 4, 384, 12);
	(info[8]) = new Settings(14, 64, 256, false, false, -1, 15, 4, 512, 16);
	(info[9]) = new Settings(14, 21, 21, false, true, 0, 14, 0, 512, 16);

	int in_size[10];
	int out_size[10];
	int param_size[10];

	for (int i = 0; i < 10; i++) {

		const int data_size = info[i]->dataSize;
		const int out_ch = info[i]->outputChannel;
		const int in_ch = info[i]->inputChannel;
		const int first_ch = info[i]->firstChannel;

		if (i == 0) {
			in_size[i] = data_size*data_size;
			out_size[i] = data_size/2*data_size/2*96/2;
			param_size[i] = 7*7*out_ch + out_ch;
		} else if (i == 9) {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*in_ch;
			param_size[i] = in_ch*first_ch/4 + in_ch;
		} else {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*out_ch/2;
			param_size[i] = 3*3*out_ch*in_ch/4 + out_ch
					+ out_ch*in_ch/4 + out_ch + in_ch*first_ch/4 + in_ch;
		}

	}

	int progress = -1;

	for (int i = 0; !result && i < NUM_TESTS; i++) {

		total_time += l1_l2_33_hw_test (in1, out, out_sw1, kernel1, info[0], &result, in_size[0], out_size[0], param_size[0]);
		total_time += l1_l2_33_hw_test (in2, out, out_sw2, kernel2, info[1], &result, in_size[1], out_size[1], param_size[1]);
		total_time += l1_l2_33_hw_test (in3, out, out_sw3, kernel3, info[2], &result, in_size[2], out_size[2], param_size[2]);
		total_time += l1_l2_33_hw_test (in4, out, out_sw4, kernel4, info[3], &result, in_size[3], out_size[3], param_size[3]);
		total_time += l1_l2_33_hw_test (in5, out, out_sw5, kernel5, info[4], &result, in_size[4], out_size[4], param_size[4]);
		total_time += l1_l2_33_hw_test (in6, out, out_sw6, kernel6, info[5], &result, in_size[5], out_size[5], param_size[5]);
		total_time += l1_l2_33_hw_test (in7, out, out_sw7, kernel7, info[6], &result, in_size[6], out_size[6], param_size[6]);
		total_time += l1_l2_33_hw_test (in8, out, out_sw8, kernel8, info[7], &result, in_size[7], out_size[7], param_size[7]);
		total_time += l1_l2_33_hw_test (in9, out, out_sw9, kernel9, info[8], &result, in_size[8], out_size[8], param_size[8]);
		total_time += l1_l2_33_hw_test (in10, out, out_sw10, kernel10, info[9], &result, in_size[9], out_size[9], param_size[9]);

		int currentProgress = (ceil)(((double)i)*100/NUM_TESTS);
		if (currentProgress > progress) {
			progress = currentProgress;
			cout << "Finished: " << progress << "%" << endl;
		}

	}

    std::cout << "Total number of CPU cycles running in hardware: "
              << total_time << std::endl;
    std::cout << "Average number of CPU cycles running in hardware: "
              << (uint64_t)(total_time/NUM_TESTS) << std::endl;

	return result;
}

int main (int argc, char* argv[]) {

     MIDTYPE *out, *kernel1, *kernel2, *kernel3, *kernel4, *kernel5
	 		, *kernel6, *kernel7, *kernel8, *kernel9, *kernel10
			, *in1, *in2, *in3, *in4, *in5
			, *in6, *in7, *in8, *in9, *in10
			, *out_sw1, *out_sw2, *out_sw3, *out_sw4, *out_sw5
			, *out_sw6, *out_sw7, *out_sw8, *out_sw9, *out_sw10;

     out = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));

     kernel1 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel2 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel3 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel4 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel5 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel6 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel7 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel8 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel9 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     kernel10 = (MIDTYPE *)sds_alloc(50176 * sizeof(MIDTYPE));
     
     in1 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in2 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in3 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in4 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in5 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in6 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in7 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in8 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in9 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     in10 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));

     out_sw1 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw2 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw3 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw4 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw5 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw6 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw7 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw8 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw9 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));
     out_sw10 = (MIDTYPE *)sds_alloc(602112 * sizeof(MIDTYPE));

     if (!out || !kernel1 || !kernel2 || !kernel3 || !kernel4 || !kernel5
	  || !kernel6 || !kernel7 || !kernel8 || !kernel9 || !kernel10
	  || !in1 || !in2 || !in3 || !in4 || !in5
	  || !in6 || !in7 || !in8 || !in9 || !in10
	  || !out_sw1 || !out_sw2 || !out_sw3 || !out_sw4 || !out_sw5
	  || !out_sw6 || !out_sw7 || !out_sw8 || !out_sw9 || !out_sw10) {

          if (out) sds_free(out);

          if (kernel1) sds_free(kernel1);
		  if (kernel2) sds_free(kernel2);
		  if (kernel3) sds_free(kernel3);
		  if (kernel4) sds_free(kernel4);
		  if (kernel5) sds_free(kernel5);
		  if (kernel6) sds_free(kernel6);
		  if (kernel7) sds_free(kernel7);
		  if (kernel8) sds_free(kernel8);
		  if (kernel9) sds_free(kernel9);
		  if (kernel10) sds_free(kernel10);

          if (in1) sds_free(in1);
		  if (in2) sds_free(in2);
		  if (in3) sds_free(in3);
		  if (in4) sds_free(in4);
		  if (in5) sds_free(in5);
		  if (in6) sds_free(in6);
		  if (in7) sds_free(in7);
		  if (in8) sds_free(in8);
		  if (in9) sds_free(in9);
		  if (in10) sds_free(in10);

          if (out_sw1) sds_free(out_sw1);
		  if (out_sw2) sds_free(out_sw2);
		  if (out_sw3) sds_free(out_sw3);
		  if (out_sw4) sds_free(out_sw4);
		  if (out_sw5) sds_free(out_sw5);
		  if (out_sw6) sds_free(out_sw6);
		  if (out_sw7) sds_free(out_sw7);
		  if (out_sw8) sds_free(out_sw8);
		  if (out_sw9) sds_free(out_sw9);
		  if (out_sw10) sds_free(out_sw10);

          return 2;
     }
     
     init_arrays(kernel1, kernel2, kernel3, kernel4, kernel5,
	 			kernel6, kernel7, kernel8, kernel9, kernel10
				,in1, in2, in3, in4, in5,
	 			in6, in7, in8, in9, in10
				,out_sw1, out_sw2, out_sw3, out_sw4, out_sw5,
	 			out_sw6, out_sw7, out_sw8, out_sw9, out_sw10);

     int test_passed = l1_l2_test(out, kernel1, kernel2, kernel3, kernel4, kernel5,
	 				kernel6, kernel7, kernel8, kernel9, kernel10
					, in1, in2, in3, in4, in5,
					in6, in7, in8, in9, in10
					, out_sw1, out_sw2, out_sw3, out_sw4, out_sw5,
					out_sw6, out_sw7, out_sw8, out_sw9, out_sw10);

     std::cout << "TEST " << (test_passed ? "FAILED" : "PASSED") << std::endl;
     
     sds_free(out);

     sds_free(kernel1);
	 sds_free(kernel2);
	 sds_free(kernel3);
	 sds_free(kernel4);
	 sds_free(kernel5);
	 sds_free(kernel6);
	 sds_free(kernel7);
	 sds_free(kernel8);
	 sds_free(kernel9);
	 sds_free(kernel10);

     sds_free(in1);
	 sds_free(in2);
	 sds_free(in3);
	 sds_free(in4);
	 sds_free(in5);
	 sds_free(in6);
	 sds_free(in7);
	 sds_free(in8);
	 sds_free(in9);
	 sds_free(in10);

     sds_free(out_sw1);
	 sds_free(out_sw2);
	 sds_free(out_sw3);
	 sds_free(out_sw4);
	 sds_free(out_sw5);
	 sds_free(out_sw6);
	 sds_free(out_sw7);
	 sds_free(out_sw8);
	 sds_free(out_sw9);
	 sds_free(out_sw10);
     
     return (test_passed ? -1 : 0);

}
