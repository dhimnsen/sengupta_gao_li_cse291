import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import os
import sys
import cv2
from PIL import Image
from keras.preprocessing.image import *
from keras.utils import plot_model
from keras.models import load_model, Model
import keras.backend as K
from keras.applications.imagenet_utils import preprocess_input
from keras.layers import Input, Convolution2D, MaxPooling2D, Activation, concatenate, Dropout, GlobalAveragePooling2D, \
    warnings

from models import *

sq1x1 = "squeeze1x1"
exp1x1 = "expand1x1"
exp3x3 = "expand3x3"
relu = "relu_"

def modify_weight(data, wbits, wq, obits, oq, name):
    x = data[0]
    y = data[1]
    # wq = wq + 1
    # oq = oq -1
    x = np.clip(np.round(np.multiply(x,2**wq)), -2**(wbits-1), 2**(wbits-1)-1)
    y = np.clip(np.round(np.multiply(y,2**oq)), -2**(obits-1), 2**(obits-1)-1)
    w=[]
    for r in x:
        for c in r:
            for i in c:
                for o in i:
                    w.append(o)
    w = np.array(w)
    np.savetxt("param/" + name + "-w.csv", np.int_(w), delimiter="\n", fmt='%d')
    np.savetxt("param/" + name + "-o.csv", np.int_(y), delimiter="\n", fmt='%d')

    x = np.divide(x, 2**wq)
    y = np.divide(y, 2**oq)
    return [x,y]

def modify_inter(data, bits, q):
    q = q - 2
    return np.divide(np.clip(np.round(np.multiply(data,2**q)), -2**(bits-1), 2**(bits-1)-1), 2**q)

def inference(model_name, weight_file, image_size, image_list, data_dir, label_dir, return_results=True, save_dir=None,
              label_suffix='.png',
              data_suffix='.jpg'):

    if K.image_data_format() == 'channels_first':
        channel_axis = 1
    else:
        channel_axis = 3

    weight_decay=0.

    current_dir = os.path.dirname(os.path.realpath(__file__))
    # mean_value = np.array([104.00699, 116.66877, 122.67892])
    batch_shape = (1, ) + image_size + (3, )
    # print(batch_shape)
    # exit()
    save_path = os.path.join(current_dir, 'Models/'+model_name)
    model_path = os.path.join(save_path, "model.json")
    checkpoint_path = os.path.join(save_path, weight_file)
    # model_path = os.path.join(current_dir, 'model_weights/fcn_atrous/model_change.hdf5')
    # model = FCN_Resnet50_32s((480,480,3))

    config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
    session = tf.Session(config=config)
    K.set_session(session)

    model = globals()[model_name](batch_shape=batch_shape, input_shape=(224, 224, 3))
    model.load_weights(checkpoint_path, by_name=True)

    # print(model.get_config()['layers'])

    # model.summary()

    plot_model(model, to_file='model.png')

    model.layers[1].set_weights(modify_weight(model.layers[1].get_weights(), 8, 7, 8, -4, model.layers[1].name))
    model.layers[4].set_weights(modify_weight(model.layers[4].get_weights(), 8, 6, 8, -4, model.layers[4].name))
    model.layers[6].set_weights(modify_weight(model.layers[6].get_weights(), 8, 7, 8, -4, model.layers[6].name))
    model.layers[7].set_weights(modify_weight(model.layers[7].get_weights(), 8, 7, 8, -4, model.layers[7].name))
    model.layers[11].set_weights(modify_weight(model.layers[11].get_weights(), 8, 7, 8, -4, model.layers[11].name))
    model.layers[13].set_weights(modify_weight(model.layers[13].get_weights(), 8, 7, 8, -4, model.layers[13].name))
    model.layers[14].set_weights(modify_weight(model.layers[14].get_weights(), 8, 7, 8, -4, model.layers[14].name))
    model.layers[18].set_weights(modify_weight(model.layers[18].get_weights(), 8, 7, 8, -4, model.layers[18].name))
    model.layers[20].set_weights(modify_weight(model.layers[20].get_weights(), 8, 8, 8, -4, model.layers[20].name))
    model.layers[21].set_weights(modify_weight(model.layers[21].get_weights(), 8, 8, 8, -4, model.layers[21].name))
    model.layers[26].set_weights(modify_weight(model.layers[26].get_weights(), 8, 8, 8, -4, model.layers[26].name))
    model.layers[28].set_weights(modify_weight(model.layers[28].get_weights(), 8, 9, 8, -3, model.layers[28].name))
    model.layers[29].set_weights(modify_weight(model.layers[29].get_weights(), 8, 9, 8, -3, model.layers[29].name))
    model.layers[33].set_weights(modify_weight(model.layers[33].get_weights(), 8, 8, 8, -3, model.layers[33].name))
    model.layers[35].set_weights(modify_weight(model.layers[35].get_weights(), 8, 9, 8, -2, model.layers[35].name))
    model.layers[36].set_weights(modify_weight(model.layers[36].get_weights(), 8, 9, 8, -2, model.layers[36].name))
    model.layers[40].set_weights(modify_weight(model.layers[40].get_weights(), 8, 8, 8, -2, model.layers[40].name))
    model.layers[42].set_weights(modify_weight(model.layers[42].get_weights(), 8, 9, 8, -1, model.layers[42].name))
    model.layers[43].set_weights(modify_weight(model.layers[43].get_weights(), 8, 9, 8, -1, model.layers[43].name))
    model.layers[47].set_weights(modify_weight(model.layers[47].get_weights(), 8, 9, 8, 0, model.layers[47].name))
    model.layers[49].set_weights(modify_weight(model.layers[49].get_weights(), 8, 9, 8, 1, model.layers[49].name))
    model.layers[50].set_weights(modify_weight(model.layers[50].get_weights(), 8, 9, 8, 1, model.layers[50].name))
    model.layers[55].set_weights(modify_weight(model.layers[55].get_weights(), 8, 9, 8, 1, model.layers[55].name))
    model.layers[57].set_weights(modify_weight(model.layers[57].get_weights(), 8, 10, 8, 3, model.layers[57].name))
    model.layers[58].set_weights(modify_weight(model.layers[58].get_weights(), 8, 10, 8, 3, model.layers[58].name))
    model.layers[63].set_weights(modify_weight(model.layers[63].get_weights(), 8, 8, 8, 3, model.layers[63].name))

    


    results = []
    total = 0
    for img_num in image_list:
        img_num = img_num.strip('\n')
        total += 1
        print('#%d: %s' % (total,img_num))
        image = Image.open('/home/dhimnsen/Pictures/21.jpg')  #.keras/datasets/VOC2012/VOCdevkit/VOC2012/JPEGImages/2007_007902.jpg') #'%s/%s%s' % (data_dir, img_num, data_suffix))
        image = image.resize((224,224))

        image = img_to_array(image)  # , data_format='default')
        label = Image.open('%s/%s%s' % (label_dir, img_num, label_suffix))
        label.resize((224,224))
        label_size = label.size

        img_h, img_w = image.shape[0:2]

        # long_side = max(img_h, img_w, image_size[0], image_size[1])
        pad_w = max(image_size[1] - img_w, 0)
        pad_h = max(image_size[0] - img_h, 0)
        # image = np.lib.pad(image, ((pad_h/2, pad_h - pad_h/2), (pad_w/2, pad_w - pad_w/2), (0, 0)), 'constant', constant_values=0.)
        # image -= mean_value
        '''img = array_to_img(image, 'channels_last', scale=False)
        img.show()
        exit()'''
        # image = cv2.resize(image, image_size)
        lo = []
        print(image.shape)
        for r in image:
            for c in r:
                for chan in c:
                    lo.append(chan)
        lo = np.array(lo)

        np.savetxt("img.csv", np.int_(lo), delimiter="\n", fmt='%d')

        image = np.expand_dims(image, axis=0)
        # print
        image = preprocess_input(image)
        img_input = Input(batch_shape=batch_shape)
        x = Convolution2D(64, (3, 3), strides=(2, 2), padding='valid', name='conv1', W_regularizer=l2(weight_decay))(img_input)
        x = Activation('relu', name='relu_conv1')(x)
        inter_model = Model(inputs=img_input, outputs=x)
        inter_model.layers[1].set_weights(model.layers[1].get_weights())
        inter_output = inter_model.predict(image, batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -2)
            
        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool1')(inter_in)

        s_id = 'fire' + str(2) + '/'

        x = Convolution2D(16, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        x = Activation('relu', name=s_id + relu + sq1x1)(x)
        inter_model = Model(inputs=inter_in, outputs=x)
        inter_model.layers[2].set_weights(model.layers[4].get_weights())
        inter_output = inter_model.predict(inter_output, batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -2)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))

        left = Convolution2D(64, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(inter_in)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[1].set_weights(model.layers[6].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, -2)

        right = Convolution2D(64, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(inter_in)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[1].set_weights(model.layers[7].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, -2)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        s_id = 'fire' + str(3) + '/'

        x = Convolution2D(16, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        x = Activation('relu', name=s_id + relu + sq1x1)(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[11].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -2)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))

        left = Convolution2D(64, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(inter_in)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[1].set_weights(model.layers[13].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, -2)

        right = Convolution2D(64, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(inter_in)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[1].set_weights(model.layers[14].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, -2)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        s_id = 'fire' + str(4) + '/'

        x = Convolution2D(32, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[18].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -2)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(128, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[20].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, -2)

        right = Convolution2D(128, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[21].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, -2)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')
        x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool4')(x)

        s_id = 'fire' + str(5) + '/'

        x = Convolution2D(32, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[4].set_weights(model.layers[26].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -2)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(128, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[28].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, -1)

        right = Convolution2D(128, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[29].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, -1)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        s_id = 'fire' + str(6) + '/'

        x = Convolution2D(48, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[33].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, -1)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(192, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[35].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, 0)

        right = Convolution2D(192, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[36].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, 0)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        s_id = 'fire' + str(7) + '/'

        x = Convolution2D(48, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[40].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, 0)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(192, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[42].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, 1)

        right = Convolution2D(192, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[43].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, 1)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        s_id = 'fire' + str(8) + '/'

        x = Convolution2D(64, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[47].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, 2)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(256, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[49].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, 3)

        right = Convolution2D(256, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[50].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, 3)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')
        x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2), name='pool8')(x)

        s_id = 'fire' + str(9) + '/'

        x = Convolution2D(64, (1, 1), padding='valid', name=s_id + sq1x1, W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[4].set_weights(model.layers[55].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, 3)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = Activation('relu', name=s_id + relu + sq1x1)(inter_in)

        left = Convolution2D(256, (1, 1), padding='valid', name=s_id + exp1x1, W_regularizer=l2(weight_decay))(x)
        left = Activation('relu', name=s_id + relu + exp1x1)(left)
        inter_model_l = Model(inputs=inter_in, outputs=left)
        inter_model_l.layers[2].set_weights(model.layers[57].get_weights())
        inter_output_l = inter_model_l.predict(inter_output, batch_size=1) 
        inter_output_l = modify_inter(inter_output_l, 8, 5)

        right = Convolution2D(256, (3, 3), padding='same', name=s_id + exp3x3, W_regularizer=l2(weight_decay))(x)
        right = Activation('relu', name=s_id + relu + exp3x3)(right)
        inter_model_r = Model(inputs=inter_in, outputs=right)
        inter_model_r.layers[2].set_weights(model.layers[58].get_weights())
        inter_output_r = inter_model_r.predict(inter_output, batch_size=1) 
        inter_output_r = modify_inter(inter_output_r, 8, 5)


        left = Input(batch_shape=(inter_output_l.shape[0],inter_output_l.shape[1], inter_output_l.shape[2], inter_output_l.shape[3]))
        right = Input(batch_shape=(inter_output_r.shape[0],inter_output_r.shape[1], inter_output_r.shape[2], inter_output_r.shape[3]))
        x = concatenate([left, right], axis=channel_axis, name=s_id + 'concat')

        x = Convolution2D(21, 1, 1, init='he_normal', activation='linear', border_mode='valid', subsample=(1, 1), W_regularizer=l2(weight_decay))(x)
        inter_model = Model(inputs=[left,right], outputs=x)
        inter_model.layers[3].set_weights(model.layers[63].get_weights())
        inter_output = inter_model.predict([inter_output_l, inter_output_r], batch_size=1) 
        inter_output = modify_inter(inter_output, 8, 5)

        inter_in = Input(batch_shape=(inter_output.shape[0],inter_output.shape[1], inter_output.shape[2], inter_output.shape[3]))
        x = BilinearUpSampling2D(target_size=tuple(image_size))(inter_in)
        model1 = Model(inter_in, x)


        # for i in range(len(model.layers)):
        #     layer = model.layers[i]
        #     data = layer.get_weights()
        #     if(len(data)>0):
        #         x = data[0]
        #         y = data[1]
        #         # x = np.clip(np.round(np.multiply(x,2**5)), -2**5, 2**5-1)
        #         # y = np.round(np.multiply(y,2**5))
        #         # x = np.divide(x, 2**5)
        #         # y = np.divide(y, 2**5)
        #         print(layer.name, np.mean(x) + 3* np.std(x))
        #         layer.set_weights([x,y])


        #     if len(data) > 0:
        #         intermediate_layer_model = Model(inputs=model.input,
        #                              outputs=model.layers[i].output)
        #         intermediate_output = intermediate_layer_model.predict(image, batch_size=1)
        #         l = []
        #         for i in intermediate_output:
        #             for j in i:
        #                 for k in j:
        #                     for t in k:
        #                         l.append(t)
        #         l = np.array(l)

                # import matplotlib.pyplot as plt
                # plt.clf()
                # plt.hist(l)
                # plt.savefig('temp.png')
                # raw_input("Press Enter to continue...")


                # print(layer.name, np.amax(l), np.amin(l), np.mean(l), np.std(l))

        # result = model.predict(image, batch_size=1)
        result = model1.predict(inter_output, batch_size=1)
        result = np.argmax(np.squeeze(result), axis=-1).astype(np.uint8)
        np.set_printoptions(threshold=np.nan)
        # print(result)

        result_img = Image.fromarray(result, mode='P')
        result_img.putpalette([
            0, 0, 0,
            255,255,255,
            128,0,0,
            255,0,0,
            255,200,220,
            170,110,40,
            255,150,0,
            255,215,180,
            128,128,0,
            255,235,0,
            255,250,200,
            0,190,0,
            170,255,195,
            0,128,128,
            100,255,255,
            0,0,128,
            67,133,255,
            130,0,150,
            230,190,255,
            255,0,255,
            128,128,128])
        # result_img = result_img.resize(label_size, resample=Image.BILINEAR)
        # result_img = result_img.crop((pad_w/2, pad_h/2, pad_w/2+img_w, pad_h/2+img_h))
        # result_img.show(title='result')
        if return_results:
            results.append(result_img)
        if save_dir:
            result_img.save(os.path.join(save_dir, img_num + '.png'))
    return results

if __name__ == '__main__':
    # model_name = 'AtrousFCN_Resnet50_16s'
    # model_name = 'Atrous_DenseNet'
    # model_name = 'DenseNet_FCN'
    model_name = 'FCN_Squeeze'
    # weight_file = 'checkpoint_weights.hdf5'
    weight_file = 'checkpoint_weights.hdf5'
    image_size = (224, 224)
    data_dir        = os.path.expanduser('~/.keras/datasets/VOC2012/VOCdevkit/VOC2012/JPEGImages')
    label_dir       = os.path.expanduser('~/.keras/datasets/VOC2012/VOCdevkit/VOC2012/SegmentationClass')

    image_list = ['2007_000491']
    results = inference(model_name, weight_file, image_size, image_list, data_dir, label_dir)
    for result in results:
        # print(result)
        result = result.resize((805,536))
        result.show(title='result', command=None)
