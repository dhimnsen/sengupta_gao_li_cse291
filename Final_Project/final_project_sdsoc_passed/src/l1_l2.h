#ifndef _L1_L2_H_
#define _L1_L2_H_
#include <stdio.h>
#include <stdlib.h>
#include "hls_stream.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include "ap_int.h"
using namespace std;

typedef ap_int<6> PTYPE;
typedef ap_uint<8> DTYPE;
typedef int MIDTYPE;
typedef hls::stream<MIDTYPE> MIDSTREAM;

const int SIZE1 = 224;
const int SIZE2 = 112;

class Settings {

public:
	int dataSize;
	int inputChannel;
	int outputChannel;
	bool isFirst;
	bool isLast;
	int startSize;
	int endSize;
	int loopDivider;
	int firstChannel;
	int firstDivider;

	Settings (int data_size, int input_channel,
			int output_channel, bool first, bool last, int start, int end,
			int loop_divider, int first_channel, int first_divider){
		dataSize = data_size;
		inputChannel = input_channel;
		outputChannel = output_channel;
		isFirst = first;
		isLast = last;
		startSize = start;
		endSize = end;
		loopDivider = loop_divider;
		firstChannel = first_channel;
		firstDivider = first_divider;
	}

};

#pragma SDS data copy(in[0:in_size], out[0:out_size], kernel[0:param_size])
#pragma SDS data access_pattern(in:SEQUENTIAL, out:SEQUENTIAL, kernel:SEQUENTIAL)

void l1_l2_hw(MIDTYPE in[602112], MIDTYPE out[602112], MIDTYPE kernel[50176],
		MIDTYPE info[10], const int in_size, const int out_size, const int param_size);

#endif
