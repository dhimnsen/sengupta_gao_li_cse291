
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "l1_l2.h"
#include "runconv.h"
#include "testbench.h"
#include "utility.h"

#include <sys/types.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include <pthread.h>
#include <opencv2/opencv.hpp>
#include <errno.h>

#include "draw.h"
#include "drm.h"

using namespace cv;

struct webcam_thread_data {
	int thread_id;
	volatile char *camera;
	uint32_t webcam_x_res;
	uint32_t webcam_y_res;
	volatile bool ready;
	volatile bool stop;
};

struct custom_data {
	int thread_id;
	MIDTYPE *out;
	unsigned int *old_img;
	volatile MIDTYPE *output_frame;
	drm_modeset_buf *buf;
};

int num_modeset_dev = 0;
drm_modeset_dev *modeset_list = NULL;


int draw_something_sw(unsigned int *fb,
				      unsigned int *img,
				      unsigned int *img_i,
					  unsigned int x_res,
					  unsigned int y_res,
					  unsigned int img_x_res,
					  unsigned int img_y_res,
					  unsigned int img_x,
					  unsigned int img_y) {

	for(unsigned int y = 0; y < y_res; y++) {
		for(unsigned int x = 0; x < x_res; x++) {
			if((x >= img_x) && (x < (img_x + img_x_res)) &&
			   (y >= img_y) && (y < (img_y + img_y_res))) {
				*fb++ = *img++;
			}
			else if((x >= img_x + 400) && (x < (img_x + img_x_res + 400)) &&
					   (y >= img_y) && (y < (img_y + img_y_res))) {
						*fb++ = *img_i++;
					}
			else {
				*fb++ = 0;
			}
		}
	}


	return 0;
}

void *new_thread(void *self_data)
{
   custom_data *data = (custom_data *)self_data;
   char *fb = NULL;

//	cout << "debug6" << endl;
	upSamplingNew(data->out, data->output_frame);
//	cout << "debug7" << endl;
	fb = (char *)(data->buf)->map;
//	cout << "debug8" << endl;
	draw_something_sw((unsigned int *)fb, (unsigned int*)(data->output_frame), (unsigned int*)(data->old_img),
			(data->buf)->width, (data->buf)->height, 224, 224, 400, 400);
//	cout << "debug9" << endl;

}

int main (int argc, char* argv[]) {

	MIDTYPE *in, *img_in, *out, *out1, *outcacheable
			 	, *kernel1, *kernel2, *kernel3, *kernel4, *kernel5
				, *kernel6, *kernel7, *kernel8, *kernel9, *kernel10;

	     in = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
	     img_in = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
	     out = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
	     out1 = (MIDTYPE *)sds_alloc_non_cacheable(301056 * sizeof(MIDTYPE));
	     outcacheable = (MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));

	     kernel1 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel2 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel3 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel4 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel5 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel6 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel7 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel8 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel9 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));
	     kernel10 = (MIDTYPE *)sds_alloc_non_cacheable(50176 * sizeof(MIDTYPE));

	     if (!in || !img_in || !out || !outcacheable
	      || !kernel1 || !kernel2 || !kernel3 || !kernel4 || !kernel5
		  || !kernel6 || !kernel7 || !kernel8 || !kernel9 || !kernel10) {

	    	  if (in) sds_free(in);
	    	  if (img_in) sds_free(img_in);
	          if (out) sds_free(out);
	          if (outcacheable) sds_free(outcacheable);

	          if (kernel1) sds_free(kernel1);
			  if (kernel2) sds_free(kernel2);
			  if (kernel3) sds_free(kernel3);
			  if (kernel4) sds_free(kernel4);
			  if (kernel5) sds_free(kernel5);
			  if (kernel6) sds_free(kernel6);
			  if (kernel7) sds_free(kernel7);
			  if (kernel8) sds_free(kernel8);
			  if (kernel9) sds_free(kernel9);
			  if (kernel10) sds_free(kernel10);

	          return 2;
	     }
	     init_lite (kernel1, kernel2, kernel3, kernel4, kernel5, kernel6, kernel7,
	          				kernel8, kernel9, kernel10, img_in);
	     volatile MIDTYPE* output_frame = (volatile MIDTYPE *)sds_alloc(301056 * sizeof(MIDTYPE));

	     drm_modeset_buf *buf;
	     int fd;

	     //==========================================================================
	     // Device Set Up
	     //==========================================================================
	     // Open the DRM device
	     if (drm_open(&fd, "/dev/dri/card0")) {
	        std::exit(1);
	     }

	     // Prepare all connectors and CRTCs
	     if (drm_prepare(fd, &num_modeset_dev)) {
	        close(fd);
	        exit(1);
	     }

	     // Perform mode setting on each found connector + CRTC
	     for (drm_modeset_dev *iter = modeset_list; iter; iter = iter->next) {
	        iter->saved_crtc = drmModeGetCrtc(fd, iter->crtc);
	        buf = &iter->bufs[iter->front_buf];
	        if (drmModeSetCrtc(fd, iter->crtc, buf->fb, 0, 0, &iter->conn, 1, &iter->mode)) {
	           std::cerr << "Cannot set CRTC for connector " << iter->conn << std::endl;
	           exit(1);
	        }
	     }

		webcam_thread_data data;

		cv::VideoCapture webcam(0);

		if(!webcam.isOpened()) {
			std::cerr << "Cannot open the webcam" << std::endl;
			exit(1);
		}

		webcam.set(3, 400);
		webcam.set(4, 400);

		data.webcam_x_res = (int)(webcam.get(3) + 0.5);
		data.webcam_y_res = (int)(webcam.get(4) + 0.5);
		std::cout << "Webcam opened at " << data.webcam_x_res << "x" << data.webcam_y_res << std::endl;

		cv::Mat webcam_frame;
		webcam >> webcam_frame;

		/* The use of sds_alloc here is equivalent to malloc(), but registers the buffers with the SDSoC
		 * back end and ensures that we're allocating contiguous memory for subsequent DMA operations.
		 */

		char *buffer_one_pointer = (char *)sds_alloc(webcam_frame.total() * 4);
		char *buffer_two_pointer = (char *)sds_alloc(webcam_frame.total() * 4);

	  if ((!buffer_one_pointer) || (!buffer_two_pointer)) {
	    std::cerr << "ERROR: Unable to allocate memory for incoming webcam frames" << std::endl;
	    exit(1);
	  }

		cv::Mat rgba_mat1(webcam_frame.size(), CV_8UC4, buffer_one_pointer);
		cv::Mat rgba_mat2(webcam_frame.size(), CV_8UC4, buffer_two_pointer);

		cv::cvtColor(webcam_frame, rgba_mat1, cv::COLOR_BGR2RGBA, 4);
		data.camera = buffer_one_pointer;
		data.ready = 1;
		data.stop = false;

	    pthread_t thread_id;
	    unsigned int *temp = (unsigned int *)sds_alloc(224*224*sizeof(unsigned int));
	    unsigned int *temp1 = (unsigned int *)sds_alloc(224*224*sizeof(unsigned int));

	    custom_data self_data;
	    self_data.buf = buf;
	    self_data.old_img = (unsigned int*)temp1;
	    self_data.out = out1;
	    self_data.output_frame = output_frame;

	    bool first_frame = true;

	    perf_counter hw_ctr;
	    int counter = 0;
	    hw_ctr.start();

		while(1) {

			webcam >> webcam_frame;
			if (data.camera == buffer_one_pointer) {
				cv::cvtColor(webcam_frame, rgba_mat2, cv::COLOR_BGR2BGRA, 4);
				data.camera = buffer_two_pointer;
			}
			else {
				cv::cvtColor(webcam_frame, rgba_mat1, cv::COLOR_BGR2BGRA, 4);
				data.camera = buffer_one_pointer;
			}

			for (int i = 0; i < 224; i++) {
				for(int j = 0; j < 224; j++){
			    	  temp[i*224+j] = ((int*)data.camera)[i*data.webcam_x_res + j];
			    	  int red = (temp[i*224+j]>>16)&0xFF;
			    	  int green = (temp[i*224+j]>>8)&0xFF;
			    	  int blue = (temp[i*224+j]>>0)&0xFF;
			    	  img_in[i*224 + j] = ((blue)<<16) | ((green)<<8) | ((red));
				}
			}
			run_segmentation (in, img_in, out, outcacheable, kernel1, kernel2, kernel3,
			   	     				kernel4, kernel5, kernel6, kernel7, kernel8, kernel9, kernel10);

			//cout << "debug1" << endl;

			if (first_frame){
				first_frame = false;
			} else {
				pthread_join(thread_id, NULL);
			}

//			cout << "debug2" << endl;
			memcpy(temp1, temp, 224*224*4);
//			cout << "debug3" << endl;
			memcpy(out1, out, 13*13*21*4);
//			cout << "debug4" << endl;
		    pthread_create(&thread_id, NULL, new_thread, (void *)&self_data);
//		    cout << "debug5" << endl;

		    counter++;
		    if (counter == 10) {
		    	counter = 0;
				hw_ctr.stop();
				uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();
				cout << "Current frame per second: " <<  1000000000.0/((hw_cycles/10)*1.5) << endl;
				hw_ctr.reset();
				hw_ctr.start();
		    }

		}

     close(fd);


     sds_free(in);
     sds_free(img_in);
     sds_free(out);
     sds_free(out1);
     sds_free(outcacheable);

     sds_free(kernel1);
	 sds_free(kernel2);
	 sds_free(kernel3);
	 sds_free(kernel4);
	 sds_free(kernel5);
	 sds_free(kernel6);
	 sds_free(kernel7);
	 sds_free(kernel8);
	 sds_free(kernel9);
	 sds_free(kernel10);

     return 0;

}
