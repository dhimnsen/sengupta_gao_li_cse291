
#include "testbench.h"
#include "runconv.h"

uint64_t l1_l2_mp1_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result) {

	perf_counter hw_ctr;
	hw_ctr.start();

	unsigned char input[111][111][64];
	unsigned char output[55][55][64];

	int inCounter = 0;
	for (int i = 0; i < 111; i++) {
		for (int j = 0; j < 111; j++) {
			for (int k = 0; k < 64; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 111; i+=2) {
		for (int j = 1; j < 111; j+=2) {
			for (int k = 0; k < 64; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 55; i++) {
		for (int j = 0; j < 55; j++) {
			for (int k = 0; k < 64; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();

	bool correct = true;
	for (int i = 0; i < 55*55*16; i++){
		if (out[i] != out_sw[i]){
			correct = false;
			//cout << "index" << i << ", correct: " << out_sw[i] << ", wrong: " << out[i] << endl;
		}
	}

	if (correct == false) {
		cout << "The layer above has error111!" << endl;
		*result = 1;
	}

    return hw_cycles;

}

uint64_t l1_l2_mp4_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result) {

	perf_counter hw_ctr;
	hw_ctr.start();

	unsigned char input[55][55][256];
	unsigned char output[27][27][256];

	int inCounter = 0;
	for (int i = 0; i < 55; i++) {
		for (int j = 0; j < 55; j++) {
			for (int k = 0; k < 256; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 55; i+=2) {
		for (int j = 1; j < 55; j+=2) {
			for (int k = 0; k < 256; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 27; i++) {
		for (int j = 0; j < 27; j++) {
			for (int k = 0; k < 256; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();

	bool correct = true;
	for (int i = 0; i < 27*27*64; i++){
		if (out[i] != out_sw[i]){
			correct = false;
			//cout << "index" << i << ", correct: " << out_sw[i] << ", wrong: " << out[i] << endl;
		}
	}

	if (correct == false) {
		cout << "The layer above has error444!" << endl;
		*result = 1;
	}

    return hw_cycles;

}


uint64_t l1_l2_mp8_test_sw (MIDTYPE in[301056], MIDTYPE out[301056], MIDTYPE *out_sw, bool *result) {

	perf_counter hw_ctr;
	hw_ctr.start();

	unsigned char input[27][27][512];
	unsigned char output[13][13][512];

	int inCounter = 0;
	for (int i = 0; i < 27; i++) {
		for (int j = 0; j < 27; j++) {
			for (int k = 0; k < 512; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 27; i+=2) {
		for (int j = 1; j < 27; j+=2) {
			for (int k = 0; k < 512; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 13; j++) {
			for (int k = 0; k < 512; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();

	bool correct = true;
	for (int i = 0; i < 13*13*128; i++){
		if (out[i] != out_sw[i]){
			correct = false;
			//cout << "index" << i << ", correct: " << out_sw[i] << ", wrong: " << out[i] << endl;
		}
	}

	if (correct == false) {
		cout << "The layer above has error888!" << endl;
		*result = 1;
	}

    return hw_cycles;

}

uint64_t l1_l2_33_hw_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *out_sw, MIDTYPE *kernel,
						Settings *info, bool *result, int in_size, int out_size, int param_size) {

	perf_counter hw_ctr;
	hw_ctr.start();

	MIDTYPE info_array[12];
	info_array[0] = (MIDTYPE)(info->dataSize);
	info_array[1] = (MIDTYPE)(info->inputChannel);
	info_array[2] = (MIDTYPE)(info->outputChannel);
	info_array[3] = (MIDTYPE)(info->isFirst);
	info_array[4] = (MIDTYPE)(info->isLast);
	info_array[5] = (MIDTYPE)(info->startSize);
	info_array[6] = (MIDTYPE)(info->endSize);
	info_array[7] = (MIDTYPE)(info->loopDivider);
	info_array[8] = (MIDTYPE)(info->firstChannel);
	info_array[9] = (MIDTYPE)(info->firstDivider);
	info_array[10] = (MIDTYPE)(info->Shift);
	info_array[11] = (MIDTYPE)(info->shiftFirst);

	l1_l2_hw(in, out, kernel, info_array, in_size, out_size, param_size);

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();

	bool correct = true;
	for (int i = 0; i < out_size; i++){
		if (out[i] != out_sw[i]){
			cout << "correct: " << out_sw[i] << ", wrong: " << out[i] << endl;
			correct = false;
		}
	}

	if (correct == false) {
		cout << "The layer above has error!" << endl;
		*result = 1;
	}

    return hw_cycles;

}

int l1_l2_test (MIDTYPE *in, MIDTYPE *out, MIDTYPE *outcacheable
				, MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10
				, MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10
				, MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10,
				MIDTYPE inmp1[301056], MIDTYPE inmp4[301056], MIDTYPE inmp8[301056],
				MIDTYPE outmp_sw1[301056], MIDTYPE outmp_sw4[301056], MIDTYPE outmp_sw8[301056]) {

	Settings* info[10];

	(info[0]) = new Settings(224, 3, 64, true, false, 0, 224, 1, 3, 1, 3, 0);
	(info[1]) = new Settings(55, 16, 64, false, false, -1, 56, 1, 64, 2, 1, 1);
	(info[2]) = new Settings(55, 16, 64, false, false, -1, 56, 1, 128, 4, 2, 1);
	(info[3]) = new Settings(55, 32, 128, false, false, -1, 56, 2, 128, 4, 2, 1);
	(info[4]) = new Settings(27, 32, 128, false, false, -1, 28, 2, 256, 8, 1, 2);
	(info[5]) = new Settings(27, 48, 192, false, false, -1, 28, 3, 256, 8, 2, 2);
	(info[6]) = new Settings(27, 48, 192, false, false, -1, 28, 3, 384, 12, 1, 2);
	(info[7]) = new Settings(27, 64, 256, false, false, -1, 28, 4, 384, 12, 1, 1);
	(info[8]) = new Settings(13, 64, 256, false, false, -1, 14, 4, 512, 16, 1, 2);
	(info[9]) = new Settings(13, 21, 21, false, true, 0, 13, 0, 512, 16, 0, 2);

	int in_size[10];
	int out_size[10];
	int param_size[10];

	for (int i = 0; i < 10; i++) {

		const int data_size = info[i]->dataSize;
		const int out_ch = info[i]->outputChannel;
		const int in_ch = info[i]->inputChannel;
		const int first_ch = info[i]->firstChannel;

		if (i == 0) {
			in_size[i] = data_size*data_size;
			out_size[i] = 111*111*64/4;
			param_size[i] = 3*3*out_ch + out_ch;
		} else if (i == 9) {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*in_ch;
			param_size[i] = in_ch*first_ch/4 + in_ch;
		} else {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*out_ch/2;
			param_size[i] = 3*3*out_ch*in_ch/4 + out_ch
					+ out_ch*in_ch/4 + out_ch + in_ch*first_ch/4 + in_ch;
		}

	}


	std::cout << "Testing " << NUM_TESTS << " iterations of l1_l2" << endl;

	bool result = 0;
	uint64_t total_time = 0;
	uint64_t total_max_pool_time = 0;
	int progress = -1;

	for (int i = 0; !result && i < NUM_TESTS; i++) {

		total_time += l1_l2_33_hw_test (in1, outcacheable, out_sw1, kernel1, info[0], &result, in_size[0], out_size[0], param_size[0]);
		total_max_pool_time += l1_l2_mp1_test_sw (inmp1, out, outmp_sw1, &result);
		total_time += l1_l2_33_hw_test (in2, out, out_sw2, kernel2, info[1], &result, in_size[1], out_size[1], param_size[1]);
		total_time += l1_l2_33_hw_test (in3, out, out_sw3, kernel3, info[2], &result, in_size[2], out_size[2], param_size[2]);
		total_time += l1_l2_33_hw_test (in4, outcacheable, out_sw4, kernel4, info[3], &result, in_size[3], out_size[3], param_size[3]);
		total_max_pool_time += l1_l2_mp4_test_sw (inmp4, out, outmp_sw4, &result);
		total_time += l1_l2_33_hw_test (in5, out, out_sw5, kernel5, info[4], &result, in_size[4], out_size[4], param_size[4]);
		total_time += l1_l2_33_hw_test (in6, out, out_sw6, kernel6, info[5], &result, in_size[5], out_size[5], param_size[5]);
		total_time += l1_l2_33_hw_test (in7, out, out_sw7, kernel7, info[6], &result, in_size[6], out_size[6], param_size[6]);
		total_time += l1_l2_33_hw_test (in8, outcacheable, out_sw8, kernel8, info[7], &result, in_size[7], out_size[7], param_size[7]);
		total_max_pool_time += l1_l2_mp8_test_sw (inmp8, out, outmp_sw8, &result);
		total_time += l1_l2_33_hw_test (in9, out, out_sw9, kernel9, info[8], &result, in_size[8], out_size[8], param_size[8]);
		total_time += l1_l2_33_hw_test (in10, out, out_sw10, kernel10, info[9], &result, in_size[9], out_size[9], param_size[9]);

		int currentProgress = (ceil)(((double)i)*100/NUM_TESTS);
		if (currentProgress > progress) {
			progress = currentProgress;
			cout << "Finished: " << progress << "%" << endl;
		}

	}

    std::cout << "Total number of CPU cycles running in hardware: "
              << total_time << std::endl;
    std::cout << "Total number of CPU cycles for max pooling running on both hardware and ARM: "
              << total_max_pool_time << std::endl;
    std::cout << "Average number of CPU cycles running in hardware: "
              << (uint64_t)(total_time/NUM_TESTS) << std::endl;
    std::cout << "Average number of CPU cycles for max pooling running on both hardware and ARM: "
              << (uint64_t)(total_max_pool_time/NUM_TESTS) << std::endl;


	std::cout << "Run " << NUM_TESTS << " iterations of l1_l2, please wait..." << endl;

	perf_counter hw_ctr;
	hw_ctr.start();

	for (int i = 0; !result && i < NUM_TESTS; i++) {

		l1_l2_33_hw_run (in1, outcacheable, kernel1, info[0], in_size[0], out_size[0], param_size[0]);
		l1_l2_mp1_run(outcacheable, in);
		l1_l2_33_hw_run (in, out, kernel2, info[1], in_size[1], out_size[1], param_size[1]);
		l1_l2_33_hw_run (out, in, kernel3, info[2], in_size[2], out_size[2], param_size[2]);
		l1_l2_33_hw_run (in, outcacheable, kernel4, info[3], in_size[3], out_size[3], param_size[3]);
		l1_l2_mp4_run(outcacheable, in);
		l1_l2_33_hw_run (in, out, kernel5, info[4], in_size[4], out_size[4], param_size[4]);
		l1_l2_33_hw_run (out, in, kernel6, info[5], in_size[5], out_size[5], param_size[5]);
		l1_l2_33_hw_run (in, out, kernel7, info[6], in_size[6], out_size[6], param_size[6]);
		l1_l2_33_hw_run (out, outcacheable, kernel8, info[7], in_size[7], out_size[7], param_size[7]);
		l1_l2_mp8_run(outcacheable, out);
		l1_l2_33_hw_run (out, in, kernel9, info[8], in_size[8], out_size[8], param_size[8]);
		l1_l2_33_hw_run (in, out, kernel10, info[9], in_size[9], out_size[9], param_size[9]);

	}

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();


    std::cout << "Total number of CPU cycles using this run: "
              << hw_cycles << std::endl;
    std::cout << "Average number of CPU cycles for each iteration: "
              << (uint64_t)(hw_cycles/NUM_TESTS) << std::endl;
    std::cout << "Estimated frame per second: "
              << 1000000000.0/((hw_cycles/NUM_TESTS)*1.5) << std::endl;

    upSamplingNew(out, in);

	return result;
}


