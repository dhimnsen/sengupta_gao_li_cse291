/**
 *  CSE 291 Assignment 2
 */


#include "sobel.h"
using namespace std;

void sobel_sw(DTYPE in[SIZE_w][SIZE_h], DTYPE out[SIZE_w][SIZE_h]) {
	
    for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {

	    	if (j == 0 || j == SIZE_h-1 || i == 0 || i == SIZE_w-1) {
	    		continue;
	    	}

	    	int gx = (int)(in[i+1][j-1]) + 2*(int)(in[i+1][j]) + (int)(in[i+1][j+1])
					- ((int)(in[i-1][j-1]) + 2*(int)(in[i-1][j]) + (int)(in[i-1][j+1]));
	    	int gy = (int)(in[i-1][j-1]) + 2*(int)(in[i][j-1]) + (int)(in[i+1][j-1])
					- ((int)(in[i-1][j+1]) + 2*(int)(in[i][j+1]) + (int)(in[i+1][j+1]));
	    	if (gx < 0) gx = -gx;
	    	if (gy < 0) gy = -gy;
	    	int result = gx + gy;
	    	if (result > 255) result = 255;
	    	out[i+1][j+1] = (DTYPE) result;

		}
	}

}


int main() {

	FILE * fp = fopen("lenna_gray.orig","r");
	int value = 0;
	DTYPE in_pic[SIZE_w][SIZE_h];

	for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i++) {
			fscanf(fp, "%d", &value);
			in_pic[i][j] = (DTYPE)value;
		}
	}
	
	fclose(fp);

	DTYPE out_pic[SIZE_w][SIZE_h];
	DTYPE sw_out_pic[SIZE_w][SIZE_h];

	sobel_sw(in_pic, sw_out_pic);
	// sobel_hw(in_pic, out_pic);

	MYSTREAM in, out;
	for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i+=8) {
			ap_uint<64> temp = 0;
			temp = temp | (((ap_uint<64>)in_pic[i+0][j])<<56);
			temp = temp | (((ap_uint<64>)in_pic[i+1][j])<<48);
			temp = temp | (((ap_uint<64>)in_pic[i+2][j])<<40);
			temp = temp | (((ap_uint<64>)in_pic[i+3][j])<<32);
			temp = temp | (((ap_uint<64>)in_pic[i+4][j])<<24);
			temp = temp | (((ap_uint<64>)in_pic[i+5][j])<<16);
			temp = temp | (((ap_uint<64>)in_pic[i+6][j])<<8);
			temp = temp | ((ap_uint<64>)in_pic[i+7][j]);
			in.write(temp);
		}
	}

	sobel_hw(&in, &out);

	for(int j=0; j<SIZE_h; j++) {
		for (int i = 0; i < SIZE_w; i+=8) {
			ap_uint<64> temp = out.read();
			out_pic[i][j] = (ap_uint<8>)((temp & 0xFF00000000000000)>>56);
			out_pic[i+1][j] = (ap_uint<8>)((temp & 0x00FF000000000000)>>48);
			out_pic[i+2][j] = (ap_uint<8>)((temp & 0x0000FF0000000000)>>40);
			out_pic[i+3][j] = (ap_uint<8>)((temp & 0x000000FF00000000)>>32);
			out_pic[i+4][j] = (ap_uint<8>)((temp & 0x00000000FF000000)>>24);
			out_pic[i+5][j] = (ap_uint<8>)((temp & 0x0000000000FF0000)>>16);
			out_pic[i+6][j] = (ap_uint<8>)((temp & 0x000000000000FF00)>>8);
			out_pic[i+7][j] = (ap_uint<8>)(temp & 0x00000000000000FF);
		}
	}

	FILE * fpw = fopen("lenna_gray.out","w");

	bool correct = 1;
	for(int j=2; j<SIZE_h; j++) {
		for (int i = 2; i < SIZE_w; i++) {
			fprintf(fpw,"%d\n",(int)(out_pic[i][j]));
			if (out_pic[i][j] != sw_out_pic[i][j]) {
				printf("Error: True %d After %d\n", (int)(sw_out_pic[i][j]), (int)(out_pic[i][j]));
				correct = 0;
			}
		}
	}	

	fclose(fpw);

	if (correct == 1) {
		printf("PASS: The output matches the sw output!\n");
		return 0;
	} else {
		printf("FAIL: Output DOES NOT match the sw output\n");
		return 1;
	}

}
