
#include "l1_l2.h"

void fire_layer (MIDTYPE in[602112], MIDTYPE out[602112], const ap_uint<9> data_size,
		const ap_uint<9> out_ch, const ap_uint<7> in_ch,
		const bool is_first, bool is_last, const ap_int<9> start_size,
		const ap_int<9> end_size, const ap_uint<3> loop_divider,
		const ap_uint<10> first_ch, const ap_uint<5> first_divider,
		const PTYPE kernelBuffer1[7][7][3][96], const PTYPE kernelBuffer2[3][3][64][256],
		const PTYPE kernelOffset[256], const PTYPE smallKernel[64][256],
		const PTYPE smallKernelOffset[256], const PTYPE firstKernel[512][64],
		const PTYPE firstKernelOffset[64], const int in_size, const int out_size) {

#pragma HLS DATAFLOW

	DTYPE lineBuffer1[7][230][3];
#pragma HLS ARRAY_PARTITION variable=lineBuffer1 complete dim=1
#pragma HLS ARRAY_PARTITION variable=lineBuffer1 complete dim=3

	DTYPE lineBuffer2[3][58][64];
#pragma HLS ARRAY_PARTITION variable=lineBuffer2 complete dim=1
#pragma HLS ARRAY_PARTITION variable=lineBuffer2 cyclic factor=4 dim=3

	DTYPE windowBuffer1[7][7][3];
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=2
#pragma HLS ARRAY_PARTITION variable=windowBuffer1 complete dim=3

	DTYPE windowBuffer2[3][3][64];
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=2
#pragma HLS ARRAY_PARTITION variable=windowBuffer2 complete dim=3

	MIDSTREAM in_cache;
#pragma HLS STREAM variable=in_cache depth=500 dim=1
	MIDSTREAM out_cache;
#pragma HLS STREAM variable=out_cache depth=500 dim=1
	MIDSTREAM middle_fifo;
#pragma HLS STREAM variable=middle_fifo depth=1000 dim=1

	In_cache_loop: for(ap_uint<18> i = 0; i < in_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=25088 max=100352
#pragma HLS PIPELINE II=2
		MIDTYPE tempBuf = in[i];
		in_cache.write(tempBuf);
	}

	First_Height_Loop: for(ap_uint<8> i = 0; i < data_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=14 max=224
		First_Width_Loop: for (ap_uint<8> j = 0; j < data_size; j++) {
#pragma HLS LOOP_TRIPCOUNT min=14 max=224

			if (is_first) {
				MIDTYPE tempBuf = in_cache.read();
				middle_fifo.write(tempBuf);
				continue;
			}

			DTYPE firstBuffer[16][32];
#pragma HLS ARRAY_PARTITION variable=firstBuffer complete dim=2

			ap_int<24> firstIntermediate[64];

			for (ap_uint<5> z = 0; z < first_divider; z++){
#pragma HLS LOOP_TRIPCOUNT min=3 max=16
#pragma HLS DATAFLOW

				for (ap_uint<10> k = 0; k < 32; k+=4) {
#pragma HLS PIPELINE II=2
					MIDTYPE tempBuf = in_cache.read();
					firstBuffer[z][k] = (DTYPE)((tempBuf>>24)&0x000000FF);
					firstBuffer[z][k+1] = (DTYPE)((tempBuf>>16)&0x000000FF);
					firstBuffer[z][k+2] = (DTYPE)((tempBuf>>8)&0x000000FF);
					firstBuffer[z][k+3] = (DTYPE)((tempBuf)&0x000000FF);
				}

				DTYPE buf1 = 0;
				DTYPE buf2 = 0;
				DTYPE buf3 = 0;
				DTYPE buf4 = 0;
				bool bufState1 = true;
				bool bufState2 = false;
				bool bufState3 = false;
				bool bufState4 = false;

				for (ap_uint<7> k = 0; k < in_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
#pragma HLS PIPELINE
					ap_int<24> firstTempBuf = (z==0)? (ap_int<24>)0 : firstIntermediate[k];
					for (int l = 0; l < 32; l++){
						firstTempBuf += firstBuffer[z][l]*firstKernel[l+32*z][k];
					}

					firstTempBuf = (z==(first_divider-1))? (ap_int<24>)(firstTempBuf+firstKernelOffset[k]) : firstTempBuf;

					firstTempBuf = ((firstTempBuf>255)&&(z==(first_divider-1)))? (ap_int<24>)255 : firstTempBuf;
					firstTempBuf = ((firstTempBuf<0)&&(z==(first_divider-1)))? (ap_int<24>)0 : firstTempBuf;
					firstIntermediate[k] = firstTempBuf;

					buf4 = buf3; buf3 = buf2; buf2 = buf1;
					buf1 = firstTempBuf;

					MIDTYPE combine = ((((MIDTYPE)buf4)<<24)&0xFF000000) | ((((MIDTYPE)buf3)<<16)&0x00FF0000)
							| ((((MIDTYPE)buf2)<<8)&0x0000FF00) | (((MIDTYPE)buf1)&0x000000FF);
					if ((z==(first_divider-1))&&(bufState4||is_last)) middle_fifo.write(combine);

					bool tempWire = bufState4;
					bufState4 = bufState3;
					bufState3 = bufState2;
					bufState2 = bufState1;
					bufState1 = tempWire;

				}
			}

		}
	}



	Height_Loop: for(ap_int<9> i = start_size; i < end_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=16 max=230
		Width_Loop: for (ap_int<9> j = start_size; j < end_size; j++) {
#pragma HLS LOOP_TRIPCOUNT min=16 max=230

			ap_int<24> intermediate[256];
			ap_int<24> smallIntermediate[256];
			DTYPE smallBuffer[64];
#pragma HLS ARRAY_PARTITION variable=smallBuffer complete dim=1

			if (is_first) {

				MIDTYPE tempBuf = !(i<0||j<0||i>=data_size||j>=data_size) ?
						middle_fifo.read() : (MIDTYPE)0;
				DTYPE tempBufArray[3];
#pragma HLS ARRAY_PARTITION variable=tempBufArray complete dim=1
				tempBufArray[0] = (DTYPE)((tempBuf>>16)&0x000000FF);
				tempBufArray[1] = (DTYPE)((tempBuf>>8)&0x000000FF);
				tempBufArray[2] = (DTYPE)(tempBuf&0x000000FF);

				for (int k = 0; k < 3; k++){
#pragma HLS DEPENDENCE variable=lineBuffer1 false
#pragma HLS DEPENDENCE variable=windowBuffer1 false
#pragma HLS UNROLL
					for (int l = 0; l < 7; l++){
#pragma HLS UNROLL
						for (int m = 0; m < 6; m++){
#pragma HLS UNROLL
							windowBuffer1[l][m][k] = windowBuffer1[l][m+1][k];
						}
					}

					DTYPE innerBuffer1[7];

					for (int l = 0; l < 6; l++){
#pragma HLS UNROLL
						innerBuffer1[l] = lineBuffer1[l+1][j+3][k];
						lineBuffer1[l][j+3][k] = innerBuffer1[l];
					}

					lineBuffer1[6][j+3][k] = tempBufArray[k];
					innerBuffer1[6] = tempBufArray[k];

					for (int l = 0; l < 7; l++){
#pragma HLS UNROLL
						windowBuffer1[l][6][k] = innerBuffer1[l];
					}

				}


			} else if (!is_last){

				for (int k = 0; k < 64; k++) {
#pragma HLS DEPENDENCE variable=windowBuffer2 false
#pragma HLS UNROLL
					windowBuffer2[0][0][k] = windowBuffer2[0][1][k];
					windowBuffer2[0][1][k] = windowBuffer2[0][2][k];

					windowBuffer2[1][0][k] = windowBuffer2[1][1][k];
					DTYPE share = windowBuffer2[1][2][k];
					windowBuffer2[1][1][k] = share;
					smallBuffer[k] = share;

					windowBuffer2[2][0][k] = windowBuffer2[2][1][k];
					windowBuffer2[2][1][k] = windowBuffer2[2][2][k];
				}

				First_Loop_2: for (ap_uint<7> k = 0; k < in_ch; k+=4){
#pragma HLS LOOP_TRIPCOUNT min=4 max=16
#pragma HLS DEPENDENCE variable=lineBuffer2 false
#pragma HLS PIPELINE II=1
					MIDTYPE tempBuf = !(i<0||j<0||i>=data_size||j>=data_size) ? middle_fifo.read() : (MIDTYPE)0;
					DTYPE tempBuf1 = (DTYPE)((tempBuf>>24)&0x000000FF);
					DTYPE tempBuf2 = (DTYPE)((tempBuf>>16)&0x000000FF);
					DTYPE tempBuf3 = (DTYPE)((tempBuf>>8)&0x000000FF);
					DTYPE tempBuf4 = (DTYPE)(tempBuf&0x000000FF);
					DTYPE innerBuffer21[3];
					DTYPE innerBuffer22[3];
					DTYPE innerBuffer23[3];
					DTYPE innerBuffer24[3];

					for (int l = 0; l < 2; l++){
						innerBuffer21[l] = lineBuffer2[l+1][j+1][k];
						innerBuffer22[l] = lineBuffer2[l+1][j+1][k+1];
						innerBuffer23[l] = lineBuffer2[l+1][j+1][k+2];
						innerBuffer24[l] = lineBuffer2[l+1][j+1][k+3];
						lineBuffer2[l][j+1][k] = innerBuffer21[l];
						lineBuffer2[l][j+1][k+1] = innerBuffer22[l];
						lineBuffer2[l][j+1][k+2] = innerBuffer23[l];
						lineBuffer2[l][j+1][k+3] = innerBuffer24[l];
					}

					lineBuffer2[2][j+1][k] = tempBuf1;
					lineBuffer2[2][j+1][k+1] = tempBuf2;
					lineBuffer2[2][j+1][k+2] = tempBuf3;
					lineBuffer2[2][j+1][k+3] = tempBuf4;

					innerBuffer21[2] = tempBuf1;
					innerBuffer22[2] = tempBuf2;
					innerBuffer23[2] = tempBuf3;
					innerBuffer24[2] = tempBuf4;

					windowBuffer2[0][2][k] = innerBuffer21[0];
					windowBuffer2[1][2][k] = innerBuffer21[1];
					windowBuffer2[2][2][k] = innerBuffer21[2];

					windowBuffer2[0][2][k+1] = innerBuffer22[0];
					windowBuffer2[1][2][k+1] = innerBuffer22[1];
					windowBuffer2[2][2][k+1] = innerBuffer22[2];

					windowBuffer2[0][2][k+2] = innerBuffer23[0];
					windowBuffer2[1][2][k+2] = innerBuffer23[1];
					windowBuffer2[2][2][k+2] = innerBuffer23[2];

					windowBuffer2[0][2][k+3] = innerBuffer24[0];
					windowBuffer2[1][2][k+3] = innerBuffer24[1];
					windowBuffer2[2][2][k+3] = innerBuffer24[2];

				}

			}

			if ((((i%2==0 || j%2==0 || i<3 || j<3)&&is_first)) || ((i<1 || j<1)&&(!is_first)&&(!is_last))) continue;

			bool state = false;
			MIDTYPE buf;

			for (ap_uint<3> z = 0; z < loop_divider; z++) {
#pragma HLS LOOP_TRIPCOUNT min=1 max=4

				Second_Loop: for (ap_uint<9> k = 0; k < out_ch; k++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
#pragma HLS PIPELINE
					ap_int<24> tempBuf = (z==0)? (ap_int<24>)0 : intermediate[k];
					ap_int<24> smallTempBuf = (z==0)? (ap_int<24>)0 : smallIntermediate[k];
					DTYPE candidate_data[147];
#pragma HLS ARRAY_PARTITION variable=candidate_data complete dim=1
					PTYPE candidate_para[147];
#pragma HLS ARRAY_PARTITION variable=candidate_para complete dim=1

					ap_uint<8> index = 0;
					Second_inner_1: for (int s = 0; s < 3; s++){
						ap_int<9> true_i = i-2+s;
						for (int t = 0; t < 3; t++){
							ap_int<9> true_j = j-2+t;
							for (int l = 0; l < 16; l++){
								candidate_data[index] = ((!is_first)&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										windowBuffer2[s][t][l+16*z] : (DTYPE)0;
								candidate_para[index] = ((!is_first)&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										kernelBuffer2[s][t][l+16*z][k] : (PTYPE)0;
								index++;
							}
						}
					}
					candidate_data[144] = (DTYPE)0;
					candidate_para[144] = (PTYPE)0;
					candidate_data[145] = (DTYPE)0;
					candidate_para[145] = (PTYPE)0;
					candidate_data[146] = (DTYPE)0;
					candidate_para[146] = (PTYPE)0;

					index = 0;
					Second_inner_2: for (int s = 0; s < 7; s++){
						ap_int<9> true_i = i-6+s;
						for (int t = 0; t < 7; t++){
							ap_int<9> true_j = j-6+t;
							for (int l = 0; l < 3; l++){
								candidate_data[index] = (is_first&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										windowBuffer1[s][t][l] : candidate_data[index];
								candidate_para[index] = (is_first&&(!(true_i<0||true_j<0||true_i>=data_size||true_j>=data_size)))?
										kernelBuffer1[s][t][l][k] : candidate_para[index];
								index++;
							}
						}
					}

					for (int l = 0; l < 147; l++){
						tempBuf += candidate_data[l]*candidate_para[l];
					}

					for (int l = 0; l < 16; l++){
						smallTempBuf += smallBuffer[l+16*z]*smallKernel[l+16*z][k];
					}

					tempBuf = (z==(loop_divider-1))? (ap_int<24>)(tempBuf+kernelOffset[k]) : tempBuf;

					tempBuf = ((tempBuf>255)&&(z==(loop_divider-1)))? (ap_int<24>)255 : tempBuf;
					tempBuf = ((tempBuf<0)&&(z==(loop_divider-1)))? (ap_int<24>)0 : tempBuf;
					intermediate[k] = tempBuf;

					smallTempBuf = (z==(loop_divider-1))? (ap_int<24>)(smallTempBuf+smallKernelOffset[k]) : smallTempBuf;

					smallTempBuf = ((smallTempBuf>255)&&(z==(loop_divider-1)))? (ap_int<24>)255 : smallTempBuf;
					smallTempBuf = ((smallTempBuf<0)&&(z==(loop_divider-1)))? (ap_int<24>)0 : smallTempBuf;
					smallIntermediate[k] = smallTempBuf;

					MIDTYPE combine = ((((MIDTYPE)smallTempBuf)<<8)&0x0000FF00)|(((MIDTYPE)tempBuf)&0x000000FF);
					MIDTYPE superCombine = ((((MIDTYPE)buf)<<16)&0xFFFF0000)|(((MIDTYPE)combine)&0x0000FFFF);
					if (z==(loop_divider-1) && state) out_cache.write(superCombine);

					state = !state;
					buf = combine;

				}

			}

			if (is_last) {
				for (ap_uint<5> k = 0; k < in_ch; k++) {
#pragma HLS LOOP_TRIPCOUNT min=21 max=21
#pragma HLS PIPELINE II=2
					MIDTYPE tempBuf = middle_fifo.read()&0x000000FF;
					out_cache.write(tempBuf);
				}
			}

		}

	}

	Out_cache_loop: for(ap_uint<21> i = 0; i < out_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=4116 max=602112
#pragma HLS PIPELINE II=2
		MIDTYPE tempBuf = out_cache.read();
		out[i] = tempBuf;
	}


}


void kernel_layer (MIDTYPE kernel[50176], PTYPE kernelBuffer1[7][7][3][96],
		PTYPE kernelBuffer2[3][3][64][256], PTYPE kernelOffset[256],
		PTYPE smallKernel[64][256], PTYPE smallKernelOffset[256],
		PTYPE firstKernel[512][64], PTYPE firstKernelOffset[64],
		MIDTYPE info[10], const int param_size) {

#pragma HLS DATAFLOW

	MIDSTREAM param_cache;
#pragma HLS STREAM variable=param_cache depth=500 dim=1

	const ap_uint<7> in_ch = (ap_uint<7>)info[1];
	const ap_uint<9> out_ch = (ap_uint<9>)info[2];
	const bool is_first = (bool)info[3];
	const bool is_last = (bool)info[4];
	const ap_uint<10> first_ch = (ap_uint<10>)info[8];

	In_cache_loop: for(ap_uint<18> i = 0; i < param_size; i++) {
#pragma HLS LOOP_TRIPCOUNT min=2500 max=50176
#pragma HLS PIPELINE II=2
		MIDTYPE tempBuf = kernel[i];
		param_cache.write(tempBuf);
	}

	if (!is_last) {

		if (is_first) {

			for (ap_uint<3> s = 0; s < 7; s++){
				for (ap_uint<3> t = 0; t < 7; t++){
					Kernel_Loop_first: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=96 max=96
#pragma HLS PIPELINE II=2
						MIDTYPE temp = param_cache.read();
						PTYPE temp1 = (PTYPE)((temp>>16)&0x0000003F);
						PTYPE temp2 = (PTYPE)((temp>>8)&0x0000003F);
						PTYPE temp3 = (PTYPE)(temp&0x0000003F);
						kernelBuffer1[s][t][0][i] = temp1;
						kernelBuffer1[s][t][1][i] = temp2;
						kernelBuffer1[s][t][2][i] = temp3;
					}
				}
			}

		} else {

			for (ap_uint<2> s = 0; s < 3; s++){
				for (ap_uint<2> t = 0; t < 3; t++){
					Kernel_Loop_other: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
						for (ap_uint<7> j = 0; j < in_ch; j+=4){
#pragma HLS DEPENDENCE variable=kernelBuffer2 false
#pragma HLS LOOP_TRIPCOUNT min=4 max=16
#pragma HLS PIPELINE II=2
							MIDTYPE temp = param_cache.read();
							PTYPE temp1 = (PTYPE)((temp>>24)&0x0000003F);
							PTYPE temp2 = (PTYPE)((temp>>16)&0x0000003F);
							PTYPE temp3 = (PTYPE)((temp>>8)&0x0000003F);
							PTYPE temp4 = (PTYPE)(temp&0x0000003F);
							kernelBuffer2[s][t][j][i] = temp1;
							kernelBuffer2[s][t][j+1][i] = temp2;
							kernelBuffer2[s][t][j+2][i] = temp3;
							kernelBuffer2[s][t][j+3][i] = temp4;
						}
					}
				}
			}

		}

		Kernel_offset_loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
#pragma HLS PIPELINE II=2
			kernelOffset[i] = (PTYPE)((param_cache.read())&0x0000003F);
		}

		if (!is_first) {

			smallKernel_Loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
				for (ap_uint<7> j = 0; j < in_ch; j+=4){
#pragma HLS DEPENDENCE variable=smallKernel false
#pragma HLS LOOP_TRIPCOUNT min=4 max=16
#pragma HLS PIPELINE II=2
					MIDTYPE temp = param_cache.read();
					PTYPE temp1 = (PTYPE)((temp>>24)&0x0000003F);
					PTYPE temp2 = (PTYPE)((temp>>16)&0x0000003F);
					PTYPE temp3 = (PTYPE)((temp>>8)&0x0000003F);
					PTYPE temp4 = (PTYPE)(temp&0x0000003F);
					smallKernel[j][i] = temp1;
					smallKernel[j+1][i] = temp2;
					smallKernel[j+2][i] = temp3;
					smallKernel[j+3][i] = temp4;
				}
			}

			smallKernel_offset_loop: for (ap_uint<9> i = 0; i < out_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=64 max=256
#pragma HLS PIPELINE II=2
				smallKernelOffset[i] = (PTYPE)((param_cache.read())&0x0000003F);
			}

		}
	}

	if (!is_first) {

	firstKernel_Loop: for (ap_uint<7> i = 0; i < in_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
			for (ap_uint<10> j = 0; j < first_ch; j+=4){
#pragma HLS DEPENDENCE variable=firstKernel false
#pragma HLS LOOP_TRIPCOUNT min=24 max=128
#pragma HLS PIPELINE II=2
				MIDTYPE temp = param_cache.read();
				PTYPE temp1 = (PTYPE)((temp>>24)&0x0000003F);
				PTYPE temp2 = (PTYPE)((temp>>16)&0x0000003F);
				PTYPE temp3 = (PTYPE)((temp>>8)&0x0000003F);
				PTYPE temp4 = (PTYPE)(temp&0x0000003F);
				firstKernel[j][i] =  temp1;
				firstKernel[j+1][i] = temp2;
				firstKernel[j+2][i] = temp3;
				firstKernel[j+3][i] = temp4;
			}
		}

		firstKernel_offset_loop: for (ap_uint<7> i = 0; i < in_ch; i++){
#pragma HLS LOOP_TRIPCOUNT min=16 max=64
#pragma HLS PIPELINE II=2
			firstKernelOffset[i] = (PTYPE)((param_cache.read())&0x0000003F);
		}
	}

}


void l1_l2_hw (MIDTYPE in[602112], MIDTYPE out[602112], MIDTYPE kernel[50176], MIDTYPE info[10],
		const int in_size, const int out_size, const int param_size) {

#pragma HLS INTERFACE ap_fifo port=in
#pragma HLS INTERFACE ap_fifo port=out
#pragma HLS INTERFACE ap_fifo port=kernel
#pragma HLS INTERFACE ap_fifo port=info

	PTYPE kernelBuffer1[7][7][3][96];
#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=1
#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=2
#pragma HLS ARRAY_PARTITION variable=kernelBuffer1 complete dim=3

	PTYPE kernelBuffer2[3][3][64][256];
#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 complete dim=1
#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 complete dim=2
#pragma HLS ARRAY_PARTITION variable=kernelBuffer2 cyclic factor=16 dim=3

	PTYPE kernelOffset[256];

	PTYPE smallKernel[64][256];
#pragma HLS ARRAY_PARTITION variable=smallKernel cyclic factor=16 dim=1
	PTYPE smallKernelOffset[256];

	PTYPE firstKernel[512][64];
#pragma HLS ARRAY_PARTITION variable=firstKernel cyclic factor=32 dim=1
	PTYPE firstKernelOffset[64];

	MIDTYPE info_local[10];
	for (ap_uint<4> i = 0; i < 10; i++) {
#pragma HLS PIPELINE II=2
		info_local[i] = info[i];
	}

	const ap_uint<9> data_size = (ap_uint<9>)info_local[0];
	const ap_uint<7> in_ch = (ap_uint<7>)info_local[1];
	const ap_uint<9> out_ch = (ap_uint<9>)info_local[2];
	const bool is_first = (bool)info_local[3];
	const bool is_last = (bool)info_local[4];
	const ap_int<9> start_size = (ap_int<9>)info_local[5];
	const ap_int<9> end_size = (ap_int<9>)info_local[6];
	const ap_uint<3> loop_divider = (ap_uint<3>)info_local[7];
	const ap_uint<10> first_ch = (ap_uint<10>)info_local[8];
	const ap_uint<5> first_divider = (ap_uint<5>)info_local[9];

	kernel_layer(kernel, kernelBuffer1, kernelBuffer2, kernelOffset,
			smallKernel, smallKernelOffset, firstKernel, firstKernelOffset,
			info_local, param_size);

	fire_layer(in, out, data_size, out_ch, in_ch, is_first, is_last, start_size,
		end_size, loop_divider, first_ch, first_divider, kernelBuffer1, kernelBuffer2,
		kernelOffset, smallKernel, smallKernelOffset, firstKernel, firstKernelOffset,
		in_size, out_size);

}

