/*
 * draw.h
 *
 *  Created on: Jan 29, 2015
 *      Author: ra
 */

#ifndef DRAW_H_
#define DRAW_H_

int draw_something(unsigned int fb[2304000],
                   unsigned int img[307200],
                   unsigned int x_res,
                   unsigned int y_res,
                   unsigned int img_x_res,
                   unsigned int img_y_res,
                   unsigned int img_x,
                   unsigned int img_y);

#endif /* DRAW_H_ */
