
open_project hls
set_top sobel_hw
add_files sobel.cpp
add_files sobel.h
add_files -tb sobel_test.cpp
add_files -tb lenna_gray.orig
open_solution "solution1"
set_part  {xc7z020clg484-1}
create_clock -period 5 -name default

source "./directives.tcl"

