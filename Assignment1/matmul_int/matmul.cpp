#include "matmul.h"

void matmul_hw(DTYPE matrix1[SIZE][SIZE], DTYPE matrix2[SIZE][SIZE], DTYPE out[SIZE][SIZE]) {

#pragma HLS ARRAY_PARTITION variable=matrix1 dim=2
#pragma HLS ARRAY_PARTITION variable=matrix2 dim=2
#pragma HLS ARRAY_PARTITION variable=out dim=2

	ap_int<13> temp1[SIZE];
	DTYPE temp2[SIZE];

#pragma HLS ARRAY_PARTITION variable=temp1 dim=1
#pragma HLS ARRAY_PARTITION variable=temp2 dim=1

	  // matrix multiplication of a A*B matrix
	for (int i = 0; i < SIZE; i++) {
		for (int k = 0; k < SIZE; k++) {
		#pragma HLS PIPELINE II=1
			for(int s = 0; s < SIZE; s++){
				temp1[s] = matrix1[i][s];
			}
			for (int j = 0; j < SIZE; j++) {
				DTYPE temp3 = (k==0)? 0:temp2[j];
				ap_int<13> temp = matrix2[k][j];
				temp2[j] = temp3 + temp*temp1[j];
				if (k==SIZE-1) out[i][j] = temp2[j];
			}
		}
	}
}

