#include <stdio.h>
#include "sobel.h"

const ap_int<3> h[3][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
const ap_int<3> v[3][3] = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};

void sobel_sw(DTYPE img_i[ROW][COL], DTYPE img_o[ROW-2][COL-2]) {

	for(int i = 0; i < ROW-2; i++) {
		for(int j = 0; j < COL-2; j++) {

			ap_int<13> Gx0 = h[0][0]*img_i[i][j]   + h[0][1]*img_i[i][j+1]   + h[0][2]*img_i[i][j+2];
			ap_int<13> Gx1 = h[1][0]*img_i[i+1][j] + h[1][1]*img_i[i+1][j+1] + h[1][2]*img_i[i+1][j+2];
			ap_int<13> Gx2 = h[2][0]*img_i[i+2][j] + h[2][1]*img_i[i+2][j+1] + h[2][2]*img_i[i+2][j+2];

			ap_int<13> Gy0 = v[0][0]*img_i[i][j]   + v[0][1]*img_i[i][j+1]   + v[0][2]*img_i[i][j+2];
			ap_int<13> Gy1 = v[1][0]*img_i[i+1][j] + v[1][1]*img_i[i+1][j+1] + v[1][2]*img_i[i+1][j+2];
			ap_int<13> Gy2 = v[2][0]*img_i[i+2][j] + v[2][1]*img_i[i+2][j+1] + v[2][2]*img_i[i+2][j+2];

			ap_int<13> Gx = Gx0 + Gx1 + Gx2;
			ap_int<13> Gy = Gy0 + Gy1 + Gy2;

			if(Gx < 0) Gx = -Gx;
			if(Gy < 0) Gy = -Gy;

			img_o[i][j] = (DTYPE)Gx + Gy;
		}
	}

}


int main() {

	DTYPE img_i[ROW][COL];

	// read input file
	FILE * fp = fopen("img.dat", "r");
	for(int i = 0; i < ROW; i++) {
		for (int j = 0; j < COL; j++) {
			int value = 0;
			fscanf(fp, "%d", &value);
			img_i[i][j] = (DTYPE)value;
		}
	}
	fclose(fp);

	DTYPE sw_img_o[ROW-2][COL-2];
	DTYPE hw_img_o[ROW-2][COL-2];

	// compute golden solution
	sobel_sw(img_i, sw_img_o);
	DSTREAM row1;
	DSTREAM row2;
	DSTREAM row3;
	DSTREAM convOut;
	// compute hardware solution
//	for(int i = 0; i < ROW-2; i++) {
//
//
//		for(int j = 0; j < COL; j++) {
//			row1.write(img_i[i][j]);
//			row2.write(img_i[i+1][j]);
//			row3.write(img_i[i+2][j]);
//		}
//
//	}
	sobel_filter(img_i, hw_img_o);
//	for(int i = 0; i < ROW-2; i++) {
//		for(int j = 0; j < COL; j++) {
//			hw_img_o[i][j] = convOut.read();
//		}
//	}
	// zero out edge
//	for(int i = 0; i < ROW; i++) {
//		hw_img_o[i][0] = 0;
//		hw_img_o[i][1] = 0;
//	}

	// check sw vs hw soln
	for(int i = 0; i < ROW-2; i++) {
		for(int j = 0; j < COL-2; j++) {
			if ((uint8_t)sw_img_o[i][j] != (uint8_t)hw_img_o[i][j]) {
				printf("FAIL: Miss match on pixel (%d, %d)\n", i, j);
				printf("   sw_img_o[i][j]: %3d\n", (uint8_t)sw_img_o[i][j]);
				printf("   hw_img_o[i][j]: %3d\n", (uint8_t)hw_img_o[i][j]);


				printf("current 3 rows: \n");
				for(int m = 0; m < 3; m++) {
					for(int n = 0; n < COL-2; n++) {
						printf("%3u ", (uint8_t)img_i[m][n]);
					}
					printf("\n");
				}

				printf("sw_img_o: ");
				for(int k = 0; k < COL-2; k++) {
					printf("%3u ", (uint8_t)sw_img_o[i][k]);
				}
				printf("\nhw_img_o: ");
				for(int k = 0; k < COL-2; k++) {
					printf("%3u ", (uint8_t)hw_img_o[i][k]);
				}
				return 1;
			}
		}
	}
	printf("PASS: The output matches the sw output!\n");
	return 0;
}
