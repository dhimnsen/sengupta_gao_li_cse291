
#ifndef _UTILITY_H_
#define _UTILITY_H_

#include "sds_lib.h"

class perf_counter
{
public:
     uint64_t tot, cnt, calls;
     perf_counter() : tot(0), cnt(0), calls(0) {};
     inline void reset() { tot = cnt = calls = 0; }
     inline void start() { cnt = sds_clock_counter(); calls++; };
     inline void stop() { tot += (sds_clock_counter() - cnt); };
     inline uint64_t avg_cpu_cycles() { return ((tot+(calls>>1)) / calls); };
};

class Settings {

public:
	int dataSize;
	int inputChannel;
	int outputChannel;
	bool isFirst;
	bool isLast;
	int startSize;
	int endSize;
	int loopDivider;
	int firstChannel;
	int firstDivider;
	int Shift;
	int shiftFirst;

	Settings (int data_size, int input_channel,
			int output_channel, bool first, bool last, int start, int end,
			int loop_divider, int first_channel, int first_divider, int shift, int shift_first){
		dataSize = data_size;
		inputChannel = input_channel;
		outputChannel = output_channel;
		isFirst = first;
		isLast = last;
		startSize = start;
		endSize = end;
		loopDivider = loop_divider;
		firstChannel = first_channel;
		firstDivider = first_divider;
		Shift = shift;
		shiftFirst = shift_first;
	}

};

#endif
