
#include "runconv.h"

void init_arrays (MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10,
				MIDTYPE *in1, MIDTYPE *in2, MIDTYPE *in3,
				MIDTYPE *in4, MIDTYPE *in5, MIDTYPE *in6, MIDTYPE *in7,
				MIDTYPE *in8, MIDTYPE *in9, MIDTYPE *in10, MIDTYPE *img_in,
				MIDTYPE *out_sw1, MIDTYPE *out_sw2, MIDTYPE *out_sw3,
				MIDTYPE *out_sw4, MIDTYPE *out_sw5, MIDTYPE *out_sw6, MIDTYPE *out_sw7,
				MIDTYPE *out_sw8, MIDTYPE *out_sw9, MIDTYPE *out_sw10,
				MIDTYPE inmp1[301056], MIDTYPE inmp4[301056], MIDTYPE inmp8[301056],
				MIDTYPE outmp_sw1[301056], MIDTYPE outmp_sw4[301056], MIDTYPE outmp_sw8[301056]) {

	MIDTYPE value;

	cout << "start init kernel arrays" << endl;

	FILE * fpc1 = fopen("/media/card/data/car_kernel_1.out","r");
	FILE * fpc2 = fopen("/media/card/data/car_kernel_2.out","r");
	FILE * fpc3 = fopen("/media/card/data/car_kernel_3.out","r");
	FILE * fpc4 = fopen("/media/card/data/car_kernel_4.out","r");
	FILE * fpc5 = fopen("/media/card/data/car_kernel_5.out","r");
	FILE * fpc6 = fopen("/media/card/data/car_kernel_6.out","r");
	FILE * fpc7 = fopen("/media/card/data/car_kernel_7.out","r");
	FILE * fpc8 = fopen("/media/card/data/car_kernel_8.out","r");
	FILE * fpc9 = fopen("/media/card/data/car_kernel_9.out","r");
	FILE * fpc10 = fopen("/media/card/data/car_kernel_10.out","r");

	for(int i = 0; i < 50176; i++) {
		fscanf(fpc1, "%d", &value);
		kernel1[i] = value;
		fscanf(fpc2, "%d", &value);
		kernel2[i] = value;
		fscanf(fpc3, "%d", &value);
		kernel3[i] = value;
		fscanf(fpc4, "%d", &value);
		kernel4[i] = value;
		fscanf(fpc5, "%d", &value);
		kernel5[i] = value;
		fscanf(fpc6, "%d", &value);
		kernel6[i] = value;
		fscanf(fpc7, "%d", &value);
		kernel7[i] = value;
		fscanf(fpc8, "%d", &value);
		kernel8[i] = value;
		fscanf(fpc9, "%d", &value);
		kernel9[i] = value;
		fscanf(fpc10, "%d", &value);
		kernel10[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);

	cout << "end init kernel arrays" << endl;

	cout << "start init in arrays (To be removed in real application)" << endl;

	fpc1 = fopen("/media/card/data/car_in_1.out","r");
	fpc2 = fopen("/media/card/data/car_in_2.out","r");
	fpc3 = fopen("/media/card/data/car_in_3.out","r");
	fpc4 = fopen("/media/card/data/car_in_4.out","r");
	fpc5 = fopen("/media/card/data/car_in_5.out","r");
	fpc6 = fopen("/media/card/data/car_in_6.out","r");
	fpc7 = fopen("/media/card/data/car_in_7.out","r");
	fpc8 = fopen("/media/card/data/car_in_8.out","r");
	fpc9 = fopen("/media/card/data/car_in_9.out","r");
	fpc10 = fopen("/media/card/data/car_in_10.out","r");
	FILE * fpcmp1 = fopen("/media/card/data/car_in_1_max_pool.out","r");
	FILE * fpcmp4 = fopen("/media/card/data/car_in_4_max_pool.out","r");
	FILE * fpcmp8 = fopen("/media/card/data/car_in_8_max_pool.out","r");

	for(int i = 0; i < 301056; i++) {
		fscanf(fpc1, "%d", &value);
		in1[i] = value;
		fscanf(fpc2, "%d", &value);
		in2[i] = value;
		fscanf(fpc3, "%d", &value);
		in3[i] = value;
		fscanf(fpc4, "%d", &value);
		in4[i] = value;
		fscanf(fpc5, "%d", &value);
		in5[i] = value;
		fscanf(fpc6, "%d", &value);
		in6[i] = value;
		fscanf(fpc7, "%d", &value);
		in7[i] = value;
		fscanf(fpc8, "%d", &value);
		in8[i] = value;
		fscanf(fpc9, "%d", &value);
		in9[i] = value;
		fscanf(fpc10, "%d", &value);
		in10[i] = value;
		fscanf(fpcmp1, "%d", &value);
		inmp1[i] = value;
		fscanf(fpcmp4, "%d", &value);
		inmp4[i] = value;
		fscanf(fpcmp8, "%d", &value);
		inmp8[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);
	fclose(fpcmp1);
	fclose(fpcmp4);
	fclose(fpcmp8);

	cout << "end init in arrays" << endl;

	cout << "start init out arrays (To be removed in real application)" << endl;

	fpc1 = fopen("/media/card/data/car_out_1.out","r");
	fpc2 = fopen("/media/card/data/car_out_2.out","r");
	fpc3 = fopen("/media/card/data/car_out_3.out","r");
	fpc4 = fopen("/media/card/data/car_out_4.out","r");
	fpc5 = fopen("/media/card/data/car_out_5.out","r");
	fpc6 = fopen("/media/card/data/car_out_6.out","r");
	fpc7 = fopen("/media/card/data/car_out_7.out","r");
	fpc8 = fopen("/media/card/data/car_out_8.out","r");
	fpc9 = fopen("/media/card/data/car_out_9.out","r");
	fpc10 = fopen("/media/card/data/car_out_10.out","r");
	fpcmp1 = fopen("/media/card/data/car_out_1_max_pool.out","r");
	fpcmp4 = fopen("/media/card/data/car_out_4_max_pool.out","r");
	fpcmp8 = fopen("/media/card/data/car_out_8_max_pool.out","r");

	for(int i = 0; i < 301056; i++) {
		fscanf(fpc1, "%d", &value);
		out_sw1[i] = value;
		fscanf(fpc2, "%d", &value);
		out_sw2[i] = value;
		fscanf(fpc3, "%d", &value);
		out_sw3[i] = value;
		fscanf(fpc4, "%d", &value);
		out_sw4[i] = value;
		fscanf(fpc5, "%d", &value);
		out_sw5[i] = value;
		fscanf(fpc6, "%d", &value);
		out_sw6[i] = value;
		fscanf(fpc7, "%d", &value);
		out_sw7[i] = value;
		fscanf(fpc8, "%d", &value);
		out_sw8[i] = value;
		fscanf(fpc9, "%d", &value);
		out_sw9[i] = value;
		fscanf(fpc10, "%d", &value);
		out_sw10[i] = value;
		fscanf(fpcmp1, "%d", &value);
		outmp_sw1[i] = value;
		fscanf(fpcmp4, "%d", &value);
		outmp_sw4[i] = value;
		fscanf(fpcmp8, "%d", &value);
		outmp_sw8[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);
	fclose(fpcmp1);
	fclose(fpcmp4);
	fclose(fpcmp8);

	cout << "end init out arrays" << endl;


	fpc1 = fopen("/media/card/data/img.out","r");
	for(int i = 0; i < 50176; i++) {
		fscanf(fpc1, "%d", &value);
		MIDTYPE temp1 = value;
		fscanf(fpc1, "%d", &value);
		MIDTYPE temp2 = value;
		fscanf(fpc1, "%d", &value);
		MIDTYPE temp3 = value;
		img_in[i] = ((temp1<<16)&0x00FF0000)
				| ((temp2<<8)&0x0000FF00)
				| ((temp3)&0x000000FF);
	}
	fclose(fpc1);


}


void init_lite (MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10, MIDTYPE *img_in) {

	MIDTYPE value;

	cout << "start init kernel arrays" << endl;

	FILE * fpc1 = fopen("/media/card/data/car_kernel_1.out","r");
	FILE * fpc2 = fopen("/media/card/data/car_kernel_2.out","r");
	FILE * fpc3 = fopen("/media/card/data/car_kernel_3.out","r");
	FILE * fpc4 = fopen("/media/card/data/car_kernel_4.out","r");
	FILE * fpc5 = fopen("/media/card/data/car_kernel_5.out","r");
	FILE * fpc6 = fopen("/media/card/data/car_kernel_6.out","r");
	FILE * fpc7 = fopen("/media/card/data/car_kernel_7.out","r");
	FILE * fpc8 = fopen("/media/card/data/car_kernel_8.out","r");
	FILE * fpc9 = fopen("/media/card/data/car_kernel_9.out","r");
	FILE * fpc10 = fopen("/media/card/data/car_kernel_10.out","r");

	for(int i = 0; i < 50176; i++) {
		fscanf(fpc1, "%d", &value);
		kernel1[i] = value;
		fscanf(fpc2, "%d", &value);
		kernel2[i] = value;
		fscanf(fpc3, "%d", &value);
		kernel3[i] = value;
		fscanf(fpc4, "%d", &value);
		kernel4[i] = value;
		fscanf(fpc5, "%d", &value);
		kernel5[i] = value;
		fscanf(fpc6, "%d", &value);
		kernel6[i] = value;
		fscanf(fpc7, "%d", &value);
		kernel7[i] = value;
		fscanf(fpc8, "%d", &value);
		kernel8[i] = value;
		fscanf(fpc9, "%d", &value);
		kernel9[i] = value;
		fscanf(fpc10, "%d", &value);
		kernel10[i] = value;
	}

	fclose(fpc1);
	fclose(fpc2);
	fclose(fpc3);
	fclose(fpc4);
	fclose(fpc5);
	fclose(fpc6);
	fclose(fpc7);
	fclose(fpc8);
	fclose(fpc9);
	fclose(fpc10);

	cout << "end init kernel arrays" << endl;

	fpc1 = fopen("/media/card/data/img.out","r");
	for(int i = 0; i < 50176; i++) {
		fscanf(fpc1, "%d", &value);
		MIDTYPE red = value;
		fscanf(fpc1, "%d", &value);
		MIDTYPE green = value;
		fscanf(fpc1, "%d", &value);
		MIDTYPE blue = value;
		img_in[i] = ((blue<<16)&0x00FF0000)
				| ((green<<8)&0x0000FF00)
				| ((red)&0x000000FF);
	}
	fclose(fpc1);


}


void l1_l2_mp1_run (MIDTYPE *in, MIDTYPE *out) {

	unsigned char input[111][111][64];
	unsigned char output[55][55][64];

	int inCounter = 0;
	for (int i = 0; i < 111; i++) {
		for (int j = 0; j < 111; j++) {
			for (int k = 0; k < 64; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 111; i+=2) {
		for (int j = 1; j < 111; j+=2) {
			for (int k = 0; k < 64; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 55; i++) {
		for (int j = 0; j < 55; j++) {
			for (int k = 0; k < 64; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

}


void l1_l2_mp4_run (MIDTYPE *in, MIDTYPE *out) {

	unsigned char input[55][55][256];
	unsigned char output[27][27][256];

	int inCounter = 0;
	for (int i = 0; i < 55; i++) {
		for (int j = 0; j < 55; j++) {
			for (int k = 0; k < 256; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 55; i+=2) {
		for (int j = 1; j < 55; j+=2) {
			for (int k = 0; k < 256; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 27; i++) {
		for (int j = 0; j < 27; j++) {
			for (int k = 0; k < 256; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

}


void l1_l2_mp8_run (MIDTYPE *in, MIDTYPE *out) {

	unsigned char input[27][27][512];
	unsigned char output[13][13][512];

	int inCounter = 0;
	for (int i = 0; i < 27; i++) {
		for (int j = 0; j < 27; j++) {
			for (int k = 0; k < 512; k+=4) {

				MIDTYPE temp = in[inCounter++];
				input[i][j][k] = (unsigned char)((temp>>24));
				input[i][j][k+1] = (unsigned char)((temp>>16));
				input[i][j][k+2] = (unsigned char)((temp>>8));
				input[i][j][k+3] = (unsigned char)((temp));

			}
		}
	}

	for (int i = 1; i < 27; i+=2) {
		for (int j = 1; j < 27; j+=2) {
			for (int k = 0; k < 512; k++) {

				unsigned char max = 0;
				for (int l = 0; l < 3; l++) {
					int truei = i-1+l;
					for (int m = 0; m < 3; m++) {
						int truej = j-1+m;
						if (input[truei][truej][k] > max) max = input[truei][truej][k];
					}
				}
				output[i/2][j/2][k] = max;

			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 13; j++) {
			for (int k = 0; k < 512; k+=4) {

				out[outCounter++] = ((output[i][j][k]<<24))
							| ((output[i][j][k+1]<<16))
							| ((output[i][j][k+2]<<8))
							| ((output[i][j][k+3]));

			}
		}
	}

}


void l1_l2_33_hw_run (MIDTYPE *in, MIDTYPE *out, MIDTYPE *kernel,
						Settings *info, int in_size, int out_size, int param_size) {

	MIDTYPE info_array[12];
	info_array[0] = (MIDTYPE)(info->dataSize);
	info_array[1] = (MIDTYPE)(info->inputChannel);
	info_array[2] = (MIDTYPE)(info->outputChannel);
	info_array[3] = (MIDTYPE)(info->isFirst);
	info_array[4] = (MIDTYPE)(info->isLast);
	info_array[5] = (MIDTYPE)(info->startSize);
	info_array[6] = (MIDTYPE)(info->endSize);
	info_array[7] = (MIDTYPE)(info->loopDivider);
	info_array[8] = (MIDTYPE)(info->firstChannel);
	info_array[9] = (MIDTYPE)(info->firstDivider);
	info_array[10] = (MIDTYPE)(info->Shift);
	info_array[11] = (MIDTYPE)(info->shiftFirst);

	l1_l2_hw(in, out, kernel, info_array, in_size, out_size, param_size);

}


void upSamplingNew (MIDTYPE *in, MIDTYPE *out) {

	perf_counter hw_ctr;
	hw_ctr.start();

	short data[224][224][21];
	int inCounter = 0;


	for (int k = 0; k < 21; k++) {
		data[10][10][k] = ((short)in[inCounter++])*17;
	}

	for (int j = 27; j < 224; j+=17) {
		for (int k = 0; k < 21; k++) {

			short temp = ((short)in[inCounter++])*17;
			short tempPrev = data[10][j-17][k];
			short diff = (temp - tempPrev)/17;

			for (int l = 1; l <= 17; l++) {
				data[10][j-17+l][k] = tempPrev + l*diff;
			}

		}
	}

	for (int i = 27; i < 224; i+=17) {

		for (int k = 0; k < 21; k++) {

			short temp = ((short)in[inCounter++])*17;
			short tempPrev = data[i-17][10][k];
			short diff = (temp - tempPrev)/17;

			for (int l = 1; l <= 17; l++) {
				data[i-17+l][10][k] = tempPrev + l*diff;
			}

		}

		for (int j = 27; j < 224; j+=17) {
			for (int k = 0; k < 21; k++) {

				short temp = ((short)in[inCounter++])*17;
				short tempPrev = data[i][j-17][k];
				short diff = (temp - tempPrev)/17;

				for (int l = 1; l <= 17; l++) {

					short temp2 = tempPrev + l*diff;
					short tempPrev2 = data[i-17][j-17+l][k];
					short diff2 = (temp2 - tempPrev2)/17;

					for (int m = 1; m <= 17; m++) {
						data[i-17+m][j-17+l][k] = tempPrev2 + m*diff2;
					}

				}

			}
		}
	}

	const unsigned char color[21][3] = {
			{0, 0, 0}, // black, background
			{128, 0, 0}, // maroon,
			{255, 0, 0}, // red
			{255, 200, 220}, // pink
			{170, 110, 40}, // brown
			{255, 150, 0}, // orange
			{255, 215, 180}, // coral
			{128, 128, 0}, // olive
			{255, 235, 0}, // yellow
			{255, 250, 200}, // beige
			{190, 255, 0}, // lime
			{0, 190, 0}, // green
			{170, 255, 195}, // mint
			{0, 128, 128}, // teal
			{100, 255, 255}, // cyan
			{0, 0, 128}, // navy
			{67, 133, 255}, // blue
			{130, 0, 150}, // purple
			{230, 190, 255}, // lavender
			{255, 0, 255}, // magenta
			{128, 128, 128}}; // grey

	unsigned char img[224][224][3];
	for (int i = 10; i <= 214; i++) {
		for (int j = 10; j <=214; j++) {
			unsigned char index = 20;
			short max = data[i][j][20];
			for (int k = 0; k < 20; k++) {
				if (data[i][j][k] > max) {
					max = data[i][j][k];
					index = k;
				}
			}
			img[i][j][0] = color[index][0];
			img[i][j][1] = color[index][1];
			img[i][j][2] = color[index][2];
		}
	}

	for (int i = 10; i <=214; i++) {
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[i][10][k];
			}
		}
		for (int j = 215; j < 224; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[i][214][k];
			}
		}
	}

	for (int j = 10; j <=214; j++) {
		for (int i = 0; i < 10; i++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[10][j][k];
			}
		}
		for (int i= 215; i < 224; i++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[214][j][k];
			}
		}
	}

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[10][10][k];
			}
		}
	}

	for (int i = 0; i < 10; i++) {
		for (int j = 215; j < 224; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[10][214][k];
			}
		}
	}

	for (int i = 215; i < 224; i++) {
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[214][10][k];
			}
		}
	}

	for (int i = 215; i < 224; i++) {
		for (int j = 215; j < 224; j++) {
			for (int k = 0; k < 3; k++) {
				img[i][j][k] = img[214][214][k];
			}
		}
	}

	int outCounter = 0;
	for (int i = 0; i < 224; i++) {
		for (int j = 0; j < 224; j++) {
			for (int k = 0; k < 3; k++) {
				out[outCounter++] = img[i][j][k];
			}
		}
	}

	hw_ctr.stop();
	uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();
	cout << "Number of cycles for up sampling: " << hw_cycles << endl;

}

void write_file (MIDTYPE *img) {

	FILE * fp = fopen("/media/card/output_image","w");

	int outCounter = 0;
	for(int i=0; i<224; i++) {
		for (int j = 0; j < 224; j++) {
			for (int k = 0; k < 3; k++) {
				fprintf(fp,"%d\n",img[outCounter++]);
			}
		}
	}

	fclose(fp);

}


void run_segmentation (MIDTYPE *in, MIDTYPE *img_in, MIDTYPE *out, MIDTYPE *outcacheable
				, MIDTYPE *kernel1, MIDTYPE *kernel2, MIDTYPE *kernel3,
				MIDTYPE *kernel4, MIDTYPE *kernel5, MIDTYPE *kernel6, MIDTYPE *kernel7,
				MIDTYPE *kernel8, MIDTYPE *kernel9, MIDTYPE *kernel10) {

	Settings* info[10];

	(info[0]) = new Settings(224, 3, 64, true, false, 0, 224, 1, 3, 1, 7, 0);
	(info[1]) = new Settings(55, 16, 64, false, false, -1, 56, 1, 64, 2, 3, 2);
	(info[2]) = new Settings(55, 16, 64, false, false, -1, 56, 1, 128, 4, 3, 3);
	(info[3]) = new Settings(55, 32, 128, false, false, -1, 56, 2, 128, 4, 4, 3);
	(info[4]) = new Settings(27, 32, 128, false, false, -1, 28, 2, 256, 8, 4, 4);
	(info[5]) = new Settings(27, 48, 192, false, false, -1, 28, 3, 256, 8, 4, 4);
	(info[6]) = new Settings(27, 48, 192, false, false, -1, 28, 3, 384, 12, 4, 4);
	(info[7]) = new Settings(27, 64, 256, false, false, -1, 28, 4, 384, 12, 4, 4);
	(info[8]) = new Settings(13, 64, 256, false, false, -1, 14, 4, 512, 16, 4, 5);
	(info[9]) = new Settings(13, 21, 21, false, true, 0, 13, 0, 512, 16, 0, 4);

	int in_size[10];
	int out_size[10];
	int param_size[10];

	for (int i = 0; i < 10; i++) {

		const int data_size = info[i]->dataSize;
		const int out_ch = info[i]->outputChannel;
		const int in_ch = info[i]->inputChannel;
		const int first_ch = info[i]->firstChannel;

		if (i == 0) {
			in_size[i] = data_size*data_size;
			out_size[i] = 111*111*64/4;
			param_size[i] = 3*3*out_ch + out_ch;
		} else if (i == 9) {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*in_ch;
			param_size[i] = in_ch*first_ch/4 + in_ch;
		} else {
			in_size[i] = data_size*data_size*first_ch/4;
			out_size[i] = data_size*data_size*out_ch/2;
			param_size[i] = 3*3*out_ch*in_ch/4 + out_ch
					+ out_ch*in_ch/4 + out_ch + in_ch*first_ch/4 + in_ch;
		}

	}

	cout << "Run convolution start" << endl;
	l1_l2_33_hw_run (img_in, outcacheable, kernel1, info[0], in_size[0], out_size[0], param_size[0]);
	l1_l2_mp1_run(outcacheable, in);
	l1_l2_33_hw_run (in, out, kernel2, info[1], in_size[1], out_size[1], param_size[1]);
	l1_l2_33_hw_run (out, in, kernel3, info[2], in_size[2], out_size[2], param_size[2]);
	l1_l2_33_hw_run (in, outcacheable, kernel4, info[3], in_size[3], out_size[3], param_size[3]);
	l1_l2_mp4_run(outcacheable, in);
	l1_l2_33_hw_run (in, out, kernel5, info[4], in_size[4], out_size[4], param_size[4]);
	l1_l2_33_hw_run (out, in, kernel6, info[5], in_size[5], out_size[5], param_size[5]);
	l1_l2_33_hw_run (in, out, kernel7, info[6], in_size[6], out_size[6], param_size[6]);
	l1_l2_33_hw_run (out, outcacheable, kernel8, info[7], in_size[7], out_size[7], param_size[7]);
	l1_l2_mp8_run(outcacheable, out);
	l1_l2_33_hw_run (out, in, kernel9, info[8], in_size[8], out_size[8], param_size[8]);
	l1_l2_33_hw_run (in, out, kernel10, info[9], in_size[9], out_size[9], param_size[9]);

	cout << "Run convolution end, upsampling start" << endl;
	upSamplingNew(out, in);

	cout << "Writing output image to file system" << endl;
	write_file (in);

}

