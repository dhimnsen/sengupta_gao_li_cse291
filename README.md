# README #

This is the CSE 291 project repository for Dhiman Sengupta (A53201551), Zihou Gao (A53210577) and Chao Li (A97421703). 

### Assignment 1 

* Due: April 13, 2017
* Current Version 1.5
* Description: Matrix multiplication is a core building block of many algorithms including convolutional neural networks. In this assignment, we will design and optimize matrix multiplication algorithm on an FPGA using Vivado High-level synthesis. 

### Assignment 2 

* Due: April 25, 2017
* Current Version 0.0
* Description: TBD

### Final Project 

**Image Segmentation using Fully Convolutional Networks on FPGA:**  The motivation for this project comes from Shelhamer et al.'s work on using [fully convolutional networks for semantic segmentation](https://arxiv.org/pdf/1605.06211.pdf). The main goal is to implement the image segmentation algorithm on an FPGA to enable realtime image segmenation. In the future, we predict self-driving cars and augmented reality will be a norm among the general population. Having an efficient image segmentation algorithm implemented on an FPGA will reduce the need to off load computations to a server, effectively reducing the latency for time sensitive events like object detection. In addition, due to the reprogrammability versatilities of an FPGA board, any software updates can be made frequently as better algorithms are implemented.

#### Project Proposal ####

* Due: April 13, 2017
* Current Version 1.0
* Description: Come up with a vision project idea we want to implement on an FPGA using Vivado HLS.

#### Project Presentation 1 ####

* Due: May 02, 2017
* Current Version: Upon request
* Description: Done


#### Project Final Presentation ####

* Due: June 06, 2017
* Current Version: Upon request
* Description: Done