Image Segmentation using Fully Convolutional Networks on FPGA 
====================


[TOC]


# Motivation

The motivation for this project comes from [Fully Convolutional Networks for Semantic Segmentation by Shelhamer, et al.](https://arxiv.org/pdf/1605.06211.pdf). The main goal is to implement the image segmentation algorithm on an FPGA to enable realtime image segmenation. In the future, we predict that low powered automonous vichecles, like drones, and augmented reality will be a norm among the general population. Having an efficient image semantic segmentation algorithm implemented on a low power FPGA will reduce the need to off load computations to a server, effectively reducing the latency for time sensitive events like object detection. In addition, due to the reprogrammability versatilities of an FPGA board, any software updates can be made frequently as better algorithms are implemented.

# Software Algorithm Description

For this project to be able to achieve our motivations we needed to set some goals, which are as follows:

* Low Power (< 3.5W, about how much a cellphone uses to record video)
* Low Latency (< .15s, which is a human's reaction time)
* High FPS (> 15 fps, about half the refresh rate of a human eye)

Keeping this in mind we developed our software algorithm. In this section, we will talk first about the algorithm introduced in the [Fully Convolutional Networks for Semantic Segmentation by Shelhamer, et al.](https://arxiv.org/pdf/1605.06211.pdf) paper. Then we will look at repurposing a more FPGA friendly convolutional neural network, Squeezenet, to do the semantic segmentation. Finally, we shall take a look at the major hurdle of porting the trained model to the FPGA. 

**Fully Convolutional Network**
 
In all image classification Convolutional Neural Networks (CNNs) the last few layers are flattened, therefore, getting rid of all of the two dimensional data available in the convolutional layers. In [Fully Convolutional Networks for Semantic Segmentation by Shelhamer, et al.](https://arxiv.org/pdf/1605.06211.pdf), they modify the CNNs so that the entire model uses only convolutional and maxpool layers, instead of flattening and using fully connected layers in the last few layers like the traditional CNNs. This new type of model is called a Fully Convolutional Network (FCN) and it can be trained to semantically segment images, just like the figure below. 

![fcn seg](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/fcn_seg_pic.png )

Shelhamer, et al. have published their code on how they trained their model on their GitHub (https://github.com/aurora95/Keras-FCN), which is what was used to train our model. The GitHub already contains some pre-trained models, however, those models were too big for us to implement on an FPGA.

**Squeezenet**

So what we did was modify an existing model called Squeezenet for our application. The reason why we choose Squeezenet is due to its size compared to other traditional CNNs, just like the name of their paper suggest [Squeezenet: Alexnet-Level Accuracy with 50x Fewer Parameters and < 0.5MB Model Size by Iandola, et al.](https://arxiv.org/pdf/1602.07360.pdf). The figure below is the overall architecture of the orignial Squeezenet. 

The three main modification that we made for our model so that it is able to semantically segment images are: first, we needed to get rid of the avgpool10, which is the flattening layer of this model; second, we inserted a bilinear upsampling layer at the end; and lastly, we reduced the filter size of conv1 from a 7x7x96 to a 3x3x64 kernel size. Our entire model can be seen at the end of this wiki.  

![fcn seg](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/Selection_012.png )

**Model to FPGA**

By adding our modified Squeezenet model to the Keras-FCN code base, we were able to sucessfully train our model. The next hurdle we had to over come was how to port the model into FPGA, especially in order to achieve the goals stated at the beginning of this section. All of the parameters, input and output of the model are in floating point, however, if we want to achieve our goals we need to convert those to fixed point. In order to do that we went layer by layer and found what the range of the values needed to be for each layer and with that information we converted all of the parameters, inputs and outputs to a 8 bit fixed point notation called Q notation (https://en.wikipedia.org/wiki/Q_(number_format)) from floating. The table shows the representation below: 

![fcn seg](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/Selection_014.png )

# Overall System Architecture

![integration](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/integration.jpg)

The above figure shows our overall system architecture on the Zedboard. In the ARM core, we are using two threads to process our algorithm. The first thread uses OpenCV to communicate with the USB camera and sends the video frame to our FPGA accelerator, which runs our FCN segmentation algorithm. The accelerator then output its result to thread two. In the second thread the data is bilinearly upsampled, which is then used to create a video output frame using false color to depict the different objects that have been semantically segmented. To display the output frames correctly, we used the given HDMI driving code to write to the frame buffer of the monitor.

# Accelerator Description

**Overview: trip of each frame**

In order to speed up the convolution computations, we decide to implement a universal core in FPGA of Zynq chip. The main structure of the accelerator is shown below:

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/each_frame.png )

We can see all layers are executed sequentially, using DRAM as intermediate buffer. Also, the max-pool core is not implemented in FPGA, because the ARM core is already fast enough to do the simple integer comparisons. 

**Universal accelerator toplevel**

We built a universal core for fire layers. Each fire layer has different input/output channel size and data size, so that our core supports variable sizes, initialized by parameters loaded into the core at the beginning of execution. Note that conv1 layer only utilizes the expand core, and conv10 only utilizes squeeze core.

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/toplevel.png )

After all parameters are loaded, the core will first load all weights and offsets to the BRAM. This will take some time, but we must wait for all weights be stored into BRAM before we start actual computations. 

Then, we can see that there are four processes in the main computation: input_buf_process, squeeze_process, expand_process and output_buf_process. Dataflow pragma is specified to let them be executed in parallel, and there are 3 FIFOs between every two processes to let the data flow through from top to the bottom. Basically, the "Expand Core" is the main core which takes up most of the resources on chip, and "Squeeze Core" is the side core. Since main core need to do 3x3 convolution, it needs the line-buffer and window-buffer (implemented in BRAM) to store the temporary rows of data. 

**Expand Core**

This core design is motivated by the [ZynqNet thesis](https://github.com/dgschwend/zynqnet/blob/master/zynqnet_report.pdf). They designed a universal 1x1&3x3 convolution core for floating-point numbers. Since our design uses fixed-point numbers, the amount of computations drops dramatically, so that we can achieve more parallelism. 

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/expand.png )

Basically our design is limited by the number of DSPs (totally 220), and this main core uses totally 144+16=160 DSPs for MACC (multiplication and accumulation) computations. We can see that all input data are first loaded into line buffer and window buffer, and this loop is pipelined to save more time. After that, 16 input channels are sent into 1x1 and 3x3 convolution cores at the same time within one cycle. This loop is also pipelined, and trip-count is equal to (in_ch/16*out_ch), since the nested loops are automatically flattened. In this way, we can see that within the pipelined loop, DSPs are always running at full speed without any waste of time.

**Squeeze Core**

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/squeeze.png )

This is a simplified version of Expand core, since it only handles the 1x1 convolution. The side core takes up 32 DSPs. In this way, the number of DSPs for squeeze1x1, expand1x1 and expand3x3 is exactly 2:1:9, which is a proper distribution considering the amount of MACCs for each convolution process. Also, the total number of DSPs is 192, which indicates really high utilization. 

This side core has a feature that the main core does not have: Dataflow is applied within the core. This is because the ratio of in_ch and out_ch of this core is approximately 8:1, so it takes almost same time to "load data" and "compute data". In order to fully utilize all DSPs, the data loading and computing are now executed at the same time, with a temporary 1D array as intermediate buffer. 


**Implementation Result**

Our design is synthesized from C code to Verilog by Vivado_HLS 2016.4. All data-transfer network are generated by SDSOC 2015.4, and implemented onto Zynq chip with Vivado 2015.4. After 80 minutes generation, the bitstream for FPGA can be generated:

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/utilization.png )

In the above implemented design, red part represents the Expand Core (main core), green part represents Squeeze Core (side core), and orange part represents HDMI output core. The actual utilization on chip is shown below:

| LUT | REG | BRAM | DSP |
------|-----|------|------
| 26554 | 41568 | 267 | 199 |
| 49.91% | 39.07% | 95.36% | 90.45% |

With this computation core only running on the zynq chip (single core mode on ARM), 15.2 frame per second can be achieved (excluding up-sampling process, since it is running in another thread in ARM), which hits our initial target. The power estimation are shown below:

|Dynamic Power|Static Power|Total Power|
|-------------|------------|-----------|
|2.371W       |0.207W      |2.578W     |

# Overall Performance

When integrated with all processes in ARM core and run as a whole, the performance gets down a little bit, because of lack of optimization on ARM. The final performance are shown below:

|Refreash Rate|Frame Delay|Resolution|Total Memory|CPU Utilization|
|-------------|-----------|----------|------------|---------------|
|12.0 fps     |~0.5s      |224x224   |140MB       |93.5%          |

(This includes the Linux Operating System)

We also test some static image on our design. The semantic segmentation results are shown below:

![each frame](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/performance.png )

It can be seen that it can correctly recognize the type of object and the segmentation is generally acceptable. It loses a lot of details (for example, leg of chair), because we up-sample the result from 13x13 to 224x224 directly. It can be better if we make it in the 4s-upsampling. Also, the tiny error in segmentation is due to the float-to-fixed process, since it loses some precision.


# How to Run the Program on the Zedboard

1. Copy all files in "Final_version_for_demo/sd_card" to the root of SD card
2. Connect USB camera to the USB port on zedboard, via usb-hub(for more power)
3. Connect monitor to zedboard HDMI port
4. Power on zedboard, use "user_name = root" and "password = root" to login linux
5. Type "/media/card/os2.elf" to run the program


# Team Members

## Dhiman Sengupta

* Design the fundamental structure of neural network 
* Implement software version of the model
* Train and Tune the model 
* Quantized the model
* Designed the overall architecture for the accelerator.
* Wrote testbench to compare the expected output from our software model to our hardware implement
* Tuned the whole design on board
* Write the presentations 

## Chao (Jack) Li

* Implement the overall architecture in SDSoC
* Implement video/image IO using USB/PCIe/HDMI, 
* Tuning the whole design on board
* Write the presentations 

## Zihou (Paul) Gao

* Implement the algorithms on the FPGA using Vivado HLS
* Optimize the building blocks on Vivado HLS
* Write testbench to compare the expected output from our software model to our hardware implement
* Tuning the whole design on board
* Write the presentations 

# Video
Just to preface this video, inorder to fit the video into 5 mins we had to speed up the video by 15%. This means that our voices will sound weird. =)

[![video](https://img.youtube.com/vi/9MxLucsJweU/0.jpg)](https://www.youtube.com/watch?v=9MxLucsJweU)

# Model

![fcn seg](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/model.png )