#ifndef _SOBEL_H_
#define _SOBEL_H_

#include <stdint.h>

#include "hls_stream.h"
#include "ap_int.h"

using namespace std;

typedef ap_int<9> DTYPE;
typedef hls::stream<DTYPE> DSTREAM;

const uint8_t DEBUG = 0;

const uint16_t ROW = 480;
const uint16_t COL = 640;

void sobel_filter(DTYPE matrix1[ROW][COL], DTYPE out[ROW-2][COL-2]);


#endif
