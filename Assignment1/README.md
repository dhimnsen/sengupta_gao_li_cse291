Assignment 1
====================

[TOC]


# Task 1: 

## Hardware Architecture Diagram 


![mat](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/mat.jpg)

**Figure 1:** Matrix partitions for integer matrix multiplication

![task1](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/task1.jpg)

**Figure 2:** Architecture datapath for integer matrix multiplication

## Performance and Area Utilization Results 

~~~~
+ Timing (ns): 
    Summary: 
    +--------+-------+----------+------------+
    |  Clock | Target| Estimated| Uncertainty|
    +--------+-------+----------+------------+
    |ap_clk  |   5.00|      5.09|        0.62|
    +--------+-------+----------+------------+

+ Latency (clock cycles): 
    Summary: 
    +-------+-------+-------+-------+---------+
    |    Latency    |    Interval   | Pipeline|
    |  min  |  max  |  min  |  max  |   Type  |
    +-------+-------+-------+-------+---------+
    |  16390|  16390|  16391|  16391|   none  |
    +-------+-------+-------+-------+---------+

    + Detail: 
        Instance: 
        N/A

        Loop: 
        +----------+-------+-------+----------+-----------+-----------+-------+----------+
        |          |    Latency    | Iteration|  Initiation Interval  |  Trip |          |
        | Loop Name|  min  |  max  |  Latency |  achieved |   target  | Count | Pipelined|
        +----------+-------+-------+----------+-----------+-----------+-------+----------+
        |- Loop 1  |  16388|  16388|         6|          1|          1|  16384|    yes   |
        +----------+-------+-------+----------+-----------+-----------+-------+----------+


================================================================
== Utilization Estimates
================================================================
Summary: 
+-----------------+---------+-------+--------+-------+
|       Name      | BRAM_18K| DSP48E|   FF   |  LUT  |
+-----------------+---------+-------+--------+-------+
|DSP              |        -|    128|       -|      -|
|Expression       |        -|      -|       0|   4158|
|FIFO             |        -|      -|       -|      -|
|Instance         |        -|      -|       -|      -|
|Memory           |        -|      -|       -|      -|
|Multiplexer      |        -|      -|       -|     41|
|Register         |        -|      -|    8259|     11|
+-----------------+---------+-------+--------+-------+
|Total            |        0|    128|    8259|   4210|
+-----------------+---------+-------+--------+-------+
|Available        |      280|    220|  106400|  53200|
+-----------------+---------+-------+--------+-------+
|Utilization (%)  |        0|     58|       7|      7|
+-----------------+---------+-------+--------+-------+

~~~~ 

## Specific Optimizations 

- We narrow down the input data bit-width from 32 bits down to 13 bits, in order to speed up the multiplication part. The reason is that our output matrix also has data type of "int", which can be -2^31 ~ (2^31-1). If we consider that there will be 128 temp values that sum up together to get the single output value, each temp value (multiplication of two input number) can be -2^24~2^24-1, so that our input values must be in range of -2^12~2^12-1 to avoid overflow in output matrix. So, using 13 bits to represent input values are enough.

- Matrix1, matrix2 and output matrix are fully partitioned in dimension 2, in order to load and write all 128 columns together, so that we can successfully unroll the inner-loop. 

- The 128 rows of temp values are added in middle loop, which is also pipelined to speed up the process. The reason why we do not apply unroll pragma on middle loop is that we are now utilizing 59% of the DSPs on chip. If we unroll the middle loop with factor of 2, we will be using 118% of the DSPs, which is not feasible. 


# Task 2: 

## Hardware Architecture Diagram 

![mat1](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/mat1.jpg)

**Figure 3:** Input matrix1 partition for binary matrix multiplication

![mat2](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/mat2.png)

**Figure 4:** Input matrix2 partition for binary matrix multiplication

![out](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/mat1.png)

**Figure 5:** Output matrix parition for binary matrix multiplication

![task1](https://bitbucket.org/dhimnsen/sengupta_gao_li_cse291/downloads/task2.png)

**Figure 6:** Architecture datapath for binary matrix multiplication


## Performance and Area Utilization Results 

~~~~ 
================================================================
== Performance Estimates
================================================================
+ Timing (ns): 
    * Summary: 
    +--------+-------+----------+------------+
    |  Clock | Target| Estimated| Uncertainty|
    +--------+-------+----------+------------+
    |ap_clk  |   5.00|      5.09|        0.62|
    +--------+-------+----------+------------+

+ Latency (clock cycles): 
    * Summary: 
    +-----+-----+-----+-----+---------+
    |  Latency  |  Interval | Pipeline|
    | min | max | min | max |   Type  |
    +-----+-----+-----+-----+---------+
    |  264|  264|  265|  265|   none  |
    +-----+-----+-----+-----+---------+

    + Detail: 
        * Instance: 
        N/A

        * Loop: 
        +----------+-----+-----+----------+-----------+-----------+------+----------+
        |          |  Latency  | Iteration|  Initiation Interval  | Trip |          |
        | Loop Name| min | max |  Latency |  achieved |   target  | Count| Pipelined|
        +----------+-----+-----+----------+-----------+-----------+------+----------+
        |- Loop 1  |  262|  262|         8|          1|          1|   256|    yes   |
        +----------+-----+-----+----------+-----------+-----------+------+----------+



================================================================
== Utilization Estimates
================================================================
* Summary: 
+-----------------+---------+-------+--------+-------+
|       Name      | BRAM_18K| DSP48E|   FF   |  LUT  |
+-----------------+---------+-------+--------+-------+
|DSP              |        -|      -|       -|      -|
|Expression       |        -|      -|       0|  48954|
|FIFO             |        -|      -|       -|      -|
|Instance         |        -|      -|       -|      -|
|Memory           |        -|      -|       -|      -|
|Multiplexer      |        -|      -|       -|     37|
|Register         |        -|      -|   19713|     11|
+-----------------+---------+-------+--------+-------+
|Total            |        0|      0|   19713|  49002|
+-----------------+---------+-------+--------+-------+
|Available        |      280|    220|  106400|  53200|
+-----------------+---------+-------+--------+-------+
|Utilization (%)  |        0|      0|      18|     92|
+-----------------+---------+-------+--------+-------+

~~~~ 

## Specific Optimizations 

- Modified the bit width since the maximum size needed to represent a -128 to 128 integer is with 9 bits. 
- Partitioned the input array such that it removes the load/store dependency and limitations. 
- Use two temporary arrays of size 2 bit, represented as wires, to more efficiently compute the matrix multiplication. 
- Figured out a way to remove all DSP48 by using XOR and Addition to do multiplications on the numbers -1 and 1.
- The equation for the i,jth matrim cell is computed by sum += (temp[k]^temp1[k]) and sum is intiated to 128 for the offset since 1^1=0 -1^-1=0 1^-1=-2 which means the ending sum will be in the range of -256 to 0 but we want it to be in the range -128 to 128 so we create an offset of 128.